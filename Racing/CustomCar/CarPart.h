#pragma once

#include <string>
#include <list>
#include <memory>
#include "../Engine/Collider.h"

//パーツの種類
enum class PART_TYPE {
	PART_BODY,
	PART_WHEEL,
	PART_ACCESSORY,
	PART_UNKNOWN
};

class Transform;

//ステータス構造体
struct Status {
	Status()
		: speed(0.f), weight(0.f), acceleration(1.f) {
	}
	Status(float s, float w, float a)
		: speed(s), weight(w), acceleration(a) {
	}

	float speed;
	float weight;
	float acceleration;

	Status& operator+=(const Status& b);
	Status operator+(const Status& b) const;
};


//車のパーツを扱う
class CarPart {

	//長いので置き換え
	using CollisionList	= std::list<std::unique_ptr<Collider>>;

private:
	int				hModel_;			//モデルハンドル
	std::string		modelName_;			//モデルファイル名
	PART_TYPE		type_;				//パーツの種類
	Status			status_;			//能力値
	CollisionList	collisionList_;		//当たり判定リスト

public:
	/**
	 * \brief デフォルトコンストラクタ(非推奨)
	 */
	CarPart();
	/**
	 * \brief コンストラクタ
	 * \param type パーツの種類
	 */
	explicit CarPart(PART_TYPE type);

	// コピー禁止
	CarPart(const CarPart&) = delete;
	CarPart& operator=(const CarPart&) = delete;

	// ムーブ禁止
	CarPart(CarPart&&) = delete;
	CarPart& operator=(CarPart&&) = delete;
	
	virtual ~CarPart() = default;

public:
	/**
	 * \brief 初期化処理 jsonを読み込み初期化する
	 * \param jsonPath jsonファイルのパス
	 */
	virtual void Initialize(const std::string& jsonPath) = 0;

	/**
	 * \brief モデルを読み込みハンドルを取得する
	 * \param fileName ロードするモデルのファイル名
	 */
	void LoadModel(const std::string& fileName);

	/**
	 * \brief 能力値の参照の取得
	 * \return 能力値の参照
	 */
	inline const Status& GetStatus() const { return status_; }

	/**
	 * \brief 当たり判定リストの参照の取得
	 * \return 当たり判定リストの参照
	 */
	inline const CollisionList& GetCollisionList() const { return collisionList_; }

	/**
	 * \brief パーツの種類の取得
	 * \return パーツの種類
	 */
	inline PART_TYPE GetType() const { return type_; }
	
	/**
	 * \brief 能力値のセット
	 * \param s スピード
	 * \param w 重さ
	 * \param a 加速度
	 */
	void SetStatus(float s, float w, float a);

	/**
	 * \brief 球体判定の追加
	 * \param center 中心座標
	 * \param radius 半径
	 */
	void AddSphereCollision(XMVECTOR center, float radius);

	/**
	 * \brief 有向箱型判定の追加
	 * \brief center 中心座標
	 * \brief extents 広がり(中心座標から広げる長さ)
	 */
	void AddOrentedBoxCollision(FXMVECTOR center, FXMVECTOR extents);

	/**
	 * \brief 描画する
	 * \param transform 変形情報
	 */
	void Draw(Transform& transform) const;
};

