#include "PartBody.h"

using namespace std;

PartBody::PartBody() 
	: CarPart(PART_TYPE::PART_BODY){

}

void PartBody::Initialize(const string& jsonPath) {

}

void PartBody::AddWheelJoint(XMFLOAT3 pos) {

	wheelJoint_.emplace_back(pos);
}

void PartBody::AddAccessoryJoint(XMFLOAT3 pos) {

	accessoryJoint_.emplace_back(pos);
}
