#pragma once
#include <DirectXMath.h>
#include <list>
#include <array>
#include <memory>

#include "CarPart.h"

//前方宣言
class GameObject;
class Transform;
class PartBody;
class PartWheel;
class PartAccessory;

//プレイヤー列挙型
enum CUSTOM_NUMBER {
	CUSTOM_FIRST,
	CUSTOM_SECOND,
	CUSTOM_MAX
};

//カスタム状態を管理
class CustomCar {

private:
	PartBody* pBody_;							//選択中のボディの参照
	PartWheel* pWheel_;							//選択中のタイヤの参照
	std::list<PartAccessory*> pAccessoryList_;	//選択中のアクセサリの参照リスト

	mutable float rotation_ = 0.f;	//１フレームの回転量

	static CUSTOM_NUMBER currentPlayerCustom_;

	//２プレイヤー分のカスタム状態のインスタンス
	static std::array<std::unique_ptr<CustomCar>, CUSTOM_MAX> playerCustom;

private:
	//外部生成禁止
	CustomCar() = default;

public:
	// コピー禁止
	CustomCar(const CustomCar&) = delete;
	CustomCar& operator=(const CustomCar&) = delete;

	// ムーブ禁止
	CustomCar(CustomCar&&) = delete;
	CustomCar& operator=(CustomCar&&) = delete;

	//デストラクタ
	~CustomCar() = default;
	
public:
	/**
	 * \brief 初期化処理
	 **/
	void Initialize();
	
	/**
	 * \brief ボディの切り替え
	 * \param index マネージャ内の配列の添え字
	 **/
	void ChangeBody(unsigned index);

	/**
	 * \brief タイヤの切り替え
	 * \param index マネージャ内の配列の添え字
	 **/
	void ChangeWheel(unsigned index);

	/**
	 * \brief アクセサリの追加　すでに３つある場合は無視される
	 * \param index マネージャ内の配列の添え字
	 **/
	void AddAccessory(unsigned index);

	/**
	 * \brief アクセサリの削除
	 * \param index マネージャ内の配列の添え字
	 **/
	void RemoveAccessory(unsigned index);


	/**
	 * \brief ステイタスの取得
	 **/
	Status GetStatus();
	
	/**
	 * \brief 車を描画 
	 * \param transform 変形情報
	 **/
	void Draw(Transform& transform) const;
	/**
	 * \brief 進行量を加味した車を描画（未実装）
	 * \param transform 変形情報
	 * \param move 進行ベクトル
	 **/
	void Draw(Transform& transform, DirectX::XMVECTOR move) const;

	/**
	 * \brief カスタムに応じた当たり判定を付加（未実装）
	 * \param gameObject 付加するオブジェクト
	 **/
	void AddColliderToObject(GameObject* gameObject);

	//インスタンス受け取り用の型（auto&&で可）
	using DCustomCar = const std::unique_ptr<CustomCar>&;

	/**
	 * \brief カスタムのインスタンスを受け取る
	 * \param number 受け取りたいカスタムの番号
	 **/
	static CustomCar* GetPlayerCustom(CUSTOM_NUMBER number);

	/**
	 * \brief 第一カスタムのインスタンスを取得
	 **/
	static CustomCar* GetFirstCustom();

	/**
	 * \brief 第二カスタムのインスタンスを取得
	 **/
	static CustomCar* GetSecondCustom();

	/**
	 * \brief 現在選択されているカスタムを取得
	 */
	static CustomCar* GetCurrentCustom();

	/**
	 * \brief 現在選択されているカスタムを進める
	 */
	static void NextCustom();

	/**
	 * \brief カスタム情報インスタンス全てを破棄
	 **/
	static void DestroyAll();


private:
	/**
	 * \brief ボディを描画
	 * \param transform 変形情報
	 **/
	void DrawBody(Transform& transform) const;

	/**
	 * \brief タイヤを描画
	 * \param transform 変形情報
	 **/
	void DrawWheel(Transform& transform) const;

	/**
	 * \brief アクセサリを描画
	 * \param transform 変形情報
	 **/
	void DrawAccessory(Transform& transform) const;
};
