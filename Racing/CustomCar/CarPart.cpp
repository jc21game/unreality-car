#include "CarPart.h"

#include "../Engine/Transform.h"
#include "../Engine/StaticModel.h"
#include "../Engine/ColliderSphere.h"
#include "../Engine/ColliderOrientedBox.h"

using CollisionList = std::list<std::unique_ptr<Collider>>;

using namespace std;

CarPart::CarPart()
	: hModel_(-1), modelName_("")
	, type_(PART_TYPE::PART_UNKNOWN) {
}

CarPart::CarPart(PART_TYPE type)
	: hModel_(-1), modelName_("")
	, type_(type) {
}

void CarPart::LoadModel(const std::string& fileName) {

	hModel_ = StaticModel::Load(fileName);
	assert(hModel_ >= 0);
	modelName_ = fileName;
}

void CarPart::SetStatus(float s, float w, float a) {

	status_ = { s, w, a };
}

void CarPart::AddSphereCollision(XMVECTOR center, float radius) {

	collisionList_.emplace_back(make_unique<ColliderSphere>(center, radius));
}

void CarPart::AddOrentedBoxCollision(FXMVECTOR center, FXMVECTOR extents) {

	auto pOBB = make_unique<ColliderOrientedBox>(center, extents, g_XMZero);
	pOBB->ValidRotateSync();
	collisionList_.emplace_back(move(pOBB));
}

void CarPart::Draw(Transform& transform) const {

	StaticModel::SetTransform(hModel_, transform);
	StaticModel::Draw(hModel_);
}


Status & Status::operator+=(const Status & b) {

	speed += b.speed;
	weight += b.weight;
	acceleration += b.acceleration;

	return *this;
}

Status Status::operator+(const Status& b) const {

	Status buf;
	buf.speed = speed + b.speed;
	buf.weight = weight + b.weight;
	buf.acceleration = acceleration + b.acceleration;

	return buf;
}
