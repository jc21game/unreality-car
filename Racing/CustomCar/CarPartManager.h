#pragma once
#include <memory>
#include <vector>

//前方宣言
class CarPart;
class PartBody;
class PartWheel;
class PartAccessory;
class Transform;

//パーツを管理する
class CarPartManager {

private:
	//シングルトン
	static std::unique_ptr<CarPartManager> instance_;
	
public:
	/**
	 * \brief インスタンスの取得
	 * \return インスタンス
	 */
	static CarPartManager* GetInstance();
	/**
	 * \brief インスタンスの破棄
	 */
	static void DestroyInstance();

private:
	std::vector<std::unique_ptr<PartBody>> bodyList_;		//ボディリスト
	std::vector<std::unique_ptr<PartWheel>> wheelList_;		//タイヤリスト
	std::vector<std::unique_ptr<PartAccessory>> accessoryList_;	//アクセサリリスト

	//コンストラクタ
	CarPartManager() = default;

public:
	// コピー禁止
	CarPartManager(const CarPartManager&) = delete;
	CarPartManager& operator=(const CarPartManager&) = delete;

	// ムーブ禁止
	CarPartManager(CarPartManager&&) = delete;
	CarPartManager& operator=(CarPartManager&&) = delete;

	//デストラクタ
	~CarPartManager() = default;

public:
	/**
	 * \brief 初期化処理 パーツをここでロードする
	 */
	void Initialize();

	/**
	 * \brief 指定されたボディ情報の取得
	 * \param index 添え字
	 * \return パーツ情報
	 */
	PartBody* GetBodyAt(unsigned index);
	/**
	 * \brief 指定されたタイヤ情報の取得
	 * \param index 添え字
	 * \return パーツ情報
	 */
	PartWheel* GetWheelAt(unsigned index);
	/**
	 * \brief 指定されたアクセサリ情報の取得
	 * \param index 添え字
	 * \return パーツ情報
	 */
	PartAccessory* GetAccessoryAt(unsigned index);

	/**
	 * \brief ロードされたボディの数の取得
	 * \return ボディの数
	 */
	size_t GetBodesSize() const { return bodyList_.size(); }
	/**
	 * \brief ロードされたタイヤの数の取得
	 * \return タイヤの数
	 */
	size_t GetWheelsSize() const { return wheelList_.size(); }
	/**
	 * \brief ロードされたアクセサリの数の取得
	 * \return アクセサリの数
	 */
	size_t GetAccessoriesSize() const { return accessoryList_.size(); }

	/**
	 * \brief 指定されたボディを描画する
	 * \param index 添え字
	 * \param transform 変形情報
	 */
	void DrawBody(unsigned index, Transform& transform);
	/**
	 * \brief 指定されたタイヤを描画する
	 * \param index 添え字
	 * \param transform 変形情報
	 */
	void DrawWheel(unsigned index, Transform& transform);
	/**
	 * \brief 指定されたアクセサリを描画する
	 * \param index 添え字
	 * \param transform 変形情報
	 */
	void DrawAccessory(unsigned index, Transform& transform);

private:
	/**
	 * \brief ボディを追加する
	 * \param fileName 読み込むfbxのパス
	 * \param s スピード
	 * \param w 重さ
	 * \param a 加速度
	 * \return 作成したボディのインスタンス
	 */
	PartBody* AddBody(const std::string& fileName, float s, float w, float a);
	/**
	 * \brief タイヤを追加する
	 * \param fileName 読み込むfbxのパス
	 * \param s スピード
	 * \param w 重さ
	 * \param a 加速度
	 * \return 作成したタイヤのインスタンス
	 */
	PartWheel* AddWheel(const std::string& fileName, float s, float w, float a);
	/**
	 * \brief アクセサリを追加する
	 * \param fileName 読み込むfbxのパス
	 * \param s スピード
	 * \param w 重さ
	 * \param a 加速度
	 * \return 作成したアクセサリのインスタンス
	 */
	PartAccessory* AddAccessory(const std::string& fileName, float s, float w, float a);
};
