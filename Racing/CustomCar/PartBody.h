#pragma once
#include "CarPart.h"

//ボディの情報を保持
class PartBody : public CarPart {

	//長いので置き換え
	using JointList = std::list<XMFLOAT3>;

private:
	JointList wheelJoint_;		//タイヤとの接続部のリスト
	JointList accessoryJoint_;	//アクセサリとの接続部のリスト

public:
	//コンストラクタ
	PartBody();

public:
	/**
	 * \brief 初期化処理 jsonを読み込み初期化する(未実装)
	 * \param jsonPath jsonファイルのパス
	 */
	virtual void Initialize(const std::string& jsonPath) override;

	/**
	 * \brief タイヤとの接続部リストの取得
	 * \return タイヤとの接続部リスト
	 */
	inline const JointList& GetWheelJoint() const { return wheelJoint_; }
	/**
	 * \brief アクセサリとの接続部リストの取得
	 * \return アクセサリとの接続部リスト
	 */
	inline const JointList& GetAccessoryJoint() const { return accessoryJoint_; }

	/**
	 * \brief タイヤとの接続部の追加
	 * \param pos 座標
	 */
	void AddWheelJoint(XMFLOAT3 pos);
	/**
	 * \brief アクセサリとの接続部の追加
	 * \param pos 
	 */
	void AddAccessoryJoint(XMFLOAT3 pos);
};

