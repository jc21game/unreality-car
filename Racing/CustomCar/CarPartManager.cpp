#include "CarPartManager.h"

#include "PartBody.h"
#include "PartWheel.h"
#include "PartAccessory.h"


using namespace std;

unique_ptr<CarPartManager> CarPartManager::instance_;

CarPartManager* CarPartManager::GetInstance() {

	if(!instance_) {

		struct A : CarPartManager {};
		instance_ = make_unique<A>();
		instance_->Initialize();
	}
	return instance_.get();
}

void CarPartManager::DestroyInstance() {

	instance_.reset();
}

void CarPartManager::Initialize() {

	// todo モデルが作られたらここに追加
	/*auto body0 = AddBody("SampleResource\\torakku.fbx", 1, 1, 1);
	body0->AddWheelJoint({ 1, 0, -1.7f });
	body0->AddWheelJoint({ -1, 0, -1.7f });
	body0->AddWheelJoint({ 1, 0, 2 });
	body0->AddWheelJoint({ -1, 0, 2 });
	body0->AddOrentedBoxCollision(XMVectorSet(0, 1.5f, 0, 0), XMVectorSet(1.25f, 1.5f, 3, 0));*/
	auto* pTruckBody = AddBody("Car/truck.fbx", 1, 1500, 20);
	pTruckBody->AddWheelJoint({ 1.f, 0, -2.1f });
	pTruckBody->AddWheelJoint({ -1.f, 0, -2.1f });
	pTruckBody->AddWheelJoint({ 1.f, 0, 1.8f });
	pTruckBody->AddWheelJoint({ -1.f, 0, 1.8f });
	pTruckBody->AddOrentedBoxCollision(XMVectorSet(0, 1.5f, 0, 0), XMVectorSet(1.25f, 1.5f, 3.4f, 0));


	auto* pTruckBody1 = AddBody("Car/truck1.fbx", 1, 1500, 20);
	pTruckBody1->AddWheelJoint({ 1.f, 0, -2.1f });
	pTruckBody1->AddWheelJoint({ -1.f, 0, -2.1f });
	pTruckBody1->AddWheelJoint({ 1.f, 0, 1.8f });
	pTruckBody1->AddWheelJoint({ -1.f, 0, 1.8f });
	pTruckBody1->AddOrentedBoxCollision(XMVectorSet(0, 1.5f, 0, 0), XMVectorSet(1.25f, 1.5f, 3.4f, 0));


	//shoppingCart
	/*auto cartB = AddBody("SampleResource\\shoppingCart.fbx", 1, 1, 1);
	cartB->AddWheelJoint({ 2, 0, -2 });
	cartB->AddWheelJoint({ -2, 0, -2 });
	cartB->AddWheelJoint({ 2, 0, 2 });
	cartB->AddWheelJoint({ -2, 0, 2 });*/
	auto* pShoppingCartBody = AddBody("Car/shopping.fbx", 1, 800, 45);
	pShoppingCartBody->AddWheelJoint({ 0.5f, 0, -0.5f });
	pShoppingCartBody->AddWheelJoint({ -0.5f, 0, -0.5f });
	pShoppingCartBody->AddWheelJoint({ 0.5f, 0, 0.5f });
	pShoppingCartBody->AddWheelJoint({ -0.5f, 0, 0.5f });
	pShoppingCartBody->AddOrentedBoxCollision(XMVectorSet(0, 0.7f, 0, 0), XMVectorSet(0.4f, 0.7f, 0.8f, 0));

	//lupin
	/*auto lupinB = AddBody("SampleResource\\lupin.fbx", 1, 1, 1);
	lupinB->AddWheelJoint({ 2, 0, -5.5 });
	lupinB->AddWheelJoint({ -2, 0, -5.5 });
	lupinB->AddWheelJoint({ 2, 0, 4.5 });
	lupinB->AddWheelJoint({ -2, 0, 4.5 });*/
	auto* pLupinBody = AddBody("Car/gaisha.fbx", 1, 1250, 15);
	pLupinBody->AddWheelJoint({ 0.6f, 0, -1.75f });
	pLupinBody->AddWheelJoint({ -0.6f, 0, -1.75f });
	pLupinBody->AddWheelJoint({ 0.6f, 0, 1.6f });
	pLupinBody->AddWheelJoint({ -0.6f, 0, 1.6f });
	pLupinBody->AddOrentedBoxCollision(XMVectorSet(0, 0.5f, 0, 0), XMVectorSet(0.5f, 0.5f, 2.f, 0));

	//bike
	auto* pBikeBody = AddBody("Car/bike.fbx", 1, 1100, 30);
	pBikeBody->AddWheelJoint({ 0, 0.5f, 1.1f });
	pBikeBody->AddWheelJoint({ 0, 0.5f, -1.05f});
	pBikeBody->AddOrentedBoxCollision(XMVectorSet(0, 1.f, 0, 0), XMVectorSet(0.5f, 2.f, 2.f, 0));


	//sample
	/*auto body1 = AddBody("SampleResource\\body_sample.fbx", 1, 1, 1);
	body1->AddWheelJoint({ -1, 0, 2 });
	body1->AddWheelJoint({ 1, 0, 2 });
	body1->AddWheelJoint({ -1, 0, -2 });
	body1->AddWheelJoint({ 1, 0, -2 });*/

	//auto wheel0 = AddWheel("SampleResource\\taiya.fbx", 1, 1, 1);
	//wheel0->SetJoint({ 0, 0, 0 });

	//auto wheel1 = AddWheel("SampleResource\\wheel_sample.fbx", 1, 1 ,1);
	//wheel1->SetJoint({0, 0, 0});

	////c
	//auto wheel2 = AddWheel("SampleResource\\taiya1.fbx", 1, 1, 1);
	//wheel2->SetJoint({ 0, 0, 0 });

	AddWheel("Car/wheel0.fbx", 1, 100, 1)->SetJoint({ 0, 0, 0 });
	AddWheel("Car/wheel1.fbx", 1, 125, 1)->SetJoint({ 0, 0, 0 });
	AddWheel("Car/wheel2.fbx", 1, 150, 1)->SetJoint({ 0, 0, 0 });

}

PartBody* CarPartManager::GetBodyAt(unsigned index) {

	if(index >= bodyList_.size()) return nullptr;
	
	return bodyList_[index].get();
}

PartWheel* CarPartManager::GetWheelAt(unsigned index) {

	if (index >= wheelList_.size()) return nullptr;

	return wheelList_[index].get();
}

PartAccessory* CarPartManager::GetAccessoryAt(unsigned index) {

	if (index >= accessoryList_.size()) return nullptr;

	return accessoryList_[index].get();
}

void CarPartManager::DrawBody(unsigned index, Transform & transform) {

	if (index >= bodyList_.size()) return;

	bodyList_[index]->Draw(transform);
}

void CarPartManager::DrawWheel(unsigned index, Transform & transform) {

	if (index >= wheelList_.size()) return;

	wheelList_[index]->Draw(transform);
}

void CarPartManager::DrawAccessory(unsigned index, Transform & transform) {

	if (index >= accessoryList_.size()) return;

	accessoryList_[index]->Draw(transform);
}

PartBody* CarPartManager::AddBody(const string& fileName, float s, float w, float a) {

	auto body = make_unique<PartBody>();
	body->LoadModel(fileName);
	body->SetStatus(s, w, a);

	bodyList_.emplace_back(move(body));

	return bodyList_[bodyList_.size() - 1].get();
}

PartWheel* CarPartManager::AddWheel(const string& fileName, float s, float w, float a) {

	auto wheel = make_unique<PartWheel>();
	wheel->LoadModel(fileName);
	wheel->SetStatus(s, w, a);

	wheelList_.emplace_back(move(wheel));

	return wheelList_[wheelList_.size() - 1].get();
}

PartAccessory* CarPartManager::AddAccessory(const string& fileName, float s, float w, float a) {

	auto accessory = make_unique<PartAccessory>();
	accessory->LoadModel(fileName);
	accessory->SetStatus(s, w, a);

	accessoryList_.emplace_back(move(accessory));

	return accessoryList_[accessoryList_.size() - 1].get();
}
