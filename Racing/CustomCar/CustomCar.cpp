#include "CustomCar.h"

#include "../Engine/Transform.h"
#include "../Engine/GameObject.h"

#include "../Engine/Collider.h"

#include "CarPartManager.h"
#include "PartBody.h"
#include "PartWheel.h"
#include "PartAccessory.h"

using namespace std;

CUSTOM_NUMBER CustomCar::currentPlayerCustom_ = CUSTOM_FIRST;

array<unique_ptr<CustomCar>, CUSTOM_MAX> CustomCar::playerCustom;

void CustomCar::Initialize() {

	auto &&manager = CarPartManager::GetInstance();
	pBody_ = manager->GetBodyAt(0);
	pWheel_ = manager->GetWheelAt(0);
}

void CustomCar::ChangeBody(unsigned index) {

	pBody_ = CarPartManager::GetInstance()->GetBodyAt(index);
}

void CustomCar::ChangeWheel(unsigned index) {

	pWheel_ = CarPartManager::GetInstance()->GetWheelAt(index);
}

void CustomCar::AddAccessory(unsigned index) {

	if (pAccessoryList_.size() >= 3)	return;
	auto accessory = CarPartManager::GetInstance()->GetAccessoryAt(index);
	pAccessoryList_.emplace_back(accessory);
}

void CustomCar::RemoveAccessory(unsigned index) {

	auto pAccessory = CarPartManager::GetInstance()->GetAccessoryAt(index);
	pAccessoryList_.remove_if([&](auto* p) {return p == pAccessory; } );
}

Status CustomCar::GetStatus() {

	Status status;
	if (pBody_)	status += pBody_->GetStatus();
	if (pWheel_) status += pWheel_->GetStatus();
	for (auto&& pAccessory : pAccessoryList_) {

		if(pAccessory) status += pAccessory->GetStatus();
	}
	return status;
}

void CustomCar::Draw(Transform& transform) const {

	Draw(transform, g_XMZero);
}

void CustomCar::Draw(Transform& transform, DirectX::XMVECTOR move) const {
	
	DrawBody(transform);
	DrawWheel(transform);
	DrawAccessory(transform);
}

void CustomCar::AddColliderToObject(GameObject* gameObject) {

	//todo 当たり判定の実装をここに実装
	//ボディの当たり判定を付加
	if (pBody_) {

		for (auto&& collision : pBody_->GetCollisionList()) {

			gameObject->AddCollider(collision->Copy());
		}
	}
	//タイヤの当たり判定を付加
	if (pWheel_) {

		for (auto&& collision : pWheel_->GetCollisionList()) {

			gameObject->AddCollider(collision->Copy());
		}
	}
	for (auto&& pAccessory : pAccessoryList_) {

		if (pAccessory) {

			for (auto&& collision : pAccessory->GetCollisionList()) {

				gameObject->AddCollider(collision->Copy());
			}
		}
	}
}

CustomCar* CustomCar::GetPlayerCustom(CUSTOM_NUMBER number) {

	if (!playerCustom[number]) {

		struct A : CustomCar{};
		playerCustom[number] = std::make_unique<A>();
		playerCustom[number]->Initialize();
	}
	return playerCustom[number].get();
}

CustomCar* CustomCar::GetFirstCustom() {

	return GetPlayerCustom(CUSTOM_FIRST);
}

CustomCar* CustomCar::GetSecondCustom() {

	return GetPlayerCustom(CUSTOM_SECOND);
}

CustomCar * CustomCar::GetCurrentCustom() {

	return GetPlayerCustom(currentPlayerCustom_);
}

void CustomCar::NextCustom() {

	currentPlayerCustom_ = currentPlayerCustom_ == CUSTOM_FIRST ? CUSTOM_SECOND : CUSTOM_FIRST;
}

void CustomCar::DestroyAll() {

	for (auto&& player : playerCustom) {

		player.reset();
	}
}

void CustomCar::DrawBody(Transform& transform) const {

	if(pBody_) {

		pBody_->Draw(transform);
	}
}

void CustomCar::DrawWheel(Transform& transform) const {

	if (pBody_) {
		
		auto&& bTowJoints = pBody_->GetWheelJoint();

		if (pWheel_) {

			auto&& wTobJoint = pWheel_->GetJoint();
			for (auto&& bTowJoint : bTowJoints) {

				Transform wheelTransform;
				wheelTransform.pParent_ = &transform;
				wheelTransform.position_ += XMLoadFloat3(&bTowJoint);
				wheelTransform.position_ -= XMLoadFloat3(&wTobJoint);
				//wheelTransform.rotate_.vecZ += rotation_;
				pWheel_->Draw(wheelTransform);

				//rotation_ += 0.1;
			}
		}
	}
}
void CustomCar::DrawAccessory(Transform& transform) const {

	if(pBody_) {
		
		auto&& bToaJoints = pBody_->GetAccessoryJoint();
		auto jItr = bToaJoints.begin();
		for (auto&& pAccessory : pAccessoryList_) {

			if (pAccessory) {

				if (jItr != bToaJoints.end()) {

					auto bToaJoint = (*jItr);
					auto aTobJoint = pAccessory->GetJoint();
					Transform accessoryTransform;
					accessoryTransform.pParent_ = &transform;
					accessoryTransform.position_ += XMLoadFloat3(&bToaJoint);
					accessoryTransform.position_ += XMLoadFloat3(&aTobJoint);
					pAccessory->Draw(accessoryTransform);

					++jItr;
				}
			}
		}
	}
}
