#pragma once
#include "CarPart.h"

class PartAccessory : public CarPart {

private:
	XMFLOAT3 jointPos_;		//接続部

public:
	//コンストラクタ
	PartAccessory();

public:
	/**
	 * \brief 初期化処理 jsonを読み込み初期化する
	 * \param jsonPath jsonファイルのパス
	 */
	virtual void Initialize(const std::string& jsonPath) override;

	/**
	 * \brief 接続部の座標を取得
	 * \return 接続部の座標
	 */
	inline XMFLOAT3 GetJoint() const { return jointPos_; }

	/**
	 * \brief 接続部の座標をセット
	 * \param pos 接続部の座標
	 */
	inline void SetJoint(XMFLOAT3 pos) { jointPos_ = pos; }
};

