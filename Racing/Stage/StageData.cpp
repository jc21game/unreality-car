#include "StageData.h"
#include <iostream>
#include <list>

#include "../Engine/StaticModel.h"
#include "../Engine/StaticImage.h"
#include "../Engine/Audio.h"

using namespace std;



const string StageData::CHECK_POINT = "CheckPoint"s;
const string StageData::FACE_POINT = "FacePoint"s;

constexpr char DIRECTRY_NAME[] = "MapData/";


StageData::StageData()
	: name_("")
	, hWholeAreaModel_(-1), hRoadModel_(-1), hDirtModel_(-1), hWallModel_(-1)
	, hMiniMap_(-1), hBGI_(-1), hBGM_(-1)
	, type_(STAGE_UNKNOWN) {

	checkPointData_.clear();
}

StageData::~StageData() = default;

void StageData::LoadModel(const string& filePath) {

	hWholeAreaModel_ = StaticModel::Load(DIRECTRY_NAME + filePath + "_All.fbx");
	assert(hWholeAreaModel_ >= 0);

	hRoadModel_ = StaticModel::Load(DIRECTRY_NAME + filePath + "_Course.fbx");
	//assert(hRoadModel_ >= 0);

	hDirtModel_ = StaticModel::Load(DIRECTRY_NAME + filePath + "_Ground.fbx");
	//assert(hDirtModel_ >= 0);

	hWallModel_ = StaticModel::Load(DIRECTRY_NAME + filePath + "_Obstacle.fbx");
	//assert(hWallModel_ >= 0);
	
	//読み込んだモデルからチェックポイントのデータを生成
	GenerateCheckPoint();
}

void StageData::LoadMiniMap(const std::string & filePath) {

	hMiniMap_ = StaticImage::Load(filePath);
	assert(hMiniMap_ >= 0);
}

void StageData::LoadBgm(const std::string& filePath) {

	hBGM_ = Audio::Load(filePath);
	assert(hBGM_ >= 0);
}

void StageData::LoadBgi(const std::string& filePath) {

	hBGI_ = StaticImage::Load(filePath);
	assert(hBGI_ >= 0);
}

void StageData::GenerateCheckPoint() {

	list<XMVECTOR> facePosList;
	for (auto i = 0u;; ++i) {

		auto boneName = FACE_POINT + to_string(i);
		auto facePos = StaticModel::GetBonePosition(hRoadModel_, boneName);

		if (XMVector3Equal(facePos, XMVectorZero()))	break;
		facePosList.emplace_back(facePos);
	}

	for (auto i = 0u;; ++i) {

		auto boneName = CHECK_POINT + to_string(i);
		auto bonePos = StaticModel::GetBonePosition(hRoadModel_, boneName);
		
		//cout << "座標:" << bonePos.m128_f32[0] << ',' << bonePos.m128_f32[1] << ',' << bonePos.m128_f32[2] << endl;
		
		if (XMVector3Equal(bonePos, XMVectorZero()))	break;

		bonePos.m128_f32[0] = -bonePos.m128_f32[0];
		CheckPointData data = {};
		XMStoreFloat3(&data.position, bonePos);

		XMVECTOR facePos = g_XMZero;
		float len = FLT_MAX;
		for (auto&& pos : facePosList) {

			float tmpLen = XMVector3Length(bonePos - pos).m128_f32[0];
			if (tmpLen < len) {
				len = tmpLen;
				facePos = pos;
			}
		}

		auto baseFace = XMVectorSet(0, 0, 1, 0);
		auto vec = bonePos - facePos;
		vec = XMVector3Normalize(vec);
		auto normal = XMVectorSet(0, 1, 0, 0);//XMVector3Cross(baseFace, vec);
		data.angle = XMConvertToDegrees(acos(XMVector3Dot(baseFace, vec).m128_f32[0])) + 90.f;
	//	auto oriented =XMQuaternionRotationAxis(normal, angle);

		//auto oriented = XMConvertToDegrees(angle);
		//XMFLOAT4 ori = XMFLOAT4(0.f, oriented, 0.f, 0.f);
		//data.oriented = ori;
		
		//XMStoreFloat4(&data.oriented, oriented);
		
		data.extents = XMFLOAT3(4.f, 0.5f, 20);

		checkPointData_.push_back(data);
	}
}
