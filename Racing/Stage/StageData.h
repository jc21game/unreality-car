#pragma once

#include <string>
#include <vector>
#include <DirectXMath.h>

using namespace DirectX;

//ステージ種別
enum STAGE_TYPE {
	STAGE_NORMAL,
	STAGE_UNKNOWN
};

/**
 * \brief ステージデータ保持
 */
class StageData {

	//チェックポイントデータ
	struct CheckPointData {

		XMFLOAT3 position;	//座標
		float angle;
		//XMFLOAT4 oriented;	//回転
		XMFLOAT3 extents;	//大きさ
	};

private:
	static const std::string CHECK_POINT;		//チェックポイントボーンの名前
	static const std::string FACE_POINT;		//チェックポイントを向ける方向のボーンの名前

	std::string					name_;				//ステージ名
	int							hWholeAreaModel_;	//全域モデルハンドル
	int							hRoadModel_;		//道路用モデルハンドル
	int							hDirtModel_;		//ダート判定用モデル
	int							hWallModel_;		//壁用モデルハンドル
	int							hMiniMap_;			//ミニマップ画像ハンドル
	int							hBGI_;				//背景画像ハンドル
	int							hBGM_;				//BGMハンドル
	std::vector<CheckPointData>	checkPointData_;	//チェックポイントデータ
	STAGE_TYPE					type_;				//分類

public:
	//コンストラクタ
	StageData();

	// コピー禁止
	StageData(const StageData&) = delete;
	StageData& operator=(const StageData&) = delete;

	// ムーブ禁止
	StageData(StageData&&) = delete;
	StageData& operator=(StageData&&) = delete;

	//デストラクタ
	~StageData();

public:
	//各種リソース読み込み
	void LoadModel(const std::string& filePath);
	void LoadMiniMap(const std::string& filePath);
	void LoadBgm(const std::string& filePath);
	void LoadBgi(const std::string& filePath);
	
	//各種リソースハンドル取得
	int GetWholeAreaModelHandle() const { return hWholeAreaModel_; }
	int GetRoadModelHandle() const { return hRoadModel_; }
	int GetDirtModelHandle() const { return hDirtModel_; }
	int GetWallModelHandle() const { return hWallModel_; }
	int GetMiniMapHandle() const { return hMiniMap_; }
	int GetBgmHandle() const { return hBGM_; }
	int GetBgiHandle() const { return hBGI_; }

	//ステージ種別取得
	STAGE_TYPE GetStageType() const { return type_; }

	//チェックポイントデータコンテナ取得
	const std::vector<CheckPointData>& GetCheckPointData() const { return checkPointData_; }
	
private:
	//チェックポイント生成
	void GenerateCheckPoint();
};

