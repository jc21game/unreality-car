#include "StageDataManager.h"

std::unique_ptr<StageDataManager> StageDataManager::instance_;  // NOLINT

void StageDataManager::Initialize() {

	auto* stage0 = AddStage("Map1");
	//auto* stage0 = AddStage("suzuki");

	stage0->LoadMiniMap("MapData/Map1_MiniMap.png");

	auto* stage1 = AddStage("Map2");
}

StageData* StageDataManager::GetStageData(int hStage) const {

	if (hStage < 0 || hStage >= stageData_.size())	return nullptr;
	if (!stageData_[hStage])						return nullptr;

	return stageData_[hStage].get();
}

StageData* StageDataManager::GoForword() {

	++currentStage_;
	currentStage_ %= stageData_.size();
	return stageData_[currentStage_].get();
}

StageData* StageDataManager::GoBack() {

	currentStage_ += static_cast<int>(stageData_.size());
	--currentStage_;
	currentStage_ %= stageData_.size();
	return stageData_[currentStage_].get();
}

StageData* StageDataManager::AddStage(const std::string& filePath) {
	
	//現在はステージのモデルのみ　いずれBGM、背景に対応
	auto stageDatum = std::make_unique<StageData>();
	stageDatum->LoadModel(filePath);
	auto* pStageDatum = stageDatum.get();
	stageData_.emplace_back(std::move(stageDatum));
	
	return pStageDatum;
}

StageDataManager* StageDataManager::GetInstance() {

	if(!instance_) {

		struct A : StageDataManager {};
		instance_ = std::make_unique<A>();
		instance_->Initialize();
	}
	return instance_.get();
}

void StageDataManager::DestroyInstance() {

	instance_.reset();
}

StageDataManager::StageDataManager()
	: currentStage_(0) {}

StageDataManager::~StageDataManager() = default;
