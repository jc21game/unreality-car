#pragma once
#include <memory>
#include <vector>
#include <string>
#include "StageData.h"

class StageDataManager {

	//StageData型のスマートポインタ
	using StageDataPtr = std::unique_ptr<StageData>;
	
private:
	std::vector<StageDataPtr> stageData_;	//ステージデータコンテナ
	int currentStage_;						//現在選ばれてるステージ

	//インスタンス
	static std::unique_ptr<StageDataManager> instance_;

public:
	/**
	 * \brief 初期化処理
	 */
	void Initialize();


	/**
	 * \brief ステージデータを取得する
	 * \param hStage ステージデータハンドル
	 * \return ステージデータ
	 */
	StageData* GetStageData(int hStage) const;

	/**
	 * \brief 読み込まれているステージの数を取得する
	 * \return サイズ
	 */
	int GetSize() const { return static_cast<int>(stageData_.size()); }


	/**
	 * \brief 現在選ばれているステージデータを取得する
	 * \return ステージデータ
	 */
	StageData* GetCurrentStageData() const { return stageData_[currentStage_].get(); };
	
	/**
	 * \brief 現在選ばれているステージを一つ先のものにする
	 * \return 現在選ばれているステージデータ
	 */
	StageData* GoForword();

	/**
	 * \brief 現在選ばれているステージを一つ前のものにする
	 * \return 現在選ばれているステージデータ
	 */
	StageData* GoBack();

private:
	/**
	 * \brief ステージデータを追加する
	 * \param filePath 読み込むステージデータのパス
	 * \return 追加されたステージ
	 */
	StageData* AddStage(const std::string& filePath);

public:
	/**
	 * \brief インスタンスを取得
	 * \return インスタンスのポインタ
	 */
	static StageDataManager* GetInstance();

	/**
	 * \brief インスタンスを破壊する
	 */
	static void DestroyInstance();

private:
	//コンストラクタ
	StageDataManager();

public:
	// コピー禁止
	StageDataManager(const StageDataManager&) = delete;
	StageDataManager& operator=(const StageDataManager&) = delete;

	// ムーブ禁止
	StageDataManager(StageDataManager&&) = delete;
	StageDataManager& operator=(StageDataManager&&) = delete;

	//デストラクタ
	~StageDataManager();
};

