/**
 * 2Dテクスチャの解像度を変更するシェーダー
 */

 //───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
 //───────────────────────────────────────
Texture2D g_texture : register(t0); // テクスチャー
SamplerState g_sampler : register(s0); // テクスチャーサンプラー

//x:screenWidth, y:screenHeight
cbuffer global
{
    float4 screenSize;
};

struct VS_OUTPUT
{
    float4 pos : SV_POSITION; // 位置
    float2 uv : TEXCOORD; // UV座標
};

VS_OUTPUT VS(float4 pos : POSITION, float4 uv : TEXCOORD)
{

    VS_OUTPUT output;
    output.pos = pos;
    output.uv = uv;
    return output;
}

typedef VS_OUTPUT PS_IN;

#define R_LUMINANCE 0.298912
#define G_LUMINANCE 0.586611
#define B_LUMINANCE 0.114478

//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
float4 PS(PS_IN input) : SV_TARGET
{
    
    float x = floor(input.uv.x * screenSize.x);
    float y = floor(input.uv.y * screenSize.y);
    float4x4 m = float4x4(
        0.f,  8.f,  2.f,  10.f,
        12.f, 4.f,  14.f, 6.f,
        3.f,  11.f, 1.f,  9.f,
        15.f, 7.f,  13.f, 5.f
    );
    x = floor(fmod(x, 4.0));
    y = floor(fmod(y, 4.0));
    float threshold = 0.f;
    
    threshold = m[x][y];
    
    float4 color = g_texture.Sample(g_sampler, input.uv);
    color *= 16.f;
    color.a = 1.f;
    float v = color.r * R_LUMINANCE + color.g * G_LUMINANCE + color.b * B_LUMINANCE;
    if (v < threshold)
    {
        color.rgb = 0.f;
    }
    else
    {
        color.rgb = 1.f;
    }
    return color;
}