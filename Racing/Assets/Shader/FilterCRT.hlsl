/**
 * 2Dテクスチャのブラウン管にするシェーダー
 */


 //───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
 //───────────────────────────────────────
Texture2D g_texture : register(t0); // テクスチャー
SamplerState g_sampler : register(s0); // テクスチャーサンプラー

cbuffer global
{
    float4 time;
};

struct VS_OUTPUT
{
    float4 pos : SV_POSITION; // 位置
    float2 uv : TEXCOORD; // UV座標
};

VS_OUTPUT VS(float4 pos : POSITION, float4 uv : TEXCOORD)
{

    VS_OUTPUT output;
    output.pos = pos;
    output.uv = uv;
    return output;
}

typedef VS_OUTPUT PS_IN;

//RGBそれぞれの輝度
#define R_LUMINANCE 0.298912
#define G_LUMINANCE 0.586611
#define B_LUMINANCE 0.114478

//uv座標を春型に変形する
float2 BARREL(float2 uv)
{
    float s1 = 0.99f;
    float s2 = 0.125f;
    
    float2 center = 2.f * uv - 1;
    
    float barrel = min(1.f - length(center) * s1, 1.f) * s2;
    
    return uv - center * barrel;

}

//走査線を加える関数
float SCANLINE(float2 uv)
{
    float scanline = clamp(0.95f + 0.05f * cos(3.14f * (uv.y + 0.008f * floor(time.x * 300.f) / 15.f) * 240.0 * 1.f), 0.f, 1.f);
    float grille = 0.85f + 0.15f * clamp(1.5f * cos(3.14f * uv.x * 640.f * 1.f), 0.f, 1.f);
    
    return scanline * grille * 1.2f;
}

//ヴィネット(周りを黒くする)
float2 CRT_VIGNETTE(float2 uv)
{
    float2 nu = uv * 2.f - 1.f;
    float2 offset = abs(nu.yx) / float2(6.f, 4.f);
    nu += nu * offset * offset;
    return nu;
}

//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
float4 PS(PS_IN input) : SV_TARGET
{

    float2 uv = BARREL(input.uv);
    
    float4 color = g_texture.Sample(g_sampler, uv);
    
    color.rgb *= float3(1.25f, 0.95f, 0.7f);
    color.rgb = clamp(color.rgb, 0.f, 1.f);
    color.rgb = color.rgb * color.rgb * (3.f - 2.f * color.rgb);
    color.rgb = 0.2f + 0.8f * color.rgb;
    
    color.rgb *= SCANLINE(uv);
    
    float2 crt = CRT_VIGNETTE(input.uv);
    crt = abs(crt);
    crt = pow(crt, 15.f);
    color.rgb = lerp(color.rgb, float3(0.f, 0.f, 0.f), crt.x + crt.y);
    
    color.a = 1.f;
    return color;
}