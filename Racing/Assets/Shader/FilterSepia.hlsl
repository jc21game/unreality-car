/**
 * 2Dテクスチャの色をセピア調にするシェーダー
 */

 //───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
 //───────────────────────────────────────
Texture2D g_texture : register(t0); // テクスチャー
SamplerState g_sampler : register(s0); // テクスチャーサンプラー

struct VS_OUTPUT {
	float4 pos : SV_POSITION; // 位置
	float2 uv : TEXCOORD; // UV座標
};

VS_OUTPUT VS(float4 pos : POSITION, float4 uv : TEXCOORD) {

	VS_OUTPUT output;
	output.pos = pos;
	output.uv = uv;
	return output;
}

typedef VS_OUTPUT PS_IN;

//RGBそれぞれの輝度
#define R_LUMINANCE 0.298912
#define G_LUMINANCE 0.586611
#define B_LUMINANCE 0.114478

//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
float4 PS(PS_IN input) : SV_TARGET{

	float4 color = g_texture.Sample(g_sampler, input.uv);
	float v = color.r * R_LUMINANCE + color.g * G_LUMINANCE + color.b * B_LUMINANCE;
	color.r = v;
	color.g = v * 0.8;
	color.b = v * 0.5;
	color.a = 1.f;
	return color;
}