/**
 * 2Dテクスチャの色を変更するシェーダー
 */

 //───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
 //───────────────────────────────────────
Texture2D g_texture : register(t0); // テクスチャー
SamplerState g_sampler : register(s0); // テクスチャーサンプラー

cbuffer global {
	float4 time;
};

struct VS_OUTPUT {
	float4 pos : SV_POSITION; // 位置
	float2 uv : TEXCOORD; // UV座標
};

VS_OUTPUT VS(float4 pos : POSITION, float4 uv : TEXCOORD) {

	VS_OUTPUT output;
	output.pos = pos;
	output.uv = uv;
	return output;
}

typedef VS_OUTPUT PS_IN;

float3 RGB_TO_HSV(float3 rgb)
{
    float3 hsv;
    float min, max, delta;
	
    min = rgb.r < rgb.g ? rgb.r : rgb.g;
    min = min < rgb.b ? min : rgb.b;
	
    max = rgb.r > rgb.g ? rgb.r : rgb.g;
    max = max > rgb.b ? max : rgb.b;
	
    hsv.z = max;
    delta = max - min;
    if (delta < 0.00001f)
    {
        hsv.x = 0.f;
        hsv.y = 0.f;
        return hsv;
    }
    if (max > 0.f)
    {
        hsv.y = delta / max;
    }
    else
    {
        hsv.y = 0.f;
        hsv.x = 0.f;
        return hsv;
    }
    if (rgb.r >= max)
    {
        hsv.x = (rgb.g - rgb.b) / delta;
    }
    else
    {
        if (rgb.g >= max)
        {
            hsv.x = 2.0 + (rgb.b - rgb.r) / delta;
        }
        else
        {
            hsv.x = 4.0 + (rgb.r - rgb.g) / delta;
        }
    }
    hsv.x *= 60.f;
	
    if (hsv.x < 0.f)
    {
        hsv.x += 360.f;
    }
    return hsv;
}

float3 HSV_TO_RGB(float3 hsv)
{
    float hh, p, q, t, ff;
    int i;
    float3 rgb;

    if (hsv.x <= 0.0)
    { // < is bogus, just shuts up warnings
        rgb.r = hsv.z;
        rgb.g = hsv.z;
        rgb.b = hsv.z;
        return rgb;
    }
    hh = hsv.x;
    if (hh >= 360.0)
        hh = fmod(hh, 360.f);
    hh /= 60.0;
    i = (int)hh;
    ff = hh - i;
    p = hsv.z * (1.0 - hsv.y);
    q = hsv.z * (1.0 - (hsv.y * ff));
    t = hsv.z * (1.0 - (hsv.y * (1.0 - ff)));

    switch (i)
    {
        case 0:
            rgb.r = hsv.z;
            rgb.g = t;
            rgb.b = p;
            break;
        case 1:
            rgb.r = q;
            rgb.g = hsv.z;
            rgb.b = p;
            break;
        case 2:
            rgb.r = p;
            rgb.g = hsv.z;
            rgb.b = t;
            break;
        case 3:
            rgb.r = p;
            rgb.g = q;
            rgb.b = hsv.z;
            break;
        case 4:
            rgb.r = t;
            rgb.g = p;
            rgb.b = hsv.z;
            break;
        case 5:
        default:
            rgb.r = hsv.z;
            rgb.g = p;
            rgb.b = q;
            break;
    }
    return rgb;
}

//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
float4 PS(PS_IN input) : SV_TARGET {

	float4 color = g_texture.Sample(g_sampler, input.uv);
    float3 hsv = RGB_TO_HSV(color.rbg);
    hsv.x += time.x;
    color.rgb = HSV_TO_RGB(hsv);
	color.a = 1.f;
	return color;
}