/**
 * 2Dテクスチャの色を反転するシェーダー
 */


 //───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
 //───────────────────────────────────────
Texture2D g_texture : register(t0); // テクスチャー
SamplerState g_sampler : register(s0); // テクスチャーサンプラー

struct VS_OUTPUT
{
    float4 pos : SV_POSITION; // 位置
    float2 uv : TEXCOORD; // UV座標
};

VS_OUTPUT VS(float4 pos : POSITION, float4 uv : TEXCOORD) {
	
    VS_OUTPUT output;
    output.pos = pos;
    output.uv = uv;
    return output;
}

typedef VS_OUTPUT PS_IN;

//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
float4 PS(PS_IN input) : SV_TARGET {

    float4 color = g_texture.Sample(g_sampler, input.uv);
    color.rgb = 1.f - color.rgb;
	color.a = 1.f;
    return color;
}