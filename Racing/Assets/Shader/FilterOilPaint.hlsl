/**
 * 2Dテクスチャの色を白黒にするシェーダー
 */


 //───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
 //───────────────────────────────────────
Texture2D g_texture : register(t0); // テクスチャー
Texture2D g_map : register(t1); //ノイズマップ
SamplerState g_sampler : register(s0); // テクスチャーサンプラー

struct VS_OUTPUT {
	float4 pos : SV_POSITION; // 位置
	float2 uv : TEXCOORD; // UV座標
};

VS_OUTPUT VS(float4 pos : POSITION, float4 uv : TEXCOORD) {

	VS_OUTPUT output;
	output.pos = pos;
	output.uv = uv;
	return output;
}

typedef VS_OUTPUT PS_IN;

//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
float4 PS(PS_IN input) : SV_TARGET{

	float gradientStep = 0.003f;

	float4 cxp = g_map.Sample(g_sampler, input.uv + float2(gradientStep, 0.f));
	float4 cxn = g_map.Sample(g_sampler, input.uv - float2(gradientStep, 0.f));
	float4 cyp = g_map.Sample(g_sampler, input.uv + float2(0.f, gradientStep));
	float4 cyn = g_map.Sample(g_sampler, input.uv - float2(0.f, gradientStep));

	float3 origin = float3(0.f, 0.f, 0.f);

	float dxp = distance(origin, cxp.xyz);
	float dxn = distance(origin, cxn.xyz);
	float dyp = distance(origin, cyp.xyz);
	float dyn = distance(origin, cyn.xyz);

	float advectStep = 0.002f;

	float2 grad = float2(dxp - dxn, dyp - dyn);
	float2 newuv = input.uv + (advectStep * normalize(grad));

	float4 color = g_texture.Sample(g_sampler, newuv);
	color.a = 1.f;

	return color;
}