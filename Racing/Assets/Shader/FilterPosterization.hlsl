/**
 * 2Dテクスチャの色を制限するシェーダー
 */

 //───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
 //───────────────────────────────────────
Texture2D g_texture : register(t0); // テクスチャー
SamplerState g_sampler : register(s0); // テクスチャーサンプラー

struct VS_OUTPUT
{
    float4 pos : SV_POSITION; // 位置
    float2 uv : TEXCOORD; // UV座標
};

VS_OUTPUT VS(float4 pos : POSITION, float4 uv : TEXCOORD)
{

    VS_OUTPUT output;
    output.pos = pos;
    output.uv = uv;
    return output;
}

typedef VS_OUTPUT PS_IN;

#define R_LUMINANCE 0.298912
#define G_LUMINANCE 0.586611
#define B_LUMINANCE 0.114478

float TRI_VALUED(float input) {

    if (input > 0.66666f)
        return 1.f;
	if (input > 0.33333f)
        return 0.5;
    return 0.f;
}

//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
float4 PS(PS_IN input) : SV_TARGET
{
    float4 color = g_texture.Sample(g_sampler, input.uv);
    color.r = TRI_VALUED(color.r);
    color.g = TRI_VALUED(color.g);
    color.b = TRI_VALUED(color.b);
    color.a = 1.f;

    return color;
}