/**
 * 2Dテクスチャの色を白黒にするシェーダー
 */


 //───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
 //───────────────────────────────────────
Texture2D g_texture : register(t0); // テクスチャー
Texture2D g_map : register(t1);	//ノイズマップ
SamplerState g_sampler : register(s0); // テクスチャーサンプラー

cbuffer global
{
	float4 time;
};

struct VS_OUTPUT
{
	float4 pos : SV_POSITION; // 位置
	float2 uv : TEXCOORD; // UV座標
};

VS_OUTPUT VS(float4 pos : POSITION, float4 uv : TEXCOORD)
{

	VS_OUTPUT output;
	output.pos = pos;
	output.uv = uv;
	return output;
}

typedef VS_OUTPUT PS_IN;

//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
float4 PS(PS_IN input) : SV_TARGET
{

	float speed = 0.045f;
	float noiseAmount = 0.01f;
	
	float2 nUv = input.uv;
	nUv.y += time.x * speed;
	
	float4 uvNoise = 2 * g_map.Sample(g_sampler, nUv) - 1;

	input.uv += uvNoise.xy * noiseAmount;
	
	float4 color = g_texture.Sample(g_sampler, input.uv);
	color.a = 1.f;

	return color;
}