/**
 * 2Dテクスチャの解像度を変更するシェーダー
 */

 //───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
 //───────────────────────────────────────
Texture2D g_texture : register(t0); // テクスチャー
SamplerState g_sampler : register(s0); // テクスチャーサンプラー

//x:screenWidth, y:screenHeight, z:time
cbuffer global
{
    float4 data;
};

struct VS_OUTPUT
{
    float4 pos : SV_POSITION; // 位置
    float2 uv : TEXCOORD; // UV座標
};

VS_OUTPUT VS(float4 pos : POSITION, float4 uv : TEXCOORD)
{

    VS_OUTPUT output;
    output.pos = pos;
    output.uv = uv;
    return output;
}

typedef VS_OUTPUT PS_IN;

//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
float4 PS(PS_IN input) : SV_TARGET
{
    float mosaicScale = clamp(sin(data.z * 0.7) * 12.f , 1.f, 7.f);
    float2 uv = input.uv;
    uv.x = floor(uv.x * data.x / mosaicScale) / (data.x / mosaicScale) + (mosaicScale * 0.5f) / data.x;
    uv.y = floor(uv.y * data.y / mosaicScale) / (data.y / mosaicScale) + (mosaicScale * 0.5f) / data.y;
    float4 color = g_texture.Sample(g_sampler, uv);
    color.a = 1.f;
    return color;
}