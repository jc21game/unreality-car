#pragma once
#include "IButton.h"
class CustomButton :
	public IButton {

public:
	CustomButton(std::string fileName, DirectX::XMFLOAT2 pos) : IButton(fileName, pos) {};

	virtual void Execute() override;
};

