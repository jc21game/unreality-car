#include "StartScene.h"
#include "BackGround.h"
#include "DisplayCar.h"
#include "pedestal.h"
#include "Cursor.h"
#include "../Engine/SceneManager.h"


StartScene::StartScene(GameObject* parent) :
	GameObject(parent, "StartScene") {
}

StartScene::~StartScene() {
}

void StartScene::Initialize() {
	//todo Endo

	//背景
	Instantiate<BackGround>(this);

	//車
	Instantiate<DisplayCar>(this);

	//台座
	//テスト表示
	//Instantiate<Pedestal>(this);

	//ボタン
	Instantiate<Cursor>(this);
}

void StartScene::Update() {
}

void StartScene::Draw() {
}

void StartScene::Release() {
}
