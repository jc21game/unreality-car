#pragma once
#include "IButton.h"

class GameObject;
class TurnLeftButton :
	public IButton {

private:
	GameObject* pTurnTarget_;
public:
	TurnLeftButton(std::string fileName, DirectX::XMFLOAT2 pos, GameObject* p) : IButton(fileName, pos) ,pTurnTarget_(p) {};

	virtual void Execute() override;
};

