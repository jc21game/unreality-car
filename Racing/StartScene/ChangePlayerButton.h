#pragma once
#include "IButton.h"
class ChangePlayerButton :
	public IButton {

public:
	ChangePlayerButton(std::string fileName, DirectX::XMFLOAT2 pos) : IButton(fileName, pos) {};

	virtual void Execute() override;
};

