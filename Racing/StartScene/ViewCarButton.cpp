#include "ViewCarButton.h"
#include "Cursor.h"
#include "DisplayCar.h"

#include <iostream>	//実行確認用

void ViewCarButton::Execute() {

	//モード切替
	pCursor_->ToggleViewMode();
	DisplayCar* pDisplayCar = (DisplayCar*)pCursor_->FindObject("DisplayCar");
	/*XMVECTOR scale = pDisplayCar->GetScale();
	scale *= 1.2f;
	pDisplayCar->SetScale(scale);*/
	
	pDisplayCar->SetTransformViewMode();
}
