#include "DisplayCar.h"
#include "../CustomCar/CustomCar.h"
#include "../Engine/Model.h"

//コンストラクタ
DisplayCar::DisplayCar(GameObject * parent)
	:GameObject(parent, "DisplayCar"), hModel_(-1) {
}

//デストラクタ
DisplayCar::~DisplayCar() {
}

//初期化
void DisplayCar::Initialize() {
	//モデルデータのロード
	//hModel_ = Model::Load("SampleResource/torakku.fbx");
	//assert(hModel_ >= 0);

	//transform_.position_.vecX += 1.5;
	transform_.scale_ = XMVectorReplicate(1.3f);
	transform_.position_ = XMVectorSet(-2.5f, -1.f, 0, 0);
	transform_.rotate_.vecY = 125; //135.f;
	transform_.rotate_.vecX = 0;
}

//更新
void DisplayCar::Update() {
}

//描画
void DisplayCar::Draw() {

	//Model::SetTransform(hModel_, transform_);

	//1Pのカスタム情報
	CustomCar::GetCurrentCustom()->Draw(transform_);

	//2Pのカスタム情報
	//auto&& p = CustomCar::GetPlayerCustom(CUSTOM_SECOND);
	//p->Draw(transform_);
}

//開放
void DisplayCar::Release() {
}

void DisplayCar::SetTransformViewMode() {

	transform_.position_ = XMVectorSet(0, -1.f, 0, 0);
	//transform_.scale_ *= 1.2f;
}
