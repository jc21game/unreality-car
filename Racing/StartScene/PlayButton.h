#pragma once
#include "IButton.h"
class PlayButton :
	public IButton {

public:
	PlayButton(std::string fileName, DirectX::XMFLOAT2 pos) : IButton(fileName, pos) {};

	virtual void Execute() override;
};

