#pragma once
#include "IButton.h"

class Cursor;

class VBackButton :
	public IButton {

private:
	Cursor* pCursor_;

public:
	VBackButton(std::string fileName, DirectX::XMFLOAT2 pos, Cursor* pCursor)
		: IButton(fileName, pos)
		, pCursor_(pCursor) {};

	virtual void Execute() override;
};