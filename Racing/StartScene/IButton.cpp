#include "IButton.h"

#include "../Engine/Image.h"
#include "../Engine/Transform.h"

//引数　ポジション情報
IButton::IButton(std::string fileName, DirectX::XMFLOAT2 pos)
	: pos_(pos)
	, hImage_(-1)
	, fileName_(fileName) {
}

void IButton::Load() {

	hImage_ = Image::Load(fileName_);
	assert(hImage_ >= 0);
}

void IButton::Draw(Transform & transform) {

	Transform trans = transform;
	trans.position_ += DirectX::XMLoadFloat2(&pos_);

	Image::SetTransform(hImage_, trans);
	Image::Draw(hImage_);
}
