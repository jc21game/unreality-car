#include "CustomButton.h"
#include "../Engine/SceneManager.h"

#include <iostream>	//実行確認用

void CustomButton::Execute() {

	std::cout << fileName_;	//実行確認用


	//カスタム画面に移行

	SceneManager* pSceneManager = SceneManager::GetInstance();
	pSceneManager->ChangeScene(SCENE_ID_CUSTOM);

}
