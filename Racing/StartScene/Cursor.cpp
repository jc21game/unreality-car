#include <iostream>

#include "Cursor.h"
#include "../Engine/Input.h"
#include "../Engine/SceneTimer.h"
#include "IButton.h"

#include "OptionButton.h"
#include "CustomButton.h"
#include "ViewCarButton.h"
#include "PlayButton.h"
#include "ChangePlayerButton.h"
#include "FilterButtom.h"

#include "VBackButton.h"
#include "TurnRightButton.h"
#include "TurnLeftButton.h"

#include "OBackButton.h"
#include "NextPageButton.h"
#include "PrevPageButton.h"

Cursor::Cursor(GameObject * parent)
	: GameObject(parent, "")
	, currentButton_(3u)
	, isViewMode_(false) {
}

Cursor::~Cursor() {

}

// ボタンの列挙型
enum BUTTONS {

	//OPTION = 0,
	VIEW_CAR,
	CUSTOM,
	CHANGE_PLAYER,
	PLAY,
	FILTER,
	BUTTONS_MAX
};

enum VIEW_MODE_BUTTONS {

	V_BACK_MODE = 0,
	V_RIGHT,
	V_LEFT,
	VIEW_MODE_BUTTONS_MAX
};

enum OPTION_MODE_BUTTONS {

	O_BACK_MODE = 0,
	O_RIGHT,
	O_LEFT,
	OPTION_MODE_BUTTONS_MAX
};

void Cursor::Initialize(void) {

	//ボタンの追加
	//buttons_.emplace_back(new OptionButton("SampleResource\\Option.png", { -0.85f, 0.8f }, this));
	buttons_.emplace_back(new ViewCarButton("SampleResource\\View.png", { 0.35f, -0.35f }, this));
	buttons_.emplace_back(new CustomButton("SampleResource\\Custom.png", { 0.8f, -0.3f }));
	buttons_.emplace_back(new ChangePlayerButton("SampleResource\\Change1.png", { -0.85f, -0.73f }));
	buttons_.emplace_back(new PlayButton("SampleResource\\Play.png", { 0.7f, -0.8f }));
	buttons_.emplace_back(new FilterButtom("SampleResource/Effect.png", { 0.8f, 0.25f }));

	viewModeButtons_.emplace_back(new VBackButton("SampleResource\\Return.png", { -0.85f, 0.7f }, this));
	viewModeButtons_.emplace_back(new TurnRightButton("SampleResource\\Right.png", { 0.85f, 0.f }, this));
	viewModeButtons_.emplace_back(new TurnLeftButton("SampleResource\\Left.png", { -0.85f, 0.f }, this));

	optionModeButtons_.emplace_back(new OBackButton("SampleResource\\Return.png", { -0.85f, 0.8f }, this));
	optionModeButtons_.emplace_back(new NextPageButton("SampleResource\\Right.png", { 0.8f, 0.f }));
	optionModeButtons_.emplace_back(new PrevPageButton("SampleResource\\Left.png", { -0.8f, 0.f }));

	for (auto&& button : buttons_) {

		button->Load();
	}
	for (auto&& button : viewModeButtons_) {

		button->Load();
	}
	for (auto&& button : optionModeButtons_) {

		button->Load();
	}

	transform_.scale_ = XMVectorReplicate(0.35f);
}

void Cursor::Update(void) {
	//todo ボタンの操作処理
	//todo コントローラ操作に変更

	//鑑賞モード
	if (isViewMode_) {
		/*
		if (Input::IsKeyDown(DIK_RETURN)) {

			viewModeButtons_[currentButton_]->Execute();
		}*/

		if (Input::IsKeyDown(DIK_ESCAPE)) {

			viewModeButtons_[V_BACK_MODE]->Execute();
		}

		if (Input::IsKey(DIK_RIGHT)) {

			viewModeButtons_[V_RIGHT]->Execute();
		}
		if (Input::IsKey(DIK_LEFT)) {

			viewModeButtons_[V_LEFT]->Execute();
		}

		/*
		if (Input::IsKeyDown(DIK_RIGHT)) {

			switch (currentButton_) {

			case BACK_MODE: currentButton_ = LEFT; break;
			case RIGHT:  break;
			case LEFT: currentButton_ = RIGHT; break;
			default: break;
			}
		}
		*/
		/*
		if (Input::IsKeyDown(DIK_LEFT)) {

			switch (currentButton_) {

			case BACK_MODE: break;
			case RIGHT: currentButton_ = LEFT; break;
			case LEFT: currentButton_ = BACK_MODE; break;
			default: break;
			}
		}*/
	}
	//オプションモード
	else if (isOptionMode_) {

		if (Input::IsKeyDown(DIK_ESCAPE)) {

			optionModeButtons_[O_BACK_MODE]->Execute();
		}

		if (Input::IsKeyDown(DIK_RIGHT)) {

			optionModeButtons_[O_RIGHT]->Execute();
		}
		if (Input::IsKeyDown(DIK_LEFT)) {

			optionModeButtons_[O_LEFT]->Execute();
		}
	}
	//ホーム
	else {
		//Spaceキー　順番に選ばれる
		if (Input::IsKeyDown(DIK_SPACE)) {

			currentButton_++;
			currentButton_ %= buttons_.size();
			std::cout << currentButton_;
		}

		//Enterキー	決定
		if (Input::IsKeyDown(DIK_RETURN)) {

			buttons_[currentButton_]->Execute();
		}

		//上キー
		if (Input::IsKeyDown(DIK_UP)) {

			switch (currentButton_) {

			case VIEW_CAR:		currentButton_ = FILTER; break;
			case CHANGE_PLAYER:	/*currentButton_ = OPTION;*/ break;
			case PLAY:			currentButton_ = CUSTOM; break;
			//case OPTION:	break;
			case CUSTOM:		currentButton_ = FILTER;	break;
			case FILTER:
			default:	break;
			}
		}

		//下キー
		if (Input::IsKeyDown(DIK_DOWN)) {

			switch (currentButton_) {

			//case OPTION: currentButton_ = CHANGE_PLAYER; break;
			case VIEW_CAR:
			case CUSTOM: currentButton_ = PLAY; break;
			case CHANGE_PLAYER:
			case PLAY:	break;
			case FILTER:	currentButton_ = CUSTOM;	break;
			default:		break;
			}
		}

		//右キー
		if (Input::IsKeyDown(DIK_RIGHT)) {

			switch (currentButton_) {

			//case OPTION:
			case VIEW_CAR:
			case CHANGE_PLAYER:	currentButton_ = PLAY; break;
			case CUSTOM:
			case PLAY:		break;
			case FILTER:
			default:		break;
			}
		}

		//左キー
		if (Input::IsKeyDown(DIK_LEFT)) {

			switch (currentButton_) {

			//case OPTION:
			case CHANGE_PLAYER:	break;
			case VIEW_CAR:currentButton_ = CHANGE_PLAYER; break;
			case CUSTOM:currentButton_ = VIEW_CAR;	break;
			case PLAY:	currentButton_ = CHANGE_PLAYER; break;
			case FILTER:	currentButton_ = VIEW_CAR;	break;
			default:			break;
			}
		}
	}

}

void Cursor::Draw() {

	if (isViewMode_) {
		for (auto i = 0u; i < VIEW_MODE_BUTTONS_MAX; ++i) {

			if (i == currentButton_) {

				Transform trans = transform_;
				trans.scale_ *= 1.1f + sin(SceneTimer::GetElapsedSecounds() * 5) * 0.1f;
				viewModeButtons_[i]->Draw(trans);
				continue;
			}
			viewModeButtons_[i]->Draw(transform_);
		}
	}
	else if (isOptionMode_) {
		for (auto i = 0u; i < OPTION_MODE_BUTTONS_MAX; ++i) {

			if (i == currentButton_) {

				Transform trans = transform_;
				trans.scale_ *= 1.1f;
				buttons_[i]->Draw(trans);
				continue;
			}
			optionModeButtons_[i]->Draw(transform_);
		}
	}
	else {
		for (auto i = 0u; i < BUTTONS_MAX; ++i) {

			if (i == currentButton_) {

				Transform trans = transform_;
				trans.scale_ *= 1.1f + sin(SceneTimer::GetElapsedSecounds() * 5) * 0.1f;
				buttons_[i]->Draw(trans);
				continue;
			}
			buttons_[i]->Draw(transform_);
		}
	}


}

void Cursor::Release(void) {

	for (auto&& button : buttons_) {

		delete button;
	}
	for (auto&& button : viewModeButtons_) {

		delete button;
	}
	for (auto&& button : optionModeButtons_) {

		delete button;
	}
}

void Cursor::ToggleViewMode()
{
	isViewMode_ = !isViewMode_;
	currentButton_ = isViewMode_ ? V_BACK_MODE : VIEW_CAR;
}

void Cursor::ToggleOptionMode()
{
	isOptionMode_ = !isOptionMode_;
	currentButton_ = isOptionMode_ ? O_BACK_MODE : VIEW_CAR;
}
