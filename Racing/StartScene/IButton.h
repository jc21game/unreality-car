#pragma once

#include <string>
#include <DirectXMath.h>

class Transform;

class IButton {

protected:
	int					hImage_;
	DirectX::XMFLOAT2	pos_;
	std::string			fileName_;

public:
	IButton(std::string fileName, DirectX::XMFLOAT2 pos);

	void Load();
	void Draw(Transform& transform);
	virtual void Execute() = 0;			//ボタンの所持機能を実行
};

