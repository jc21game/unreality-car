#include "BackGround.h"
//#include "../Engine/Model.h"
#include "../Engine/Image.h"

//コンストラクタ
BackGround::BackGround(GameObject * parent)
	:GameObject(parent, "BackGround"), hModel_(-1), hPict_(-1){
}

//デストラクタ
BackGround::~BackGround(){
}

//初期化
void BackGround::Initialize(){
	//モデルデータのロード
	/*hModel_ = Model::Load("Model/background.fbx");
	assert(hModel_ >= 0);

	transform_.position_.vecZ = 20;*/

	//画像データのロード
	hPict_ = Image::Load("SampleResource/BGI04.png");
	assert(hPict_ >= 0);
	Image::SetTransformFullSize(hPict_);
}

//更新
void BackGround::Update(){
}

//描画
void BackGround::Draw() {
	/*Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);*/

	//Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void BackGround::Release(){
}


