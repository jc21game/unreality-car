#pragma once
#include "../Engine/GameObject.h"

//車表示を管理するクラス
class DisplayCar : public GameObject {

	int hModel_;    //モデル番号

public:
	//コンストラクタ
	DisplayCar(GameObject* parent);

	//デストラクタ
	~DisplayCar();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void SetTransformViewMode();
};