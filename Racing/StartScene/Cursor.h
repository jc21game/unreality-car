#pragma once
#include "../Engine/GameObject.h"

#include <vector>

class IButton;

class Cursor
	: public GameObject {

private:
	std::vector<IButton*>	buttons_;			//用意されているボタン
	std::vector<IButton*>	viewModeButtons_;
	std::vector<IButton*>	optionModeButtons_;

	unsigned				currentButton_;		//選ばれているボタン(番号)
	bool					isViewMode_;		//鑑賞モードかどうか
	bool					isOptionMode_;		//オプションモードかどうか

public:
	Cursor(GameObject* parent);
	~Cursor();

	virtual void Initialize(void) override;
	virtual void Update(void) override;
	virtual void Draw() override;
	virtual void Release(void) override;

	void ToggleViewMode();
	void ToggleOptionMode();
};

