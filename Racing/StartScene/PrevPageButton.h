#pragma once
#include "IButton.h"

class PrevPageButton
	: public IButton {

public:
	PrevPageButton(std::string fileName, DirectX::XMFLOAT2 pos)
		: IButton(fileName, pos) {};

	virtual void Execute() override;

};

