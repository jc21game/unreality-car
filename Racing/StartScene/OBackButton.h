#pragma once
#include "IButton.h"

class Cursor;

class OBackButton
	: public IButton {

private:
	Cursor* pCursor_;

public:
	OBackButton(std::string fileName, DirectX::XMFLOAT2 pos, Cursor* pCursor)
		: IButton(fileName, pos)
		, pCursor_(pCursor) {};

	virtual void Execute() override;
};
