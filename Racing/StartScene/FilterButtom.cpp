#include "FilterButtom.h"

#include "../Engine/SceneManager.h"

#include <iostream>	//実行確認用

void FilterButtom::Execute() {


	//フィルタ選択画面に移行
	SceneManager* pSceneManager = SceneManager::GetInstance();
	pSceneManager->ChangeScene(SCENE_ID_FILTER);

}