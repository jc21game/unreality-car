#include "Pedestal.h"
#include "../Engine/Model.h"

//コンストラクタ
Pedestal::Pedestal(GameObject * parent)
	:GameObject(parent, "Pedestal"), hModel_(-1){
}

//デストラクタ
Pedestal::~Pedestal(){
}

//初期化
void Pedestal::Initialize(){
	//モデルデータのロード
	hModel_ = Model::Load("SampleResource/Pedestal.fbx");
	assert(hModel_ >= 0);

	//テスト位置
	transform_.position_.vecZ = 2;
	transform_.position_.vecY = -2;
}

//更新
void Pedestal::Update(){
	//テスト回転
	//transform_.rotate_.vecY += 1;
}

//描画
void Pedestal::Draw(){
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Pedestal::Release(){
}