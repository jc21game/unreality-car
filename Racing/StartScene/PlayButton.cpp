#include "PlayButton.h"
#include "../Engine/SceneManager.h"

#include <iostream>	//実行確認用

void PlayButton::Execute() {

	std::cout << fileName_;		//実行確認用

	//プレイ画面に移行

	SceneManager* pSceneManager = SceneManager::GetInstance();
	pSceneManager->ChangeScene(SCENE_ID_SELECT);
}
