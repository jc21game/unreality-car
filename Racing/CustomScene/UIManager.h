#pragma once
#include "../Engine/GameObject.h"

#include <string>
#include <array>

//画像の種類
enum IMAGE_TYPE {
	IMAGE_RETURN,
	IMAGE_PREVIEW,
	IMAGE_CUSTOMIZE,
	IMAGE_LEFT,
	IMAGE_RIGHT,
	IMAGE_BODY,
	IMAGE_WHEEL,
	IMAGE_MAX
};

class ImageObject;

//画像オブジェクトを管理する
class UIManager : public GameObject {

private:
	std::array<ImageObject*, IMAGE_MAX> images_;	//画像オブジェクトの参照

public:
	/**
	 * \brief コンストラクタ
	 * \param parent 親オブジェクト
	 */
	UIManager(GameObject* parent);
	
	/**
	 * \brief デストラクタ
	 */
	~UIManager();

	/**
	 * \brief 初期化処理
	 */
	virtual void Initialize() override;

	/**
	 * \brief 更新処理
	 */
	virtual void Update() override;

	/**
	 * \brief 描画処理
	 */
	virtual void Draw() override;

	/**
	 * \brief 解放処理
	 */
	virtual void Release() override;


	/**
	 * \brief 画像の表示を切り替える
	 * \param type 画像の種類
	 */
	void ToggleVisible(IMAGE_TYPE type);

	ImageObject* GetImageObject(IMAGE_TYPE type);

private:
	/**
	 * \brief 画像オブジェクト生成用関数
	 * \param name 画像オブジェクトの名前
	 * \param fileName 読み込みたい画像のパス
	 * \return 生成したオブジェクトの参照
	 */
	ImageObject* InstantiateImage( const std::string& name, const std::string& fileName);
};

