#include "Tab.h"

#include "../Engine/Input.h"

Tab::Tab(GameObject* parent, const std::string& name, size_t partSize)
	: GameObject(parent, name)
	, PART_SIZE(partSize), BASE_POSITION(XMVectorSet(-5.0f, -3.2f, 0.f, 0.f))
	, currentIndex_(0u) {
}

bool Tab::shouldSetup_ = true;

void Tab::Initialize() {

	//デフォルトで描画、更新を拒否
	LeaveSolo();
	Invisible();

	transform_.position_ = BASE_POSITION;
}

void Tab::Update() {

	if (shouldSetup_) {

		if (transform_.position_.vecY == BASE_POSITION.vecY) {

			transform_.position_.vecY -= 3.f;
		}
		
		transform_.position_.vecY += 0.3f;

		if (transform_.position_.vecY >= BASE_POSITION.vecY) {

			transform_.position_.vecY = BASE_POSITION.vecY;
			shouldSetup_ = false;
		}
		return;
	}

	//回転させて全体を見せる
	transform_.rotate_.vecY += 1.f;
	
	//一つでもパーツがロードされてるなら
	if (PART_SIZE) {

		auto prevIndex = currentIndex_;	//過去の状態保持

		//選択変更
		if (Input::IsKeyDown(DIK_RIGHT))	++currentIndex_;
		if (Input::IsKeyDown(DIK_LEFT))		--currentIndex_;

		//変更されたら範囲内にとどめる
		if(currentIndex_ != prevIndex)		currentIndex_ %= PART_SIZE;

		//決定されたらそのパーツで上書き
		if (Input::IsKeyDown(DIK_RETURN))		OverwritePart();
	}
}
