#include "TabBody.h"

#include "../Engine/SceneTimer.h"
#include "../CustomCar/CarPartManager.h"
#include "../CustomCar/CustomCar.h"

TabBody::TabBody(GameObject * parent) 
	: Tab(parent, "TabBody", CarPartManager::GetInstance()->GetBodesSize()) {
}

TabBody::~TabBody() = default;

void TabBody::Initialize() {

	Tab::Initialize();	//親クラスの初期化処理

	//todo ここから自分自身で必要な初期化処理
	transform_.scale_ *= 0.4f;
}

void TabBody::Draw() {

	//todo ここで選択できるパーツを描画
	auto&& carPartManager = CarPartManager::GetInstance();

	//変形情報をコピー
	Transform trans = transform_;

	//すべてのパーツを描画
	for (auto i = 0u; i < PART_SIZE; ++i) {

		//選択されているなら大きく表示
		if (i == currentIndex_) {

			Transform bigTrans = trans;
			bigTrans.scale_ *= 1.f + sin(SceneTimer::GetElapsedSecounds() * 5) * 0.2f;
			carPartManager->DrawBody(i, bigTrans);
		}
		else {

			carPartManager->DrawBody(i, trans);
		}
		//右へずらす
		trans.position_.vecX += 3;
	}
}

void TabBody::Release() {
}

void TabBody::OverwritePart() {

	//選択されているものを設定する
	CustomCar::GetCurrentCustom()->ChangeBody(static_cast<unsigned>(currentIndex_));
}
