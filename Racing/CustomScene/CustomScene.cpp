#include "CustomScene.h"

#include "../Engine/Input.h"
#include "../Engine/SceneManager.h"
#include "../Engine/Image.h"

#include "OrnamentalCar.h"
#include "TabManager.h"
#include "UIManager.h"
#include "ImageObject.h"

CustomScene::CustomScene(GameObject * parent)
	: GameObject(parent, "CustomScene")
	, hPictBackGround_(-1), currentIcon_(0)
	, pCar_(nullptr), pTabManager_(nullptr), pUIManager_(nullptr) {
}

CustomScene::~CustomScene() = default;

void CustomScene::Initialize() {

	//子クラスのオブジェクト生成
	pCar_		= Instantiate<OrnamentalCar>(this);
	pUIManager_ = Instantiate<UIManager>(this);
	pTabManager_ = Instantiate<TabManager>(this);

	pUIManager_->GetImageObject(IMAGE_PREVIEW)->ValidateVariableScaling();

	//背景用画像読み込み
	hPictBackGround_ = Image::Load("SampleResource/BGI04.png");
	assert(hPictBackGround_ >= 0);
	Image::SetTransformFullSize(hPictBackGround_);	//ウィンドウサイズに合わせる
}

void CustomScene::Update() {

	//アイコンごとの処理実行
	if (Input::IsKeyDown(DIK_RETURN)) {

		//フォーカスモードへ
		if (currentIcon_ == 0) {

			//画像表示の切り替え
			pUIManager_->ToggleVisible(IMAGE_CUSTOMIZE);
			pUIManager_->ToggleVisible(IMAGE_PREVIEW);
			pUIManager_->ToggleVisible(IMAGE_RIGHT);
			pUIManager_->ToggleVisible(IMAGE_LEFT);

			//描画更新の切り替え
			pCar_->FocusOn();
			LeaveSolo();
		}

		// カスタマイズモードへ
		if (currentIcon_ == 1) {

			//画像表示の切り替え
			pUIManager_->ToggleVisible(IMAGE_CUSTOMIZE);
			pUIManager_->ToggleVisible(IMAGE_PREVIEW);

			//描画更新の切り替え
			pTabManager_->EnterSolo();
			pTabManager_->Visible();
			LeaveSolo();
		}
	}

	if (Input::IsKeyDown(DIK_LEFT) || Input::IsKeyDown(DIK_RIGHT)) {

		if (currentIcon_ == 0) {
			pUIManager_->GetImageObject(IMAGE_PREVIEW)->InvalidateVariableScaling();
			pUIManager_->GetImageObject(IMAGE_CUSTOMIZE)->ValidateVariableScaling();
		}
		else {
			pUIManager_->GetImageObject(IMAGE_PREVIEW)->ValidateVariableScaling();
			pUIManager_->GetImageObject(IMAGE_CUSTOMIZE)->InvalidateVariableScaling();
		}
		++currentIcon_;
		currentIcon_ %= 2;
	}
	

	//シーン切り替え
	if (Input::IsKeyDown(DIK_ESCAPE)) {

		if (auto* pSceneManager = SceneManager::GetInstance()) {

			pSceneManager->ChangeScene(SCENE_ID_START);
		}
	}
}

void CustomScene::Draw() {

	Image::Draw(hPictBackGround_);
}

void CustomScene::Release() {
}
