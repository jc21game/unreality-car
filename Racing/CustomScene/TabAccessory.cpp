#include "TabAccessory.h"

#include "../Engine/SceneTimer.h"
#include "../CustomCar/CarPartManager.h"
#include "../CustomCar/CustomCar.h"

TabAccessory::TabAccessory(GameObject * parent)
	: Tab(parent, "TabAccessory", CarPartManager::GetInstance()->GetAccessoriesSize()) {
}

TabAccessory::~TabAccessory() = default;

void TabAccessory::Initialize() {

	Tab::Initialize();	//親クラスの初期化処理

	//todo ここから自分自身で必要な初期化処理

}

void TabAccessory::Draw() {

	//todo ここで選択できるパーツを描画
}

void TabAccessory::Release() {
}

void TabAccessory::OverwritePart() {

	CustomCar::GetCurrentCustom()->AddAccessory(static_cast<unsigned>(currentIndex_));
}
