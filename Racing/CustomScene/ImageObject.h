#pragma once
#include "../Engine/GameObject.h"

#include <string>


class ImageObject : public GameObject {

private:
	int hPict_;						//画像ハンドル
	const std::string fileName_;	//画像のファイルパス
	bool isVariableScaling_;

public:
	/**
	 * \brief コンストラクタ
	 * \param parent 親オブジェクト
	 * \param name オブジェクトの名前
	 * \param fileName 読み込む画像ファイルパス
	 */
	ImageObject(GameObject* parent, const std::string& name, const std::string& fileName);
	
	/**
	 * \brief デストラクタ
	 */
	~ImageObject();


	/**
	 * \brief 初期化処理
	 */
	virtual void Initialize() override;

	/**
	 * \brief 更新処理
	 */
	virtual void Update() override;

	/**
	 * \brief 描画処理
	 */
	virtual void Draw() override;

	/**
	 * \brief 解放処理
	 */
	virtual void Release() override;

	void ValidateVariableScaling();
	void InvalidateVariableScaling();
};

