#pragma once
#include "Tab.h"

//ボディパーツのtabを管理するクラス
class TabBody : public Tab {

public:
	/**
	 * \brief コンストラクタ
	 * \param parent 親オブジェクト
	 */
	TabBody(GameObject* parent);

	/**
	 * \brief デストラクタ
	 */
	virtual ~TabBody();
	

	/**
	* \brief 初期化処理
	*/
	virtual void Initialize() override;

	/**
	 * \brief 描画処理
	 */
	virtual void Draw() override;

	/**
	 * \brief 解放処理
	 */
	virtual void Release() override;
	
	/**
	 * \brief 選択された時の実行される処理
	 */
	virtual void OverwritePart() override;
};

