#pragma once
#include "../Engine/GameObject.h"

class TabManager;
class OrnamentalCar;
class UIManager;

class CustomScene : public GameObject {

private:
	int hPictBackGround_;		//背景画像用の画像ハンドル

	int currentIcon_;
	
	OrnamentalCar* pCar_;		//観賞用車オブジェクトの参照
	TabManager* pTabManager_;	//タブ管理オブジェクトの参照
	UIManager* pUIManager_;		//画像管理オブジェクトの参照
	
public:
	/**
	 * \brief コンストラクタ
	 * \param parent 親オブジェクト
	 */
	CustomScene(GameObject* parent);

	/**
	 * \brief デストラクタ
	 */
	~CustomScene();


	/**
	 * \brief 初期化処理
	 */
	virtual void Initialize() override;

	/**
	 * \brief 更新処理
	 */
	virtual void Update() override;

	/**
	 * \brief 描画処理
	 */
	virtual void Draw() override;

	/**
	 * \brief 解放処理
	 */
	virtual void Release() override;
};

