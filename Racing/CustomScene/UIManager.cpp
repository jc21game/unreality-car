#include "UIManager.h"

#include "ImageObject.h"

UIManager::UIManager(GameObject* parent)
	: GameObject(parent, "UIManager") {

	images_.fill(nullptr);
}

UIManager::~UIManager() = default;

void UIManager::Initialize() {

	//画像オブジェクトを生成し、変形情報を設定
	//汚いので要検討
	images_[IMAGE_RETURN] = InstantiateImage("ImageReturn", "SampleResource\\Return.png");
	images_[IMAGE_RETURN]->SetPosition(-0.85f, 0.75f, 0.f);
	images_[IMAGE_RETURN]->SetScale(XMVectorReplicate(0.35f));

	images_[IMAGE_PREVIEW] = InstantiateImage("ImagePreview", "SampleResource\\View2.png");
	images_[IMAGE_PREVIEW]->SetPosition(-0.75f, -0.8f, 0.f);
	images_[IMAGE_PREVIEW]->SetScale(XMVectorReplicate(0.55f));

	images_[IMAGE_CUSTOMIZE] = InstantiateImage("ImageCustom", "SampleResource\\Custom2.png");
	images_[IMAGE_CUSTOMIZE]->SetPosition(-0.3f, -0.8f, 0.f);
	images_[IMAGE_CUSTOMIZE]->SetScale(XMVectorReplicate(0.55f));

	images_[IMAGE_LEFT] = InstantiateImage("ImageLeft", "SampleResource\\Left.png");
	images_[IMAGE_LEFT]->SetPosition(-0.85f, 0.f, 0.f);
	images_[IMAGE_LEFT]->SetScale(XMVectorReplicate(0.3f));
	images_[IMAGE_LEFT]->Invisible();

	images_[IMAGE_RIGHT] = InstantiateImage("ImageRight", "SampleResource\\Right.png");
	images_[IMAGE_RIGHT]->SetPosition(0.85f, 0.f, 0.f);
	images_[IMAGE_RIGHT]->SetScale(XMVectorReplicate(0.3f));
	images_[IMAGE_RIGHT]->Invisible();

	images_[IMAGE_BODY] = InstantiateImage("ImageBody", "CustomTab\\TabBodyA.png");
	images_[IMAGE_BODY]->SetPosition(0.f, -0.62f, 0.f);
	images_[IMAGE_BODY]->SetScale(XMVectorSet(1.24f, 0.5, 0, 0));
	images_[IMAGE_BODY]->Invisible();

	images_[IMAGE_WHEEL] = InstantiateImage("ImageWheel", "CustomTab\\TabWheelA.png");
	images_[IMAGE_WHEEL]->SetPosition(0.f, -0.62f, 0.f);
	images_[IMAGE_WHEEL]->SetScale(XMVectorSet(1.24f, 0.5, 0, 0));
	images_[IMAGE_WHEEL]->Invisible();
}

void UIManager::Update() {
}

void UIManager::Draw() {
}

void UIManager::Release() {
}

void UIManager::ToggleVisible(IMAGE_TYPE type) {

	auto* image = images_[type];
	if (image->IsVisibled()) {

		image->Invisible();
	}
	else {

		image->Visible();
	}
}

ImageObject* UIManager::GetImageObject(IMAGE_TYPE type) {

	return images_[type];
}

ImageObject* UIManager::InstantiateImage(const std::string& name, const std::string& fileName) {

	ImageObject* pNewObject = new ImageObject(this, name, fileName);
	PushBackChild(pNewObject);
	pNewObject->Initialize();
	return pNewObject;
}