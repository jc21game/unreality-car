#include "ImageObject.h"

#include "../Engine/Image.h"
#include "../Engine/SceneTimer.h"

ImageObject::ImageObject(GameObject* parent, const std::string& name, const std::string& fileName)
	: GameObject(parent, name)
	, hPict_(-1), fileName_(fileName), isVariableScaling_(false) {
}

ImageObject::~ImageObject() = default;

void ImageObject::Initialize() {

	hPict_ = Image::Load(fileName_);
	assert(hPict_ >= 0);
}

void ImageObject::Update() {
}

void ImageObject::Draw() {

	auto trans = transform_;

	if (isVariableScaling_) {

		trans.scale_ *= 1.f + sin(SceneTimer::GetElapsedSecounds() * 5) * 0.2f;
	}

	Image::SetTransform(hPict_, trans);
	Image::Draw(hPict_);
}

void ImageObject::Release() {
}

void ImageObject::ValidateVariableScaling() {
	
	isVariableScaling_ = true;
}

void ImageObject::InvalidateVariableScaling() {

	isVariableScaling_ = false;
}
