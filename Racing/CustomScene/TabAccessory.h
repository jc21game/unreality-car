#pragma once
#include "Tab.h"

class TabAccessory : public Tab {

public:
	TabAccessory(GameObject* parent);
	virtual ~TabAccessory();

	virtual void Initialize() override;
	virtual void Draw() override;
	virtual void Release() override;

	virtual void OverwritePart() override;
};

