#pragma once
#include "../Engine/GameObject.h"

class OrnamentalCar : public GameObject {

private:
	bool isFocus_;	//フォーカスモードを管理するフラグ
	
public:
	/**
	 * \brief コンストラクタ
	 * \param parent 親オブジェクト
	 */
	OrnamentalCar(GameObject* parent);

	/**
	 * \brief デストラクタ
	 */
	~OrnamentalCar();


	/**
	 * \brief 初期化処理
	 */
	virtual void Initialize() override;

	/**
	 * \brief 更新処理
	 */
	virtual void Update() override;

	/**
	 * \brief 描画処理
	 */
	virtual void Draw() override;

	/**
	 * \brief 解放処理
	 */
	virtual void Release() override;

	/**
	 * \brief フォーカスモードを有効にする
	 */
	void FocusOn();
};

