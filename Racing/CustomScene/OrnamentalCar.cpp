#include "OrnamentalCar.h"

#include <iostream>


#include "../CustomCar/CustomCar.h"
#include "../Engine/Input.h"
#include "UIManager.h"


OrnamentalCar::OrnamentalCar(GameObject * parent) 
	: GameObject(parent, "OrnamentalCar")
	, isFocus_(false) {
}

OrnamentalCar::~OrnamentalCar() = default;

void OrnamentalCar::Initialize() {

	//todo 表示場所を指定する
	transform_.scale_ = XMVectorReplicate(0.7f);
	transform_.rotate_.vecY = 225.0f;
}

void OrnamentalCar::Update() {

	//フォーカスがあてられているとき
	if(isFocus_) {

		//ひとつ前のオブジェクト操作に戻る
		if (Input::IsKeyDown(DIK_ESCAPE)) {

			//画像表示の切り替え
			if (auto* pUIManager = dynamic_cast<UIManager*>(FindObject("UIManager"))) {

				pUIManager->ToggleVisible(IMAGE_CUSTOMIZE);
				pUIManager->ToggleVisible(IMAGE_PREVIEW);
				pUIManager->ToggleVisible(IMAGE_RIGHT);
				pUIManager->ToggleVisible(IMAGE_LEFT);
			}

			//変形情報の切り替え
			transform_.scale_ = XMVectorReplicate(0.7f);
			transform_.rotate_.vecY = 225.0f;

			isFocus_ = false;		//フォーカスを終了
			GetParent()->EnterSolo();	//親の更新許可
		}

		//入力で回転
		auto selectPos = XMVectorSelectControl(0, Input::IsKey(DIK_RIGHT), 0, 0);
		auto selectNeg = XMVectorSelectControl(0, Input::IsKey(DIK_LEFT), 0, 0);

		transform_.rotate_ += XMVectorSelect(g_XMOne, g_XMZero, selectPos);
		transform_.rotate_ -= XMVectorSelect(g_XMOne, g_XMZero, selectNeg);
	}
	else {

		//transform_.rotate_.vecY += 1.f;
	}
}

void OrnamentalCar::Draw() {

	Transform trans = transform_;

	//フォーカスがあてられているとき
	if (isFocus_) {

		//大きめに中央に表示させる
		trans.position_.vecY -= 1;
		trans.scale_ = XMVectorReplicate(1.2f);
	}
	CustomCar::GetCurrentCustom()->Draw(trans);
}

void OrnamentalCar::Release() {
}

void OrnamentalCar::FocusOn() {

	isFocus_ = true;
}
