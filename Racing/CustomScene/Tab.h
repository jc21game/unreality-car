#pragma once
#include "../Engine/GameObject.h"

class Tab : public GameObject {

protected:
	const size_t PART_SIZE;	//パーツの数
	const XMVECTOR BASE_POSITION;	//基本座標

	size_t currentIndex_;	//現在選ばれているパーツ

	static bool shouldSetup_;		//セットアップを行うかどうか

public:
	/**
	 * \brief コンストラクタ
	 * \param parent 親オブジェクト
	 * \param name オブジェクトの名前
	 * \param partSize パーツの数
	 */
	Tab(GameObject* parent, const std::string& name, size_t partSize);

	/**
	 * \brief デストラクタ
	 */
	virtual ~Tab() = default;


	/**
	 * \brief 初期化処理
	 */
	virtual void Initialize() override;

	/**
	 * \brief 更新処理
	 */
	virtual void Update() override;

	/**
	 * \brief 選択された時の実行される処理
	 */
	virtual void OverwritePart() = 0;

	static void ResetSetUp() { shouldSetup_ = true; }
};

