#include "TabManager.h"


#include "TabAccessory.h"
#include "TabBody.h"
#include "TabWheel.h"
#include "UIManager.h"
#include "../Engine/Input.h"

TabManager::TabManager(GameObject * parent)
	: GameObject(parent, "TabManager") 
	, firstRun_(true), currentTab_(0u) {

	tabs_.clear();
}

TabManager::~TabManager() = default;

void TabManager::Initialize() {

	//デフォルトで描画、更新を拒否
	LeaveSolo();
	Invisible();

	//tabのインスタンス生成と登録
	tabs_.emplace_back(Instantiate<TabBody>(this));
	tabs_.emplace_back(Instantiate<TabWheel>(this));
	//tabs_.emplace_back(Instantiate<TabAccessory>(this));
	
	//デフォルトで先頭のものを描画
	tabs_[0]->Visible();
}

void TabManager::Update() {

	//カスタマイズモードが有効になってから1度だけ呼ばれる処理
	if (firstRun_) {

		//画像表示の切り替え
		if (auto* pUIManager = dynamic_cast<UIManager*>(FindObject("UIManager"))) {
			
			pUIManager->ToggleVisible((IMAGE_TYPE)(currentTab_ + 5));
		}
		tabs_[currentTab_]->EnterSolo();	//現在のtabの更新を許可
		tabs_[currentTab_]->Visible();		//現在のtabの描画を許可
		firstRun_ = false;
	}

	auto prevTab = currentTab_;	//過去の状態保持

	//入力により選択されているタブを変更
	if (Input::IsKeyDown(DIK_A))	--currentTab_;
	if (Input::IsKeyDown(DIK_D))	++currentTab_;

	//入力されたら
	if (prevTab != currentTab_) {

		currentTab_ %= tabs_.size();	//範囲内にとどめる

		//画像商事の切り替え
		if (auto* pUIManager = dynamic_cast<UIManager*>(FindObject("UIManager"))) {
			
			pUIManager->ToggleVisible((IMAGE_TYPE)(currentTab_ + 5));
			pUIManager->ToggleVisible((IMAGE_TYPE)(prevTab + 5));
		}

		tabs_[prevTab]->Invisible();		//過去のtabの描画を拒否
		tabs_[prevTab]->LeaveSolo();		//過去のtabの更新を拒否
		tabs_[currentTab_]->Visible();		//現在のtabの描画を許可
		tabs_[currentTab_]->EnterSolo();	//現在のtabの更新を許可
	}

	//ひとつ前のオブジェクト操作に戻る
	if (Input::IsKeyDown(DIK_ESCAPE)) {

		//画像表示の切り替え
		if (auto* pUIManager = dynamic_cast<UIManager*>(FindObject("UIManager"))) {
			
			pUIManager->ToggleVisible(IMAGE_CUSTOMIZE);
			pUIManager->ToggleVisible(IMAGE_PREVIEW);
			pUIManager->ToggleVisible((IMAGE_TYPE)(currentTab_ + 5));
		}
		Tab::ResetSetUp();
		firstRun_ = true;
		tabs_[currentTab_]->Invisible();	//現在のtabの描画を拒否
		tabs_[currentTab_]->LeaveSolo();	//現在のtabの更新を拒否
		LeaveSolo();						//自身の更新拒否
		GetParent()->EnterSolo();			//親の更新許可
	}
}

void TabManager::Draw() {
}

void TabManager::Release() {
}
