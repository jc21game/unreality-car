#include "TabWheel.h"

#include "../Engine/SceneTimer.h"
#include "../CustomCar/CarPartManager.h"
#include "../CustomCar/CustomCar.h"

TabWheel::TabWheel(GameObject * parent)
	: Tab(parent, "TabWheel", CarPartManager::GetInstance()->GetWheelsSize()) {
}

TabWheel::~TabWheel() = default;

void TabWheel::Initialize() {

	Tab::Initialize();	//親クラスの初期化処理

	//todo ここから自分自身で必要な初期化処理
	transform_.scale_ *= 1.7f;
}

void TabWheel::Draw() {

	//todo ここで選択できるパーツを描画
	auto&& carPartManager = CarPartManager::GetInstance();

	//変形情報をコピー
	Transform trans = transform_;

	//すべてのパーツを描画
	for (auto i = 0u; i < PART_SIZE; ++i) {

		//選択されているらな大きく表示
		if (i == currentIndex_) {

			Transform bigTrans = trans;
			bigTrans.scale_ *= 1.f + sin(SceneTimer::GetElapsedSecounds() * 5) * 0.2f;
			carPartManager->DrawWheel(i, bigTrans);
		}
		else {

			carPartManager->DrawWheel(i, trans);
		}
		//右へずらす
		trans.position_.vecX += 3;
	}
}

void TabWheel::Release() {
}

void TabWheel::OverwritePart() {

	CustomCar::GetCurrentCustom()->ChangeWheel(static_cast<unsigned>(currentIndex_));
}
