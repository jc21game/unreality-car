#pragma once
#include "../Engine/GameObject.h"

#include <vector>

class Tab;

//タブを管理するクラス
class TabManager : public GameObject {

private:
	bool firstRun_;				//tabがアクティブになってから最初のフレームかどうか

	std::vector<Tab*> tabs_;	//登録されたtabを格納
	unsigned currentTab_;		//現在選ばれているtab

public:
	/**
	 * \brief コンストラクタ
	 * \param parent 親オブジェクト
	 */
	TabManager(GameObject* parent);

	/**
	 * \brief デストラクタ
	 */
	~TabManager();


	/**
	 * \brief 初期化処理
	 */
	virtual void Initialize() override;

	/**
	 * \brief 更新処理
	 */
	virtual void Update() override;

	/**
	 * \brief 描画処理
	 */
	virtual void Draw() override;

	/**
	 * \brief 解放処理
	 */
	virtual void Release() override;
};

