#include "ResultScene.h"
#include "ResultMonitor.h"
#include "../PlayScene/DataMediator.h"
#include "../Engine/SceneManager.h"
#include "../Engine/Input.h"

#include "../Engine/Image.h"
#include "../Engine/Direct3D.h"

//コンストラクタ
ResultScene::ResultScene(GameObject * parent)
	: GameObject(parent, "ResultScene")
	, hImage_(-1) {

}

//初期化
void ResultScene::Initialize() {

	//1P表示
	ResultMonitor* pMonitor1 = Instantiate<ResultMonitor>(this);
	pMonitor1->ResetDrawViewPortList();
	pMonitor1->AddDrawViewPort(Direct3D::VIEW_PORT_LEFT);
	auto&& dataMeditor = DataMediator::GetInstance();
	std::string playerName1 = dataMeditor->GetPlayerName(0);
	pMonitor1->SetPlayerName(playerName1);

	ResultMonitor* pMonitor2 = Instantiate<ResultMonitor>(this);
	pMonitor2->ResetDrawViewPortList();
	pMonitor2->AddDrawViewPort(Direct3D::VIEW_PORT_RIGHT);
	std::string playerName2 = dataMeditor->GetPlayerName(1);
	pMonitor2->SetPlayerName(playerName2);

	hImage_ = Image::Load("SampleResource/BGI04.png");
	Image::SetTransformFullSize(hImage_);
}

//更新
void ResultScene::Update() {

	//DIK_ESCAPEでホームへ
	if (Input::IsKeyDown(DIK_ESCAPE))
		ChangeNextScene();
}

//描画
void ResultScene::Draw() {

	Image::Draw(hImage_);
}

//開放
void ResultScene::Release() {

	//メタAIのリセット処理
	auto&& dataMeditor = DataMediator::GetInstance();
	dataMeditor->DestroyInstance();

	//シーン切り替え
	ChangeNextScene();
}

//シーン切り替え
void ResultScene::ChangeNextScene() {

	//スタートシーンへ
	SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
	pSceneManager->ChangeScene(SCENE_ID_START);
}
