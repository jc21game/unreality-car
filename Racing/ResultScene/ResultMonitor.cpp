#include "ResultMonitor.h"
#include "../Engine/Image.h"
#include "../PlayScene/DataMediator.h"
#include "../Engine/Direct3D.h"
#include "../CustomCar/CustomCar.h"

//コンストラクタ
ResultMonitor::ResultMonitor(GameObject * parent)
	: GameObject(parent, "ResultMonitor"), rotate_(0.f) {

	for (int i = 0; i < NUMBER_TYPE_MAX; i++) {

		hImagePeriod[i] = -1;

		for (int j = 0; j < NUMBER_MAX; j++)
			hImageNumbers[i][j] = -1;
	}

	for (int i = 0; i < CHARACTER_MAX; i++)
		hImageCaracters[i] = -1;

}

//初期化
void ResultMonitor::Initialize() {

	const int HALF_SIZE = 2;

	ImageLoad();

	transform_.scale_.vecY /= HALF_SIZE;	//Y方向の縮小
	//transform_.scale_ /= 5;					//全体の縮小
}

//更新
void ResultMonitor::Update() {


}

//描画
void ResultMonitor::Draw() {

	DrawCar();
	DrawResultTime();
	DrawRanking();
	DrawMaxSpeed();
}

//開放
void ResultMonitor::Release() {

}

void ResultMonitor::SetPlayerName(std::string& name) {

	playerName_ = name;
}

void ResultMonitor::ImageLoad() {

	//Assetsフォルダまでのパス
	std::string assets = "../Assets/";

	//数字のロード
	for (int i = 0; i < NUMBER_MAX; i++) {

		//画像データのロード
		hImageNumbers[NUMBER_TYPE_TEST][i] = Image::Load(assets + "SampleResource/numbers/" + std::to_string(i) + ".png");
		assert(hImageNumbers[i] >= 0);

		hImageNumbers[NUMBER_TYPE_NORMAL][i] = Image::Load(assets + "Number/" + std::to_string(i) + ".png");
		assert(hImageNumbers[i] >= 0);

		hImageNumbers[NUMBER_TYPE_RANK][i] = Image::Load(assets + "Play/rank/Rank" + std::to_string(i) + ".png");
		assert(hImageNumbers[i] >= 0);
	}

	//ピリオド（小数点）の画像ロード
	hImagePeriod[NUMBER_TYPE_TEST] = Image::Load(assets + "SampleResource/numbers/period.png");
	assert(hImagePeriod[NUMBER_TYPE_TEST] >= 0);
	hImagePeriod[NUMBER_TYPE_NORMAL] = Image::Load(assets + "Number/piriod.png");
	assert(hImagePeriod[NUMBER_TYPE_NORMAL] >= 0);

	//文字画像のロード
	hImageCaracters[CHARACTER_MAX_SPEED] = Image::Load(assets + "Result/max_speed_BTK.png");
	assert(hImageCaracters[CHARACTER_MAX_SPEED] >= 0);
	hImageCaracters[CHARACTER_RANK] = Image::Load(assets + "Result/rank_BTK.png");
	assert(hImageCaracters[CHARACTER_RANK] >= 0);
	hImageCaracters[CHARACTER_TIME] = Image::Load(assets + "Result/time_BTK.png");
	assert(hImageCaracters[CHARACTER_TIME] >= 0);
}

void ResultMonitor::DrawCar()
{
	auto&& dataMeditor = DataMediator::GetInstance();						//プレイシーンの情報を保持しているインスタンス
	int playerNumber = dataMeditor->GetPlayerNumber(playerName_);			//プレイヤーの番号
	Transform trans;														//描画する状態
	trans.rotate_.vecY = rotate_;											//回転
	trans.position_ = { 0.f, 0.6f, 0.f, 0.f };								//描画位置
	trans.scale_ *= 0.5f;

	//描画
	CustomCar::GetPlayerCustom((CUSTOM_NUMBER)playerNumber)->Draw(trans);
	rotate_ += 1;
}

void ResultMonitor::DrawResultTime() {

	auto&& dataMeditor = DataMediator::GetInstance();			//プレイシーンの情報を保持しているインスタンス
	int ranking = dataMeditor->GetRanking(playerName_);			//順位
	float resultTime = dataMeditor->GetResultTime(ranking);	//リザルトタイム
	XMVECTOR drawPosition = { -0.3f, -0.4f, 0.f, 0.f };			//描画位置

	//文字の描画
	transform_.position_ = drawPosition;
	Image::SetTransform(hImageCaracters[CHARACTER_TIME], transform_);
	Image::Draw(hImageCaracters[CHARACTER_TIME]);

	drawPosition.vecX = 0.5;									//右にずらす

	//リザルトタイムの描画
	DrawNumber(drawPosition, resultTime, NUMBER_TYPE_NORMAL);
}

void ResultMonitor::DrawRanking() {

	auto&& dataMeditor = DataMediator::GetInstance();			//プレイシーンの情報を保持しているインスタンス
	int ranking = dataMeditor->GetRanking(playerName_);			//順位
	XMVECTOR drawPosition = { -0.2f, 0.85f, 0.f, 0.f };			//描画位置
	transform_.position_ = drawPosition;

	//文字の描画
	Image::SetTransform(hImageCaracters[CHARACTER_RANK], transform_);
	Image::Draw(hImageCaracters[CHARACTER_RANK]);

	drawPosition.vecX = 0.3f;

	//描画
	DrawNumber(drawPosition, ranking, NUMBER_TYPE_RANK);
}

void ResultMonitor::DrawMaxSpeed() {

	auto&& dataMeditor = DataMediator::GetInstance();						//プレイシーンの情報を保持しているインスタンス
	float maxSpeed = dataMeditor->GetMaxSpeed(playerName_);					//最高速度
	XMVECTOR drawPosition = { -0.5f, -0.7f, 0.f, 0.f };						//描画位置
	transform_.position_ = drawPosition;

	//描画	
	Image::SetTransform(hImageCaracters[CHARACTER_MAX_SPEED], transform_);
	Image::Draw(hImageCaracters[CHARACTER_MAX_SPEED]);

	drawPosition.vecX = 0.5;

	DrawNumber(drawPosition, maxSpeed, NUMBER_TYPE_NORMAL);
}

void ResultMonitor::DrawLapTime() {

	const int MAX_ROUND = 3;										//最大周回数
	auto&& dataMeditor = DataMediator::GetInstance();				//プレイシーンの情報を保持しているインスタンス
	XMVECTOR drawPosition = { 0.f, -0.85f, 0.f, 0.f };				//描画位置

	//周回数分ループ
	for (int round = 1; round < MAX_ROUND + 1; round++) {
		float lapTime = dataMeditor->GetLapTime(playerName_, round);	//ラップタイム
		if (lapTime < 0) return;										//記録がなければ終わり

		//描画
		DrawNumber(drawPosition, 10.f, NUMBER_TYPE_NORMAL);

		drawPosition.vecY += LINE_FEED_WIDTH;						//位置を下にずらす
	}
}

void ResultMonitor::DrawNumber(XMVECTOR& position, float number, NUMBER_TYPE type) {

	//整数部の桁数を求める
	int digit = static_cast<int>(std::to_string((int)number).length());			//整数部の桁数

	//整数部の桁数に合わせて位置調整
	XMVECTOR startPosition = position;							//描画の開始位置
	startPosition.vecX -= digit * NUMBER_INTERVAL;
	transform_.position_ = startPosition;


	//描画する小数を四捨五入
	number = static_cast<float>(std::round(number * pow(10, ROUND_DIGIT)) / pow(10, ROUND_DIGIT));

	std::string str = std::to_string(number);					//文字列に変換
	int size = digit + ROUND_DIGIT + 1;							//表示桁数

	//数字の描画
	for (int i = 0; i < size; i++) {

		//小数点の位置はスキップ
		if (i == digit) {
			transform_.position_.vecX += NUMBER_INTERVAL;
			i++;
		}

		int drawNumber = str[i] - '0';							//描画する数字

		Transform trans = transform_;							//描画する状態
		if (drawNumber == 0)		trans.scale_.vecX *= 0.85f;	//0の画像の横幅調整

		//描画
		Image::SetTransform(hImageNumbers[type][drawNumber], trans);
		Image::Draw(hImageNumbers[type][drawNumber]);

		//一文字分ずらす
		transform_.position_.vecX += NUMBER_INTERVAL;
	}

	//小数点の描画
	transform_.position_ = position;
	Image::SetTransform(hImagePeriod[type], transform_);
	Image::Draw(hImagePeriod[type]);
}

void ResultMonitor::DrawNumber(XMVECTOR&position, int number, NUMBER_TYPE type) {

	//文字列に変換
	std::string str = std::to_string(number);

	//整数部の桁数に合わせて位置調整
	XMVECTOR startPosition = position;							//描画の開始位置
	startPosition.vecX -= str.size() * NUMBER_INTERVAL;
	transform_.position_ = startPosition;

	//数字の描画
	for (auto number : str) {

		int drawNumber = number - '0';							//描画する数字

		Transform trans = transform_;							//描画する状態
		if (drawNumber == 0)		trans.scale_.vecX *= 0.85f;	//0の画像の横幅調整

		//描画
		Image::SetTransform(hImageNumbers[type][drawNumber], trans);
		Image::Draw(hImageNumbers[type][drawNumber]);

		//一文字分ずらす
		transform_.position_.vecX += NUMBER_INTERVAL;
	}
}
