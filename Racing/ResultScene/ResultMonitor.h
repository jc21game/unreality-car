#pragma once
#include "../Engine/GameObject.h"

const int NUMBER_MAX = 10;	//数字の数


//リザルト表示を管理するクラス
class ResultMonitor : public GameObject {

private:
	const float NUMBER_INTERVAL = 0.12f;	//描画する数字の間隔
	const int ROUND_DIGIT = 2;				//小数値を切り捨てる桁数
	const float LINE_FEED_WIDTH = 0.2f;		//改行幅

	//描画する数字の種類
	enum NUMBER_TYPE {
		NUMBER_TYPE_TEST = 0,				//テスト用
		NUMBER_TYPE_NORMAL,					//ノーマル
		NUMBER_TYPE_RANK,					//順位用
		NUMBER_TYPE_MAX
	};

	//文字
	enum CHARACTER {

		CHARACTER_TEST = 0,					//テスト用
		CHARACTER_MAX_SPEED,
		CHARACTER_RANK,
		CHARACTER_TIME,
		CHARACTER_MAX
	};

	std::string playerName_;				//リザルト表示をするプレイヤーの名前

	int hImageNumbers[NUMBER_TYPE_MAX][NUMBER_MAX];		//数値表示用のイメージハンドル
	int hImagePeriod[NUMBER_TYPE_MAX];	//ピリオドのイメージハンドル
	int hImageCaracters[CHARACTER_MAX];	//文字のイメージハンドル
	float rotate_;						//回転量

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	ResultMonitor(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//リザルト表示するプレイヤーを指定
	void SetPlayerName(std::string& name);

private:
	//モデルデータのロード
	void ImageLoad();

	//描画
	void DrawCar();			//車
	void DrawResultTime();	//リザルトタイム
	void DrawRanking();		//順位
	void DrawMaxSpeed();	//最高速度
	void DrawLapTime();		//ラップタイム





	//値を画像で描画（負数は非対応）
	//引数：小数点の位置、描画する数値
	void DrawNumber(XMVECTOR& position, float number, NUMBER_TYPE type);
	void DrawNumber(XMVECTOR& position, int number, NUMBER_TYPE type);

};