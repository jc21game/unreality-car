#include <iostream>
#include <DirectXColors.h>
#include "SplashScene.h"
#include "../Engine/SceneManager.h"
#include "../Engine/Input.h"
//#include "../Engine/Model.h"
//#include "../Engine/Camera.h"
#include "../Engine/Image.h"
#include "../Engine/Audio.h"
#include "../Engine/SceneTimer.h"
#include "../CustomCar/CarPartManager.h"

//コンストラクタ
SplashScene::SplashScene(GameObject * parent)
	: GameObject(parent, "SplashScene"), vimage_(-1), sSound_(-1){

	//position = Camera::GetPosition();
}

//初期化
void SplashScene::Initialize() {

	CarPartManager::GetInstance();

	//画像データのロード
	vimage_ = Image::Load("SampleResource\\SampleSplash.png");
	assert(vimage_ >= 0);


	//動画データのロード
	//vimage_ = Model::Load("TestSplashMove.fbx");
	//vimage_ = Model::Load("SampleResource/SampleModel.fbx");
	//assert(vimage_ >= 0);


	//サウンドデータのロード
	sSound_ = Audio::Load("SampleResource\\elephant1.wav");
	assert(sSound_ >= 0);

	//サウンド流す
	//Audio::Play(sSound_);

	//動画のフレーム設定
	//Model::SetAnimFrame(vimage_, 1, 120, 1.f);


	//カメラの位置変更
	//XMVECTOR pos = { -20,0,20,0 };
	//Camera::SetPosition(pos);	


	//背景色の変更
	Direct3D::SetClearScreenColor(Colors::Snow);
}

//更新
void SplashScene::Update() {

	//画像をフェードインフェードアウトさせるやつ
	float elapsedTime = SceneTimer::GetElapsedSecounds();

	if (elapsedTime > XM_PI) {

		Invisible();
	}

	//ALL実装しなかったらその時考える
	if (Input::IsKeyDown(DIK_RETURN) || /*controllerState_.Gamepad.wButtons & XINPUT_GAMEPAD_ALL ||*/ elapsedTime > 4) {

		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void SplashScene::Draw() {

	//画像の描画
	int alpha = static_cast<int>(sin(SceneTimer::GetElapsedSecounds()) * 255.f);
	Image::SetAlpha(vimage_, alpha);
	Image::SetTransform(vimage_, transform_);
	Image::Draw(vimage_);


	//動画の描画
	//Model::SetTransform(vimage_, transform_);
	//Model::Draw(vimage_);


}

//開放
void SplashScene::Release() {
}