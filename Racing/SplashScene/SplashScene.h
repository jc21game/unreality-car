#pragma once
#include "../Engine/GameObject.h"


class SplashScene : public GameObject {

private:
	/*※画像と動画一緒に出したい時は
		無いと思うので宣言を一つにまとめてみた*/
	int vimage_;			//画像、動画番号

	int sSound_;		//サウンド番号

	//背景色
	//XMFLOAT4 col_{ 0.95,0.95,0.95,1 };

	//カメラ位置の保存
	//XMVECTOR position;
public:
	//コンストラクタ
		//引数：parent  親オブジェクト（SceneManager）
	SplashScene(GameObject* parent);

	//デストラクタでカメラ位置戻す
	/*~SplashScene() {
		Camera::SetPosition(position);
	}*/

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
