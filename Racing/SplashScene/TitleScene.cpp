#include <iostream>
#include <DirectXColors.h>
#include "TitleScene.h"
#include "../Engine/Direct3D.h"
#include "../Engine/SceneManager.h"
#include "../Engine/Input.h"
#include "../Engine/Model.h"
#include "../Engine/Image.h"
#include "../Engine/Audio.h"
#include "../Engine/SceneTimer.h"

//コンストラクタ
TitleScene::TitleScene(GameObject * parent)
	: GameObject(parent, "TitleScene")
	, vimage_(-1), hPictEnter_(-1), sSound_(-1) {

}

TitleScene::~TitleScene() {
}

//初期化
void TitleScene::Initialize() {

	//画像データのロード
	vimage_ = Image::Load("SampleResource\\SampleTitleImage01.png");
	assert(vimage_ >= 0);

	Image::SetTransformFullSize(vimage_);

	hPictEnter_ = Image::Load("SampleResource\\SampleTitleImageTapEnter.png");
	assert(hPictEnter_ >= 0);

	Image::SetTransformFullSize(hPictEnter_);
	//動画データのロード
	//vimage_ = Model::Load("TestSplashMove.fbx");
	//vimage_ = Model::Load("SampleResource\\SampleModel.fbx");
	//assert(vimage_ >= 0);


	//サウンドデータのロード
	sSound_ = Audio::Load("SampleResource\\car-rocket-start1.wav");
	assert(sSound_ >= 0);

	//サウンド流す
	//Audio::PlayLoop(sSound_);

	//動画のフレーム設定
	//Model::SetAnimFrame(vimage_, 1, 120, 1.f);

}

//更新
void TitleScene::Update() {
	//ゲームパッド入力したらコメント外すよ
	if (Input::IsKeyDown(DIK_RETURN/* || controllerState_.Gamepad.wButtons & XINPUT_GAMEPAD_START ||*/)) {
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_START);

		//音を止める
		Audio::Stop(sSound_);
	 }
}

//描画
void TitleScene::Draw() {

	//画像の描画
	Image::Draw(vimage_);

	int alpha = static_cast<int>(cos(SceneTimer::GetElapsedSecounds() * 3.f) * 255.f);
	Image::SetAlpha(hPictEnter_, alpha);
	Image::Draw(hPictEnter_);

	//動画の描画
	//Model::SetTransform(vimage_, transform_);
	//Model::Draw(vimage_);


}

//開放
void TitleScene::Release() {

	auto color = XMColorAdjustSaturation(Colors::DarkCyan, 0.8f);
	Direct3D::SetClearScreenColor(color);
}