#pragma once
#include "../Engine/GameObject.h"


class TitleScene : public GameObject {
private:
	int vimage_;			//画像、動画番号
	int hPictEnter_;
	/*※画像と動画一緒に出したい時は
		無いと思うので宣言を一つにまとめてみた*/

	int sSound_;		//サウンド番号

	//背景色
	//XMFLOAT4 col_ = { 0.1f, 0.5f, 0.5f, 1.0f };
public:
	//コンストラクタ
		//引数：parent  親オブジェクト（SceneManager）
	TitleScene(GameObject* parent);

	~TitleScene();


	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
