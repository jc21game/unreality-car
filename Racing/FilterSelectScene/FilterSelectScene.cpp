#include "FilterSelectScene.h"

#include <string>
#include <iostream>

#include "../Engine/Input.h"
#include "../Engine/Image.h"
#include "../Engine/Direct3D.h"
#include "../Engine/SceneManager.h"
#include "../Engine/SceneTimer.h"

enum class ICON : unsigned char {
	NONE,
	NEGATIVE,
	MONOTONE,
	OIL,
	GAMING,
	SEPIA,
	THREAHOLD,
	MAYFLY,
	MOSAIC,
	BAYER,
	CRT,
	POSTERIZATION,
};


FilterSelectScene::FilterSelectScene(GameObject* parent) 
	: GameObject(parent, "FilterSelectScene")
	, hBGI_(-1), hBackImage_(-1), currentIcon_(0) {
}

void FilterSelectScene::Initialize() {

	//現在せれているものをスコープ
	currentIcon_ = static_cast<int>(Direct3D::GetCurrentFilterType());

#ifdef _DEBUG
	Direct3D::ValidatePostEffect();
#endif 

	hBGI_ = Image::Load("SampleResource/BGI04.png");
	assert(hBGI_ >= 0);
	Image::SetTransformFullSize(hBGI_);

	hBackImage_ = Image::Load("SampleResource\\return.png");
	assert(hBackImage_ >= 0);
	Transform trans = transform_;
	trans.scale_ = XMVectorReplicate(0.3f);
	trans.position_ = XMVectorSet(-0.85f, 0.8f, 0, 0);
	Image::SetTransform(hBackImage_, trans);

	const std::array<std::string, ICON_MAX> FILE_NAMES = {
		"neutral",
		"negative",
		"monotone",
		"oil",
		"gaming",
		"sepia",
		"threahold",
		"mayfly",
		"mosaic",
		"bayer",
		"crt",
		"poterization"
	};

	trans = transform_;
	trans.scale_ = XMVectorReplicate(0.8f);
	auto screenPos = XMVectorSet(0, 290.f, 0, 0);
	trans.position_ = Image::ScreenToImagePosition(screenPos);
	for (auto i = 0; i < ICON_MAX; ++i) {

		std::cout << i;
		hIcons_[i] = Image::Load("Filter/filter_" + FILE_NAMES[i] + ".png");
		assert(hIcons_[i] >= 0);

		hIconsBase_[i] = Image::Load("Filter/base/filter_" + FILE_NAMES[i] + ".png");
		assert(hIconsBase_[i] >= 0);
		Image::SetTransform(hIconsBase_[i], trans);
	}
}

void FilterSelectScene::Update() {

	using namespace Input;

	//決定されたら切り替え
	if (IsKeyDown(DIK_RETURN)) {

		Direct3D::SetPostEffect(static_cast<Direct3D::SHADER_FILTER>(currentIcon_));
	}

	//前画面へ
	if (IsKeyDown(DIK_ESCAPE)) {

		auto* sceneManager = SceneManager::GetInstance();
		sceneManager->ChangeScene(SCENE_ID_START);
	}

	//各方向入力
	if (IsKeyDown(DIK_UP)) {

		currentIcon_ -= WIDTH;
		if (currentIcon_ < 0)	currentIcon_ += ICON_MAX;
		currentIcon_ %= ICON_MAX;
	}
	if (IsKeyDown(DIK_DOWN)) {

		currentIcon_ += WIDTH;
		currentIcon_ %= ICON_MAX;
	}
	if (IsKeyDown(DIK_LEFT)) {

		auto line = currentIcon_ / WIDTH;
		--currentIcon_;
		if (currentIcon_ < 0)	currentIcon_ += ICON_MAX;
		currentIcon_ = currentIcon_ % WIDTH + WIDTH * line;
	}
	if (IsKeyDown(DIK_RIGHT)) {

		auto line = currentIcon_ / WIDTH;
		++currentIcon_;
		currentIcon_ = currentIcon_ % WIDTH + WIDTH * line;
	}
}

void FilterSelectScene::Draw() {

	Image::Draw(hBGI_);

	Image::Draw(hBackImage_);

	auto currentFilter = static_cast<int>(Direct3D::GetCurrentFilterType());
	Image::Draw(hIconsBase_[currentFilter]);

	Transform trans = transform_;
	auto offset = XMVectorSet(375.f, 125.f, 0, 0); //アイコン同士の隙間
	//描画開始の始点
	auto start = -(offset * XMVectorSet(static_cast<float>((WIDTH - 1) / 2.f), static_cast<float>((HEIGHT - 1) / -2.f), 0, 0)) + XMVectorSet(0, -40, 0, 0);

	for (auto h = 0; h < HEIGHT; ++h) {
		for (auto w = 0; w < WIDTH; ++w) {

			trans.scale_ = XMVectorReplicate(0.6f);
			auto screenPos = start + offset * XMVectorSet(w, -h, 0, 0);
			trans.position_ = Image::ScreenToImagePosition(screenPos);

			auto index = h * WIDTH + w;

			//選択されているものをわかりやすく
			if (index == currentIcon_) {

				trans.scale_ *= 1.2f + sin(SceneTimer::GetElapsedSecounds() * 5) * 0.2f;
			}
			Image::SetTransform(hIcons_[index], trans);
			Image::Draw(hIcons_[index]);
		}
	}
}

void FilterSelectScene::Release() {
}
