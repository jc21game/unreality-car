#pragma once
#include "../Engine/GameObject.h"

#include <array>

class FilterSelectScene : public GameObject {


	static constexpr int ICON_MAX = 12;
	static constexpr int WIDTH = 3;
	static constexpr int HEIGHT = 4;

private:
	int hBGI_;					//背景用画像ハンドル
	int hBackImage_;			//バック用画像ハンドル

	std::array<int, ICON_MAX> hIconsBase_;	//加工前画像アイコンハンドル
	std::array<int, ICON_MAX> hIcons_;	//フィルタ選択用アイコンハンドル

	int currentIcon_;

public:
	explicit FilterSelectScene(GameObject*);
	~FilterSelectScene() = default;

	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

