#pragma once
#include <d3d11.h>
#include <memory>
#include <vector>

#pragma comment(lib, "d3d11.lib")

struct ID3D11Device;
struct ID3D11DeviceContext;
struct D3D11_INPUT_ELEMENT_DESC;

class ShaderBundle {

protected:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;

public:
	ShaderBundle();
	virtual ~ShaderBundle();

	virtual bool Initialize(ID3D11Device* d3dDevice) = 0;
	void SetShaderBundle(ID3D11DeviceContext* d3dDevice) const;

protected:
	bool CreateVertexShader(ID3D11Device* d3dDevice, const std::wstring& srcFile, const std::vector<D3D11_INPUT_ELEMENT_DESC>& layoutDesc) const;
	bool CreatePixelShader(ID3D11Device* d3dDevice, const std::wstring& srcFile) const;
	bool CreateRasterizerState(ID3D11Device* d3dDevice, D3D11_CULL_MODE cullMode, D3D11_FILL_MODE fillMode = D3D11_FILL_SOLID) const;
};

