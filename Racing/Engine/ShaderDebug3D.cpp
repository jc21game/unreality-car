#include "ShaderDebug3D.h"

#include <d3d11.h>

#pragma comment(lib, "d3d11.lib")

bool ShaderDebug3D::Initialize(ID3D11Device* d3dDevice) {

	std::vector<D3D11_INPUT_ELEMENT_DESC> layout = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	bool result = CreateVertexShader(d3dDevice, L"Shader/Debug3D.hlsl", layout);
	if (!result) return false;
	result = CreatePixelShader(d3dDevice, L"Shader/Debug3D.hlsl");
	if (!result) return false;
	result = CreateRasterizerState(d3dDevice, D3D11_CULL_BACK);
	if (!result) return false;

	return true;
}
