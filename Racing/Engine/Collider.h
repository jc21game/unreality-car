#pragma once
#include <DirectXCollision.h>	//衝突判定ライブラリ

//前方宣言
class GameObject;
class ColliderSphere;
class ColliderBox;
class ColliderOrientedBox;

using namespace DirectX;


//---------------------------------------------
// 当たり判定を管理するクラス
//---------------------------------------------
class Collider {
	
protected:
	GameObject*	pObject_;		//所有オブジェクト
	XMFLOAT3	center_;		//所有オブジェクトの原点からの位置
	int			hDebugModel_;	//デバッグ表示用モデルID

public:
	Collider();
	virtual ~Collider() = default;

	
	/**
	 * \brief 所有オブジェクトの設定
	 * \param pObject 所有オブジェクト
	 */
	void SetGameObject(GameObject* pObject);

	
	/**
	 * \brief 接触判定
	 * \param pTarget 相手の当たり判定
	 * \return 接触しているなら真
	 */
	virtual bool IsHit(Collider* pTarget) = 0;

	/**
	 * \brief デバッグ用モデル描画
	 */
	virtual void Draw() = 0;

	/**
	 * \brief 境界範囲の座標を更新
	 */
	virtual void TranslationPosition() = 0;

	/**
	 * \brief インスタンスをコピーする
	 * \return コピーしたインスタンス
	 */
	virtual Collider* Copy() = 0;

protected:
	/**
	 * \brief ワールド座標の取得
	 * \return ワールド座標
	 */
	XMVECTOR GetWorldPosition() const;

protected:
	/**
	 * \brief 球体同士の衝突判定
	 * \param sphereA 1つ目の球体判定
	 * \param sphereB 2つ目の球体判定
	 * \return 接触しているなら真
	 */
	static bool IsHitSpheres(ColliderSphere* sphereA, ColliderSphere* sphereB);

	/**
	 * \brief 球体と箱型の衝突判定
	 * \param sphere 球体判定
	 * \param box 箱型判定
	 * \return 接触しているなら真
	 */
	static bool IsHitSphereVsBox(ColliderSphere* sphere, ColliderBox* box);

	/**
	 * \brief 球体と有向箱型の衝突判定
	 * \param sphere 球体判定
	 * \param orientedBox 有向箱型判定
	 * \return 接触しているなら真
	 */
	static bool IsHitSphereVsOrientedBox(ColliderSphere* sphere, ColliderOrientedBox* orientedBox);

	/**
	 * \brief 箱型同士の衝突判定
	 * \param boxA 1つ目の箱型判定
	 * \param boxB 2つ目の箱型判定
	 * \return 接触しているなら真
	 */
	static bool IsHitBoxes(ColliderBox* boxA, ColliderBox* boxB);

	/**
	 * \brief 箱型と有向箱型の衝突判定
	 * \param box 箱型判定
	 * \param orientedBox 有向箱型判定
	 * \return 接触しているなら真
	 */
	static bool IsHitBoxVsOrientedBox(ColliderBox* box, ColliderOrientedBox* orientedBox);

	/**
	 * \brief 有向箱型同士の衝突判定
	 * \param orientedBoxA 1つ目の有向箱型判定
	 * \param orientedBoxB 2つ目の有向箱型判定
	 * \return 接触しているなら真
	 */
	static bool IsHitOrientedBoxes(ColliderOrientedBox* orientedBoxA, ColliderOrientedBox* orientedBoxB);
};

