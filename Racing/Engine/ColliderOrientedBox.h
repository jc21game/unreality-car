#pragma once
#include "Collider.h"

class ColliderOrientedBox : public Collider {

	friend class Collider;	//親クラスにprivateメンバを公開

private:
	BoundingOrientedBox bOrientedBox_;	//有向境界ボックス
	XMFLOAT3 rotate_;					//回転状態(度)
	bool isRotateSync_;					//所持オブジェクトの回転に合わせるか
	
	/**
	 * \brief コンストラクタ
	 * \param bb コピーしたい境界ボックス
	 * \param rotate 回転状態(度)
	 * \param isRotateSync 所持オブジェクトの回転に合わせるか
	 */
	ColliderOrientedBox(BoundingOrientedBox& bob, XMFLOAT3 rotate, bool isRotateSync);

public:
	/**
	 * \brief コンストラクタ
	 * \param center 中心座標
	 * \param extents 広がり(中心座標から広げる長さ)
	 * \param rotate 回転状態(度)
	 */
	ColliderOrientedBox(FXMVECTOR center, FXMVECTOR extents, FXMVECTOR rotate);
	
	/**
	 * \brief 接触判定
	 * \param pTarget 相手の当たり判定
	 * \return 接触しているなら真
	 */
	bool IsHit(Collider* pTarget) override;

	/**
	 * \brief デバッグ用モデル描画
	 */
	void Draw() override;

	/**
	 * \brief 境界範囲の座標を更新
	 */
	void TranslationPosition() override;

	/**
	 * \brief インスタンスをコピーする
	 * \return コピーしたインスタンス
	 */
	virtual Collider* Copy() override;

	
	/**
	 * \brief 回転状態を変更する
	 * \param rotate 回転状態
	 */
	void SetRotate(FXMVECTOR rotate);

	
	/**
	 * \brief 所持オブジェクトの回転に合わせる
	 */
	void ValidRotateSync() { isRotateSync_ = true; }
	
	/**
	 * \brief 所持オブジェクトの回転に合わせない
	 */
	void InvalidRotateSync() { isRotateSync_ = false; }

	/**
	 * \brief 所持オブジェクトの回転に合わせるか
	 * \return 合わせるなら真
	 */
	bool IsRotateSync() const { return isRotateSync_; }
};

