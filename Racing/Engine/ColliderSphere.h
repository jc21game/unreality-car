#pragma once
#include "Collider.h"

class ColliderSphere : public Collider {

	friend class Collider;	//親クラスにprivateメンバを公開
	
private:
	BoundingSphere bSphere_;	//境界球

	/**
	 * \brief コピーコンストラクタ
	 * \param bs コピーしたい境界スフィア
	 */
	ColliderSphere(BoundingSphere& bs);

public:
	/**
	 * \brief コンストラクタ
	 * \param center 中心座標
	 * \param radius 半径
	 */
	ColliderSphere(FXMVECTOR center, float radius);
	
	/**
	 * \brief 接触判定
	 * \param pTarget 相手の当たり判定
	 * \return 接触しているなら真
	 */
	bool IsHit(Collider* pTarget) override;

	/**
	 * \brief デバッグ用モデル描画
	 */
	void Draw() override;

	/**
	 * \brief 境界範囲の座標を更新
	 */
	void TranslationPosition() override;

	/**
	 * \brief インスタンスをコピーする
	 * \return コピーしたインスタンス
	 */
	virtual Collider* Copy() override;
};

