#include "sceneManager.h"

#include "../TestScene.h"
//#include "../SampleScene.h"
#include "../SplashScene/SplashScene.h"
#include "../SplashScene/TitleScene.h"
#include "../PlayScene/PlayScene.h"
#include "../StartScene/StartScene.h"
#include "../CustomScene/CustomScene.h"
#include "../ResultScene/ResultScene.h"
#include "../FilterSelectScene/FilterSelectScene.h"

#include "../StageSelectScene/StageSelectScene.h"
#include "Model.h"
#include "Image.h"
#include "Audio.h"
#include "Camera.h"
#include "SceneTimer.h"
#include "StaticModel.h"

SceneManager* SceneManager::instance_ = nullptr;

//コンストラクタ
SceneManager::SceneManager(GameObject * parent)
	: GameObject(parent, "SceneManager") {
}

//初期化
void SceneManager::Initialize()
{
	instance_ = this;

	//最初のシーンを準備
	//currentSceneID_ = SCENE_ID_PLAY;
	currentSceneID_ = SCENE_ID_SPLASH;
	nextSceneID_ = currentSceneID_;
	Instantiate<SplashScene>(this);
	//Instantiate<PlayScene>(this);
}

//更新
void SceneManager::Update()
{
	//次のシーンが現在のシーンと違う　＝　シーンを切り替えなければならない
	if (currentSceneID_ != nextSceneID_)
	{
		//そのシーンのオブジェクトを全削除
		KillAllChildren();

		//ロードしたデータを全削除
		Audio::AllRelease();
		Model::AllRelease();
		Image::AllRelease();

		Camera::Initialize();

		//ポストエフェクトを無効化
		Direct3D::InvalidatePostEffect();

		StaticModel::RefreshTransform();

		//次のシーンを作成
		switch (nextSceneID_)
		{
		case SCENE_ID_TEST:		Instantiate<TestScene>(this);	break;
		//case SCENE_ID_SAMPLE:	Instantiate<SampleScene>(this); break;
		case SCENE_ID_PLAY:		Instantiate<PlayScene>(this);	break;
		case SCENE_ID_SPLASH:	Instantiate<SplashScene>(this);	break;
		case SCENE_ID_TITLE:	Instantiate<TitleScene>(this);	break;
		case SCENE_ID_START:	Instantiate<StartScene>(this);	break;
		case SCENE_ID_CUSTOM:	Instantiate<CustomScene>(this);	break;
		case SCENE_ID_FILTER:	Instantiate<FilterSelectScene>(this);	break;
		case SCENE_ID_SELECT:	Instantiate<StageSelectScene>(this);	break;
		case SCENE_ID_RESULT:	Instantiate<ResultScene>(this);	break;
		default:				Instantiate<TestScene>(this);	break;
		}

		currentSceneID_ = nextSceneID_;

		// タイマーをリセット
		SceneTimer::Reset();

	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

//シーン切り替え（実際に切り替わるのはこの次のフレーム）
void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}

SceneManager* SceneManager::GetInstance() {
	
	return instance_;
}
