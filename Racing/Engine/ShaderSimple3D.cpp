#include "ShaderSimple3D.h"

#include <d3d11.h>
#include <DirectXCollision.h>

#pragma comment(lib, "d3d11.lib")

bool ShaderSimple3D::Initialize(ID3D11Device* d3dDevice) {

	UINT vectorSize = sizeof DirectX::XMVECTOR;
	
	std::vector<D3D11_INPUT_ELEMENT_DESC> layout = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//頂点位置
		{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 1, D3D11_INPUT_PER_VERTEX_DATA, 0 },	//法線
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, vectorSize * 2, D3D11_INPUT_PER_VERTEX_DATA, 0 },	//テクスチャ（UV）座標
	};
	
	bool result = CreateVertexShader(d3dDevice, L"Shader/Simple3D.hlsl", layout);
	if (!result) return false;
	result = CreatePixelShader(d3dDevice, L"Shader/Simple3D.hlsl");
	if (!result) return false;
	result = CreateRasterizerState(d3dDevice, D3D11_CULL_BACK);
	if (!result) return false;

	return true;
}
