#include <d3dcompiler.h>
#include <array>
#include "Direct3D.h"

#include <vector>
#include <unordered_map>

#include "Global.h"
#include "Transform.h"
#include "Camera.h"
#include "FilterNegative.h"
#include "FilterMonotone.h"
#include "FilterOilPaint.h"
#include "FilterGaming.h"
#include "FilterSepia.h"
#include "FilterThreshold.h"
#include "FilterMayfly.h"
#include "FilterMosaic.h"
#include "FilterBayer.h"
#include "FilterCRT.h"
#include "FilterPosterization.h"
#include "ShaderBundle.h"
#include "ShaderDebug3D.h"
#include "ShaderSimple2D.h"
#include "ShaderSimple3D.h"

//画面の描画に関する処理
namespace Direct3D
{
	//【スワップチェーン】
	//画用紙を2枚用紙しておき、片方を画面に映している間にもう一方に描画。
	//1フレーム分の絵が出来上がったら画用紙を交換。これにより画面がちらつかない。
	//その辺を司るのがスワップチェーン
	IDXGISwapChain*         pSwapChain_ = nullptr;

	//【レンダーターゲットビュー】
	//描画したいものと、描画先（上でいう画用紙）の橋渡しをするもの
	ID3D11RenderTargetView* pRenderTargetView_ = nullptr;

	//【デプスステンシル】
	//Zバッファ法を用いて、3D物体の前後関係を正しく表示するためのもの
	ID3D11Texture2D*		pDepthStencil;

	//【デプスステンシルビュー】
	//デプスステンシルの情報をシェーダーに渡すためのもの
	ID3D11DepthStencilView* pDepthStencilView;

	//【ブレンドステート】
	//半透明のものをどのように表現するか
	ID3D11BlendState*		pBlendState;


	bool		isDrawCollision_ = true;	//コリジョンを表示するか
	bool		_isLighting = false;		//ライティングするか

	//std::array<float, 4> clearColor = { 0.1f, 0.5f, 0.5f, 1.0f }; //R,G,B,A
	XMVECTORF32 clearColor = { 0.1f, 0.5f, 0.5f, 1.0f };

	//SHADER_BUNDLE			shaderBundle[SHADER_MAX] = { 0 };
	std::vector<std::unique_ptr<ShaderBundle>> shaderBundles;
	std::unordered_map<SHADER_FILTER, std::unique_ptr<FilterShader>> filterShaders;
	SHADER_FILTER currentFilterType = SHADER_FILTER::NONE;
	bool isActivePostEffect = false;
	bool postDraw = false;

	//extern宣言した変数の初期化
	ID3D11Device*           pDevice_ = nullptr;
	ID3D11DeviceContext*    pContext_ = nullptr;
	int						screenWidth_ = 0;
	int						screenHeight_ = 0;
	
	//ビューポート
	//レンダリング結果を表示する範囲
	std::array<D3D11_VIEWPORT, VIEW_PORT_MAX> viewPorts;
	VIEW_PORT_TYPE			currentViewPort;

	//初期化処理
	HRESULT Direct3D::Initialize(HWND hWnd, int screenWidth, int screenHeight)
	{
		///////////////////////////いろいろ準備するための設定///////////////////////////////
		//いろいろな設定項目をまとめた構造体
		DXGI_SWAP_CHAIN_DESC scDesc;

		//とりあえず全部0
		ZeroMemory(&scDesc, sizeof(scDesc));

		//描画先のフォーマット
		scDesc.BufferDesc.Width = screenWidth;		//画面幅
		scDesc.BufferDesc.Height = screenHeight;		//画面高さ
		scDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;	// 何色使えるか

		//FPS（1/60秒に1回）
		scDesc.BufferDesc.RefreshRate.Numerator = 60;
		scDesc.BufferDesc.RefreshRate.Denominator = 1;

		//その他
		scDesc.Windowed = TRUE;				//ウィンドウモードかフルスクリーンか
		scDesc.OutputWindow = hWnd;			//ウィンドウハンドル
		scDesc.BufferCount = 1;				//裏画面の枚数
		scDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;	//画面に描画するために使う
		scDesc.SampleDesc.Count = 1;		//MSAA（アンチエイリアス）の設定
		scDesc.SampleDesc.Quality = 0;		//　〃



		///////////////////////////上記設定をもとにデバイス、コンテキスト、スワップチェインを作成///////////////////////////////
		D3D_FEATURE_LEVEL level;
		HRESULT  hr;
		hr = D3D11CreateDeviceAndSwapChain(
			nullptr,					// どのビデオアダプタを使用するか？既定ならばnullptrで
			D3D_DRIVER_TYPE_HARDWARE,	// ドライバのタイプを渡す。これ以外は基本的にソフトウェア実装で、どうしてもという時やデバグ用に用いるべし.
			nullptr,					// 上記をD3D_DRIVER_TYPE_SOFTWAREに設定した際に、その処理を行うDLLのハンドルを渡す。それ以外を指定している際には必ずnullptrを渡す.
			0,							// 何らかのフラグを指定する。（デバッグ時はD3D11_CREATE_DEVICE_DEBUG？）
			nullptr,					// デバイス、コンテキストのレベルを設定。nullptrにしとけば可能な限り高いレベルにしてくれる
			0,							// 上の引数でレベルを何個指定したか
			D3D11_SDK_VERSION,			// SDKのバージョン。必ずこの値
			&scDesc,					// 上でいろいろ設定した構造体
			&pSwapChain_,				// 無事完成したSwapChainのアドレスが返ってくる
			&pDevice_,					// 無事完成したDeviceアドレスが返ってくる
			&level,						// 無事完成したDevice、Contextのレベルが返ってくる
			&pContext_);				// 無事完成したContextのアドレスが返ってくる

		//失敗したら終了
		if (FAILED(hr))	return hr;


		///////////////////////////描画のための準備///////////////////////////////
		//スワップチェーンからバックバッファを取得（バックバッファ ＝ 裏画面 ＝ 描画先）
		ID3D11Texture2D* pBackBuffer;
		hr = pSwapChain_->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
		
		D3D11_TEXTURE2D_DESC bbDesc;
		pBackBuffer->GetDesc(&bbDesc);

		//失敗したら終了
		if (FAILED(hr))	return hr;

		//レンダーターゲットビューを作成
		hr = pDevice_->CreateRenderTargetView(pBackBuffer, NULL, &pRenderTargetView_);

		//失敗したら終了
		if (FAILED(hr))	return hr;

		//一時的にバックバッファを取得しただけなので、解放
		pBackBuffer->Release();




		/////////////////////////////////////////////////////////////////////////////////////////////


		// ビューポートの設定
		//レンダリング結果を表示する範囲
		viewPorts[VIEW_PORT_FULL] = {
			0.f,
			0.f,
			(FLOAT)screenWidth,
			(FLOAT)screenHeight,
			0.f,
			1.f
		};
		viewPorts[VIEW_PORT_LEFT] = {
			0.f,
			0.f,
			((FLOAT)screenWidth) / 2,
			(FLOAT)screenHeight,
			0.f,
			1.f
		};
		viewPorts[VIEW_PORT_RIGHT] = {
			((FLOAT)screenWidth) / 2,
			0.f,
			((FLOAT)screenWidth) / 2.f,
			(FLOAT)screenHeight,
			0.f,
			1.f
		};
		viewPorts[VIEW_PORT_TOP] = {
			0,
			0,
			(FLOAT)screenWidth,
			(FLOAT)screenHeight / 2.f,
			0,
			1.f
		};
		viewPorts[VIEW_PORT_BOTTOM] = {
			0,
			(FLOAT)screenHeight / 2.f,
			(FLOAT)screenWidth,
			(FLOAT)screenHeight / 2.f,
			0,
			1.f
		};

		for (auto&& vp : viewPorts) {

			Camera::AddCamera(vp);
		}


		screenWidth_ = screenWidth;
		screenHeight_ = screenHeight;



		//深度ステンシルビューの作成
		D3D11_TEXTURE2D_DESC descDepth;
		descDepth.Width = screenWidth;
		descDepth.Height = screenHeight;
		descDepth.MipLevels = 1;
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT_D32_FLOAT;
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;
		pDevice_->CreateTexture2D(&descDepth, NULL, &pDepthStencil);
		pDevice_->CreateDepthStencilView(pDepthStencil, NULL, &pDepthStencilView);


		//ブレンドステート
		D3D11_BLEND_DESC BlendDesc;
		ZeroMemory(&BlendDesc, sizeof(BlendDesc));
		BlendDesc.AlphaToCoverageEnable = FALSE;
		BlendDesc.IndependentBlendEnable = FALSE;
		BlendDesc.RenderTarget[0].BlendEnable = TRUE;
		BlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		BlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		BlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		BlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		BlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		BlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		BlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		pDevice_->CreateBlendState(&BlendDesc, &pBlendState);
		float blendFactor[4] = { D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO };
		pContext_->OMSetBlendState(pBlendState, blendFactor, 0xffffffff);


		//パイプラインの構築
		//データを画面に描画するための一通りの設定
		pContext_->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);  // データの入力種類を指定
		pContext_->OMSetRenderTargets(1, &pRenderTargetView_, pDepthStencilView);            // 描画先を設定（今後はレンダーターゲットビューを介して描画してね）
		pContext_->RSSetViewports(1, &viewPorts[VIEW_PORT_FULL]);                                      // ビューポートのセット
		currentViewPort = VIEW_PORT_FULL;



		//各パターンのシェーダーセット準備
		InitShaderBundle();
		Direct3D::SetShader(Direct3D::SHADER_3D);



		//コリジョン表示するか
		isDrawCollision_ = GetPrivateProfileInt("DEBUG", "ViewCollider", 0, ".\\setup.ini") != 0;


		return S_OK;
	}


	//シェーダー関連で必要なセット準備
	void InitShaderBundle()
	{
		shaderBundles.emplace_back(std::make_unique<ShaderSimple3D>());
		shaderBundles.emplace_back(std::make_unique<ShaderSimple2D>());
		shaderBundles.emplace_back(std::make_unique<ShaderDebug3D>());

		for (const auto& shaderBundle : shaderBundles) {

			shaderBundle->Initialize(pDevice_);
		}

		filterShaders[SHADER_FILTER::NEGATIVE]		= std::make_unique<FilterNegative>();
		filterShaders[SHADER_FILTER::MONOTONE]		= std::make_unique<FilterMonotone>();
		filterShaders[SHADER_FILTER::OIL]			= std::make_unique<FilterOilPaint>();
		filterShaders[SHADER_FILTER::GAMING]		= std::make_unique<FilterGaming>();
		filterShaders[SHADER_FILTER::SEPIA]			= std::make_unique<FilterSepia>();
		filterShaders[SHADER_FILTER::THREAHOLD]		= std::make_unique<FilterThreshold>();
		filterShaders[SHADER_FILTER::MAYFLY]		= std::make_unique<FilterMayfly>();
		filterShaders[SHADER_FILTER::MOSAIC]		= std::make_unique<FilterMosaic>();
		filterShaders[SHADER_FILTER::BAYER]			= std::make_unique<FilterBayer>();
		filterShaders[SHADER_FILTER::CRT]			= std::make_unique<FilterCRT>();
		filterShaders[SHADER_FILTER::POSTERIZATION]	= std::make_unique<FilterPosterization>();

		for (const auto& filterShader : filterShaders) {

			filterShader.second->Initialize(pDevice_);
		}
	}


	//今から描画するShaderBundleを設定
	void SetShader(SHADER_TYPE type)
	{
		//pContext_->RSSetState(shaderBundle[type].pRasterizerState);
		//pContext_->VSSetShader(shaderBundle[type].pVertexShader, NULL, 0);                         // 頂点シェーダをセット
		//pContext_->PSSetShader(shaderBundle[type].pPixelShader, NULL, 0);                          // ピクセルシェーダをセット
		//pContext_->IASetInputLayout(shaderBundle[type].pVertexLayout);

		shaderBundles[type]->SetShaderBundle(pContext_);
	}


	//描画開始
	void BeginDraw()
	{
		//何か準備できてないものがあったら諦める
		if (NULL == pDevice_) return;
		if (NULL == pContext_) return;
		if (NULL == pRenderTargetView_) return;
		if (NULL == pSwapChain_) return;

		//画面をクリア
		pContext_->ClearRenderTargetView(pRenderTargetView_, clearColor);

		//深度バッファクリア
		pContext_->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
		
		if (isActivePostEffect && currentFilterType != SHADER_FILTER::NONE) {

			filterShaders[currentFilterType]->ClearRenderTarget(pContext_, clearColor);
			filterShaders[currentFilterType]->SetRenderTargetView(pContext_, pDepthStencilView);
		}
		postDraw = false;
	}

	void DrawPostEffect() {

		if (isActivePostEffect && currentFilterType != SHADER_FILTER::NONE) {

			pContext_->OMSetRenderTargets(1, &pRenderTargetView_, nullptr);
			filterShaders[currentFilterType]->Render(pContext_);
			pContext_->OMSetRenderTargets(1, &pRenderTargetView_, pDepthStencilView);
			SetShader(SHADER_3D);
		}
		postDraw = true;
	}


	//描画終了
	void EndDraw()
	{
		//スワップ（バックバッファを表に表示する）
		pSwapChain_->Present(0, 0);
	}


	//開放処理
	void Release()
	{
		SAFE_RELEASE(pDepthStencil);
		SAFE_RELEASE(pDepthStencilView);
		SAFE_RELEASE(pBlendState);
		SAFE_RELEASE(pRenderTargetView_);
		SAFE_RELEASE(pSwapChain_);

		/*for (int i = 0; i < SHADER_MAX; i++)
		{
			SAFE_RELEASE(shaderBundle[i].pRasterizerState);
			SAFE_RELEASE(shaderBundle[i].pVertexLayout);
			SAFE_RELEASE(shaderBundle[i].pVertexShader);
			SAFE_RELEASE(shaderBundle[i].pPixelShader);
		}*/

		for (auto&& shaderBundle : shaderBundles) {

			shaderBundle.reset();
		}

		if (pContext_)
		{
			pContext_->ClearState();
		}

		SAFE_RELEASE(pRenderTargetView_);
		SAFE_RELEASE(pSwapChain_);
		SAFE_RELEASE(pContext_);
		SAFE_RELEASE(pDevice_);
	}


	//三角形と線分の衝突判定（衝突判定に使用）
	//https://pheema.hatenablog.jp/entry/ray-triangle-intersection
	bool Intersect(XMVECTOR & start, XMVECTOR & direction, XMVECTOR & v0, XMVECTOR & v1, XMVECTOR & v2, float* distance)
	{
		// 微小な定数([M?ller97] での値)
		constexpr float kEpsilon = 1e-6f;

		//三角形の２辺
		XMVECTOR edge1 = XMVectorSet(v1.vecX - v0.vecX, v1.vecY - v0.vecY, v1.vecZ - v0.vecZ, 0.0f);
		XMVECTOR edge2 = XMVectorSet(v2.vecX - v0.vecX, v2.vecY - v0.vecY, v2.vecZ - v0.vecZ, 0.0f);

		XMVECTOR alpha = XMVector3Cross(direction, edge2);
		float det = XMVector3Dot(edge1, alpha).m128_f32[0];

		// 三角形に対して、レイが平行に入射するような場合 det = 0 となる。
		// det が小さすぎると 1/det が大きくなりすぎて数値的に不安定になるので
		// det ? 0 の場合は交差しないこととする。
		if (-kEpsilon < det && det < kEpsilon) 
		{
			return false;
		}

		float invDet = 1.0f / det;
		XMVECTOR r = start - v0;

		// u が 0 <= u <= 1 を満たしているかを調べる。
		float u = XMVector3Dot(alpha, r).vecX * invDet;
		if (u < 0.0f || u > 1.0f)
		{
			return false;
		}

		XMVECTOR beta = XMVector3Cross(r, edge1);

		// v が 0 <= v <= 1 かつ u + v <= 1 を満たすことを調べる。
		// すなわち、v が 0 <= v <= 1 - u をみたしているかを調べればOK。
		float v = XMVector3Dot(direction, beta).vecX * invDet;
		if (v < 0.0f || u + v > 1.0f) 
		{
			return false;
		}

		// t が 0 <= t を満たすことを調べる。
		float t = XMVector3Dot(edge2, beta).vecX * invDet;
		if (t < 0.0f) 
		{
			return false;
		}

		*distance = t;
		return true;
	}

	//Zバッファへの書き込みON/OFF
	void SetDepthBafferWriteEnable(bool isWrite)
	{
		////ON
		//if (isWrite)
		//{
		//	//Zバッファ（デプスステンシルを指定する）
		//	pContext_->OMSetRenderTargets(1, &pRenderTargetView_, pDepthStencilView);
		//}

		////OFF
		//else
		//{
		//	pContext_->OMSetRenderTargets(1, &pRenderTargetView_, nullptr);
		//}

		//Zバッファへ書き込むならビューを設定
		ID3D11DepthStencilView* depthStencil = isWrite ? pDepthStencilView : nullptr;

		//ポストエフェクトが設定されているかつ最終描画でないなら
		if (isActivePostEffect && currentFilterType != SHADER_FILTER::NONE && !postDraw) {

			filterShaders[currentFilterType]->SetRenderTargetView(pContext_, depthStencil);
		}
		else {

			pContext_->OMSetRenderTargets(1, &pRenderTargetView_, depthStencil);
		}
	}

	//描画範囲の設定
	void SetViewPortType(VIEW_PORT_TYPE type) {

		pContext_->RSSetViewports(1, &viewPorts[type]);
		currentViewPort = type;
	}

	//現在のビューポートのタイプ
	VIEW_PORT_TYPE GetCurrentViewPort() {

		return currentViewPort;
	}
	void SetClearScreenColor(FXMVECTOR color) {

		clearColor.v = color;

		/*clearColor[0] = color.x;
		clearColor[1] = color.y;
		clearColor[2] = color.z;
		clearColor[3] = color.w;*/
	}

	void SetDefaultRenderTargetView() {

		pContext_->OMGetRenderTargets(1, &pRenderTargetView_, &pDepthStencilView);
	}

	void InvalidatePostEffect() {

		isActivePostEffect = false;
	}

	void ValidatePostEffect() {

		isActivePostEffect = true;
	}

	void SetPostEffect(SHADER_FILTER type) {

		currentFilterType = type;
	}

	const SHADER_FILTER& GetCurrentFilterType()
	{
		return currentFilterType;
	}
}
