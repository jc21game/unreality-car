#pragma once
#include <chrono>

using namespace std::chrono;

namespace ChronoTimer
{
	float GetTimeSec();
	float GetElapsed(float start, float end);
};