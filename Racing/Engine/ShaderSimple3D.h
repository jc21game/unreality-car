#pragma once
#include "ShaderBundle.h"

class ShaderSimple3D final : public ShaderBundle {

public:
	~ShaderSimple3D() = default;
	
	bool Initialize(ID3D11Device* d3dDevice) override;
};