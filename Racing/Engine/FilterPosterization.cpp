#include "FilterPosterization.h"

#include <DirectXCollision.h>
#include <d3d11_1.h>

void FilterPosterization::CreateConstantBuffer(ID3D11Device * d3dDevice) {
}

bool FilterPosterization::Initialize(ID3D11Device * d3dDevice) {

	UINT vectorSize = sizeof DirectX::XMVECTOR;

	CreateIndexBuffer(d3dDevice);
	CreateVertexBuffer(d3dDevice);
	CreateRenderTargetView(d3dDevice);

	std::vector<D3D11_INPUT_ELEMENT_DESC> layout = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, vectorSize * 1, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	bool result = CreateVertexShader(d3dDevice, L"Shader/FilterPosterization.hlsl", layout);
	if (!result) return false;
	result = CreatePixelShader(d3dDevice, L"Shader/FilterPosterization.hlsl");
	if (!result) return false;
	result = CreateRasterizerState(d3dDevice, D3D11_CULL_BACK);
	if (!result) return false;

	D3D11_SAMPLER_DESC  samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	if (FAILED(d3dDevice->CreateSamplerState(&samplerDesc, &samplerState))) return false;

	return true;
}

void FilterPosterization::Render(ID3D11DeviceContext* d3dContext) {

	SetShaderBundle(d3dContext);
	SetBuffers(d3dContext);

	d3dContext->PSSetShaderResources(0, 1, shaderResourceView_.GetAddressOf());
	d3dContext->PSSetSamplers(0, 1, samplerState.GetAddressOf());

	d3dContext->DrawIndexed(6, 0, 0);
}
