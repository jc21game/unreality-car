#pragma once
#include "FilterShader.h"

class FilterOilPaint : public FilterShader {

private:
	Microsoft::WRL::ComPtr<ID3D11SamplerState> samplerState;
	Microsoft::WRL::ComPtr<ID3D11Texture2D> map_;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> mapView_;

public:
	virtual ~FilterOilPaint() = default;

	void CreateConstantBuffer(ID3D11Device* d3dDevice) override;

	bool Initialize(ID3D11Device* d3dDevice) override;
	void Render(ID3D11DeviceContext* d3dContext) override;
};

