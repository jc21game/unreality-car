#include "ColliderOrientedBox.h"

#include "Model.h"
#include "ColliderSphere.h"
#include "ColliderBox.h"


ColliderOrientedBox::ColliderOrientedBox(BoundingOrientedBox& bob, XMFLOAT3 rotate, bool isRotateSync) {

	center_ = bob.Center;
	bOrientedBox_ = bob;
	rotate_ = rotate;
	isRotateSync_ = isRotateSync;

#ifdef _DEBUG

	hDebugModel_ = Model::Load("DebugCollision/BoxCollider.fbx");
#endif
}


ColliderOrientedBox::ColliderOrientedBox(FXMVECTOR center, FXMVECTOR extents, FXMVECTOR rotate)
	: isRotateSync_(false) {

	XMStoreFloat3(&rotate_, rotate);

	XMStoreFloat3(&center_, center);
	bOrientedBox_.Center = center_;
	XMStoreFloat3(&bOrientedBox_.Extents, extents);

	XMVECTOR eulerAngle = g_XMZero;
	eulerAngle.vecX = XMConvertToRadians(rotate.vecX);
	eulerAngle.vecY = XMConvertToRadians(rotate.vecY);
	eulerAngle.vecZ = XMConvertToRadians(rotate.vecZ);
	
	auto orientation = XMQuaternionRotationRollPitchYawFromVector(eulerAngle);
	XMStoreFloat4(&bOrientedBox_.Orientation, orientation);
	

#ifdef _DEBUG

	hDebugModel_ = Model::Load("DebugCollision/BoxCollider.fbx");
#endif
}

bool ColliderOrientedBox::IsHit(Collider* pTarget) {

	if (auto* sphere = dynamic_cast<ColliderSphere*>(pTarget)) {

		return IsHitSphereVsOrientedBox(sphere, this);
	}
	if (auto* box = dynamic_cast<ColliderBox*>(pTarget)) {

		return IsHitBoxVsOrientedBox(box, this);
	}
	if (auto* orientedBox = dynamic_cast<ColliderOrientedBox*>(pTarget)) {

		return IsHitOrientedBoxes(this, orientedBox);
	}
	return false;
}

void ColliderOrientedBox::Draw() {

	Transform transform;
	transform.scale_ = XMLoadFloat3(&bOrientedBox_.Extents) * 2;
	transform.position_ = GetWorldPosition();
	transform.rotate_ = XMLoadFloat3(&rotate_);
	transform.Calclation();

	Model::SetTransform(hDebugModel_, transform);
	Model::Draw(hDebugModel_);
}

void ColliderOrientedBox::TranslationPosition() {

	XMStoreFloat3(&bOrientedBox_.Center, GetWorldPosition());
}

Collider* ColliderOrientedBox::Copy() {

	return new ColliderOrientedBox(bOrientedBox_, { 0, 0, 0 }, isRotateSync_);
}

void ColliderOrientedBox::SetRotate(FXMVECTOR rotate) {

	XMStoreFloat3(&rotate_, rotate);
	
	XMVECTOR eulerAngle = g_XMZero;
	eulerAngle.vecX = XMConvertToRadians(rotate.vecX);
	eulerAngle.vecY = XMConvertToRadians(rotate.vecY);
	eulerAngle.vecZ = XMConvertToRadians(rotate.vecZ);

	auto orientation = XMQuaternionRotationRollPitchYawFromVector(eulerAngle);
	XMStoreFloat4(&bOrientedBox_.Orientation, orientation);
}
