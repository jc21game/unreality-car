#pragma once
#include "ShaderBundle.h"

class ShaderSimple2D final : public ShaderBundle {

public:
	~ShaderSimple2D() = default;

	bool Initialize(ID3D11Device* d3dDevice) override;
};