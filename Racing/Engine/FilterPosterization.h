#pragma once
#include "FilterShader.h"

class FilterPosterization : public FilterShader {

private:
	Microsoft::WRL::ComPtr<ID3D11SamplerState> samplerState;

public:
	virtual ~FilterPosterization() = default;

	void CreateConstantBuffer(ID3D11Device* d3dDevice) override;

	bool Initialize(ID3D11Device* d3dDevice) override;
	void Render(ID3D11DeviceContext* d3dContext) override;
};

