#include "ShaderSimple2D.h"

#include <DirectXCollision.h>

bool ShaderSimple2D::Initialize(ID3D11Device* d3dDevice) {

	UINT vectorSize = sizeof DirectX::XMVECTOR;
	
	std::vector<D3D11_INPUT_ELEMENT_DESC> layout = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, vectorSize * 1, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	bool result = CreateVertexShader(d3dDevice, L"Shader/Simple2D.hlsl", layout);
	if (!result) return false;
	result = CreatePixelShader(d3dDevice, L"Shader/Simple2D.hlsl");
	if (!result) return false;
	result = CreateRasterizerState(d3dDevice, D3D11_CULL_BACK);
	if (!result) return false;

	return true;
}
