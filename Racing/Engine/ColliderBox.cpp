#include "ColliderBox.h"

#include "Model.h"
#include "ColliderSphere.h"
#include "ColliderOrientedBox.h"

ColliderBox::ColliderBox(BoundingBox& bb) {

	center_ = bb.Center;
	bBox_ = bb;

#ifdef _DEBUG

	hDebugModel_ = Model::Load("DebugCollision/BoxCollider.fbx");
#endif
}

ColliderBox::ColliderBox(FXMVECTOR center, FXMVECTOR extents) {

	XMStoreFloat3(&center_, center);
	bBox_.Center = center_;
	XMStoreFloat3(&bBox_.Extents, extents);

#ifdef _DEBUG

	hDebugModel_ = Model::Load("DebugCollision/BoxCollider.fbx");
#endif
}

bool ColliderBox::IsHit(Collider* pTarget) {

	if (auto* sphere = dynamic_cast<ColliderSphere*>(pTarget)) {

		return IsHitSphereVsBox(sphere, this);
	}
	if (auto* box = dynamic_cast<ColliderBox*>(pTarget)) {

		return IsHitBoxes(this, box);
	}
	if (auto* orientedBox = dynamic_cast<ColliderOrientedBox*>(pTarget)) {

		return IsHitBoxVsOrientedBox(this, orientedBox);
	}
	return false;
}

void ColliderBox::Draw() {

	Transform transform;
	transform.scale_ = XMLoadFloat3(&bBox_.Extents) * 2;
	transform.position_ = GetWorldPosition();
	transform.Calclation();

	Model::SetTransform(hDebugModel_, transform);
	Model::Draw(hDebugModel_);
}

void ColliderBox::TranslationPosition() {

	XMStoreFloat3(&bBox_.Center, GetWorldPosition());
}

Collider* ColliderBox::Copy() {

	return new ColliderBox(bBox_);
}
