#include "ShaderBundle.h"

#include <d3d11.h>
#include <d3dcompiler.h>
#include <wrl/client.h>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

using Microsoft::WRL::ComPtr;

struct ShaderBundle::Impl {

	ComPtr<ID3D11InputLayout>		inputLayout;
	ComPtr<ID3D11VertexShader>		vertexShader;
	ComPtr<ID3D11PixelShader>		pixelShader;
	ComPtr<ID3D11RasterizerState>	rasterizerState;
};

ShaderBundle::ShaderBundle() {

	pImpl_ = std::make_unique<Impl>();
}

ShaderBundle::~ShaderBundle() = default;

void ShaderBundle::SetShaderBundle(ID3D11DeviceContext* d3dDevice) const {

	d3dDevice->IASetInputLayout(pImpl_->inputLayout.Get());
	d3dDevice->VSSetShader(pImpl_->vertexShader.Get(), nullptr, 0);
	d3dDevice->PSSetShader(pImpl_->pixelShader.Get(), nullptr, 0);
	d3dDevice->RSSetState(pImpl_->rasterizerState.Get());
}

bool ShaderBundle::CreateVertexShader(ID3D11Device* d3dDevice, const std::wstring& srcFile, const std::vector<D3D11_INPUT_ELEMENT_DESC>& layoutDesc) const {

	ComPtr<ID3DBlob> blob;
	D3DCompileFromFile(srcFile.c_str(), nullptr, nullptr, "VS", "vs_5_0", 0, 0, &blob, nullptr);
	HRESULT hr = d3dDevice->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, &pImpl_->vertexShader);
	if (FAILED(hr))	return false;
	
	hr = d3dDevice->CreateInputLayout(layoutDesc.data(), static_cast<UINT>(layoutDesc.size()), blob->GetBufferPointer(), blob->GetBufferSize(), &pImpl_->inputLayout);
	if (FAILED(hr))	return false;

	return true;
}

bool ShaderBundle::CreatePixelShader(ID3D11Device* d3dDevice, const std::wstring& srcFile) const {

	ComPtr<ID3DBlob> blob;
	D3DCompileFromFile(srcFile.c_str(), nullptr, nullptr, "PS", "ps_5_0", 0, 0, &blob, nullptr);
	HRESULT hr = d3dDevice->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, &pImpl_->pixelShader);
	if (FAILED(hr))	return false;

	return true;
}

bool ShaderBundle::CreateRasterizerState(ID3D11Device* d3dDevice, D3D11_CULL_MODE cullMode, D3D11_FILL_MODE fillMode) const {

	D3D11_RASTERIZER_DESC RasterizerDesc;

	::ZeroMemory(&RasterizerDesc, sizeof(RasterizerDesc));
	RasterizerDesc.FillMode = fillMode;            
	RasterizerDesc.CullMode = cullMode;  
	RasterizerDesc.FrontCounterClockwise = TRUE;  
	RasterizerDesc.DepthBias = 0;
	RasterizerDesc.DepthBiasClamp = 0;
	RasterizerDesc.SlopeScaledDepthBias = 0;
	RasterizerDesc.DepthClipEnable = TRUE;
	RasterizerDesc.ScissorEnable = FALSE; 
	RasterizerDesc.MultisampleEnable = FALSE;
	RasterizerDesc.AntialiasedLineEnable = FALSE;

	HRESULT hr = d3dDevice->CreateRasterizerState(&RasterizerDesc, &pImpl_->rasterizerState);
	if (FAILED(hr))	return false;

	return true;
}
