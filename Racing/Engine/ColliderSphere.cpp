#include "ColliderSphere.h"

#include "Model.h"
#include "ColliderBox.h"
#include "ColliderOrientedBox.h"


ColliderSphere::ColliderSphere(BoundingSphere & bs) {

	center_ = bs.Center;
	bSphere_ = bs;

#ifdef _DEBUG

	hDebugModel_ = Model::Load("DebugCollision/SphereCollider.fbx");
#endif
}

ColliderSphere::ColliderSphere(FXMVECTOR center, float radius) {

	XMStoreFloat3(&center_, center);
	bSphere_.Center = center_;
	bSphere_.Radius = radius;

#ifdef _DEBUG

	hDebugModel_ = Model::Load("DebugCollision/SphereCollider.fbx");
#endif
}

bool ColliderSphere::IsHit(Collider* pTarget) {

	if (auto* sphere = dynamic_cast<ColliderSphere*>(pTarget)) {

		return IsHitSpheres(this, sphere);
	}
	if (auto* box = dynamic_cast<ColliderBox*>(pTarget)) {

		return IsHitSphereVsBox(this, box);
	}
	if (auto* orientedBox = dynamic_cast<ColliderOrientedBox*>(pTarget)) {

		return IsHitSphereVsOrientedBox(this, orientedBox);
	}
	return false;
}

void ColliderSphere::Draw() {

	Transform transform;
	transform.scale_ = XMVectorReplicate(bSphere_.Radius * 2);
	transform.position_ = GetWorldPosition();
	transform.Calclation();

	Model::SetTransform(hDebugModel_, transform);
	Model::Draw(hDebugModel_);
}

void ColliderSphere::TranslationPosition() {

	XMStoreFloat3(&bSphere_.Center, GetWorldPosition());
}

Collider* ColliderSphere::Copy() {
	
	return new ColliderSphere(bSphere_);
}
