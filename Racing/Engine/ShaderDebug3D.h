#pragma once
#include "ShaderBundle.h"

class ShaderDebug3D final : public ShaderBundle {

public:
	~ShaderDebug3D() = default;

	bool Initialize(ID3D11Device* d3dDevice) override;
};