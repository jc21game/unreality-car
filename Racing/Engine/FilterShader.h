#pragma once
#include <wrl/client.h>
#include <DirectXMath.h>

#include "ShaderBundle.h"


class FilterShader : public ShaderBundle {

protected:
	Microsoft::WRL::ComPtr<ID3D11Buffer> vertexBuffer_;
	Microsoft::WRL::ComPtr<ID3D11Buffer> indexBuffer_;
	Microsoft::WRL::ComPtr<ID3D11Buffer> constantBuffer_;
	Microsoft::WRL::ComPtr<ID3D11Texture2D> renderTarget_;
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView> renderTargetView_;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> shaderResourceView_;

public:
	virtual ~FilterShader() = default;

	void CreateVertexBuffer(ID3D11Device* d3dDevice);
	void CreateIndexBuffer(ID3D11Device* d3dDevice);
	virtual void CreateConstantBuffer(ID3D11Device* d3dDevice) = 0;
	void CreateRenderTargetView(ID3D11Device* d3dDevice);

	virtual void SetBuffers(ID3D11DeviceContext* d3dContext);
	void SetRenderTargetView(ID3D11DeviceContext* d3dContext, ID3D11DepthStencilView* depthStencil);
	void ClearRenderTarget(ID3D11DeviceContext* d3dContext, DirectX::XMVECTORF32 clearColor);

	virtual void Render(ID3D11DeviceContext* d3dContext) = 0;
};

