#include "Camera.h"
#include <vector>

//XMVECTOR _position[Direct3D::VIEW_PORT_MAX];
//XMVECTOR _target[Direct3D::VIEW_PORT_MAX];
//XMMATRIX _view[Direct3D::VIEW_PORT_MAX];
//XMMATRIX _proj[Direct3D::VIEW_PORT_MAX];

//std::array<XMVECTOR, Direct3D::VIEW_PORT_MAX> _position;
//std::array<XMVECTOR, Direct3D::VIEW_PORT_MAX> _target;
//std::array<XMMATRIX, Direct3D::VIEW_PORT_MAX> _view;
//std::array<XMMATRIX, Direct3D::VIEW_PORT_MAX> _proj;

namespace Camera {

	std::vector<Cam> cameras;

	//初期化（プロジェクション行列作成）
	void Initialize() {

		for (auto&& camera : cameras) {

			camera.Initialize();
		}
	}

	void AddCamera(const D3D11_VIEWPORT& viewport) {

		cameras.emplace_back(viewport);
	}

	//更新（ビュー行列作成）
	void Update() {
		//ビュー行列
		/*for (auto i = 0u; i < Direct3D::VIEW_PORT_MAX; ++i) {

			_view[i] = XMMatrixLookAtLH(_position[i], _target[i], XMVectorSet(0, 1, 0, 0));
		}*/

		for (auto&& camera : cameras) {

			camera.Update();
		}
	}

	//焦点を設定
	void SetTarget(XMVECTOR target, Direct3D::VIEW_PORT_TYPE type = Direct3D::VIEW_PORT_FULL) {
		
		cameras[type].SetTarget(target); 
	}

	//位置を設定
	void SetPosition(XMVECTOR position, Direct3D::VIEW_PORT_TYPE type = Direct3D::VIEW_PORT_FULL) { 
		
		cameras[type].SetPosition(position);
	}

	//焦点を取得
	XMVECTOR GetTarget(Direct3D::VIEW_PORT_TYPE type = Direct3D::VIEW_PORT_FULL) { 

		return cameras[type].GetTarget(); 
	}
	XMVECTOR GetPosition() { 

		return cameras[Direct3D::GetCurrentViewPort()].GetPosition(); 
	}

	//位置を取得
	XMVECTOR GetPosition(Direct3D::VIEW_PORT_TYPE type = Direct3D::VIEW_PORT_FULL) {
		
		return cameras[type].GetPosition();
	}

	//ビュー行列を取得
	XMMATRIX GetViewMatrix() { 
		
		return cameras[Direct3D::GetCurrentViewPort()].GetViewMatrix(); 
	}

	//プロジェクション行列を取得
	XMMATRIX GetProjectionMatrix() { 
		
		return cameras[Direct3D::GetCurrentViewPort()].GetProjectionMatrix(); 
	}


	Cam::Cam(const D3D11_VIEWPORT& viewport)
		: width_(viewport.Width), height_(viewport.Height)
		, position_(XMVectorZero()), target_(XMVectorZero())
		, view_(XMMatrixIdentity()), proj_(XMMatrixIdentity()) {

		Initialize();
	}

	void Cam::Initialize() {

		position_ = XMVectorSet(0, 3, -10, 0);
		target_ = XMVectorSet(0, 0, 0, 0);
		view_ = XMMatrixLookAtLH(position_, target_, XMVectorSet(0, 1, 0, 0));
		proj_ = XMMatrixPerspectiveFovLH(XM_PIDIV4, width_ / height_, 0.1f, 1000.0f);
	}

	void Cam::Update() {

		view_ = XMMatrixLookAtLH(position_, target_, XMVectorSet(0, 1, 0, 0));
	}

	void Cam::SetPosition(XMVECTOR position) {

		position_ = position;
	}

	void Cam::SetTarget(XMVECTOR target) {

		target_ = target;
	}

	XMVECTOR Cam::GetPosition() const {

		return position_;
	}

	XMVECTOR Cam::GetTarget() const {

		return target_;
	}

	XMMATRIX Cam::GetViewMatrix() const {

		return view_;
	}

	XMMATRIX Cam::GetProjectionMatrix() const {

		return proj_;
	}
}