#include "FilterMayfly.h"

#include <DirectXCollision.h>
#include <d3d11_1.h>

#include "WICTextureLoader11.h"
#include "SceneTimer.h"

struct CBuffer {
	DirectX::XMVECTOR time;
};

void FilterMayfly::CreateConstantBuffer(ID3D11Device* d3dDevice) {

	//必要な設定項目
	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CBuffer);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	// 定数バッファの作成
	d3dDevice->CreateBuffer(&cb, nullptr, &constantBuffer_);
}

bool FilterMayfly::Initialize(ID3D11Device* d3dDevice) {

	UINT vectorSize = sizeof DirectX::XMVECTOR;

	CreateIndexBuffer(d3dDevice);
	CreateVertexBuffer(d3dDevice);
	CreateConstantBuffer(d3dDevice);
	CreateRenderTargetView(d3dDevice);

	std::vector<D3D11_INPUT_ELEMENT_DESC> layout = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, vectorSize * 1, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	bool result = CreateVertexShader(d3dDevice, L"Shader/FilterMayfly.hlsl", layout);
	if (!result) return false;
	result = CreatePixelShader(d3dDevice, L"Shader/FilterMayfly.hlsl");
	if (!result) return false;
	result = CreateRasterizerState(d3dDevice, D3D11_CULL_BACK);
	if (!result) return false;

	if (FAILED(DirectX::CreateWICTextureFromFile(d3dDevice, L"Shader/Map/oil_paint.png", reinterpret_cast<ID3D11Resource**>(map_.GetAddressOf()), &mapView_))) {

		return false;
	}

	D3D11_SAMPLER_DESC  samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
	if (FAILED(d3dDevice->CreateSamplerState(&samplerDesc, &samplerState))) return false;

	return true;
}

void FilterMayfly::Render(ID3D11DeviceContext* d3dContext) {

	SetShaderBundle(d3dContext);
	SetBuffers(d3dContext);

	d3dContext->PSSetShaderResources(0, 1, shaderResourceView_.GetAddressOf());
	d3dContext->PSSetShaderResources(1, 1, mapView_.GetAddressOf());
	d3dContext->PSSetSamplers(0, 1, samplerState.GetAddressOf());

	CBuffer cb;
	D3D11_MAPPED_SUBRESOURCE pdata;
	cb.time.m128_f32[0] = SceneTimer::GetElapsedSecounds();

	d3dContext->Map(constantBuffer_.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);	// GPUからのリソースアクセスを一時止める
	memcpy_s(pdata.pData, pdata.RowPitch, reinterpret_cast<void*>(&cb), sizeof(cb));				// リソースへ値を送る

	d3dContext->Unmap(constantBuffer_.Get(), 0);

	d3dContext->DrawIndexed(6, 0, 0);
}
