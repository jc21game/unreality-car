#pragma once
#include "GameObject.h"
#include "Direct3D.h"

//-----------------------------------------------------------
//カメラ
//-----------------------------------------------------------
namespace Camera
{
	//初期化（プロジェクション行列作成）
	void Initialize();

	//カメラを追加する
	void AddCamera(const D3D11_VIEWPORT& viewport);

	//更新（ビュー行列作成）
	void Update();

	//視点（カメラの位置）を設定
	void SetPosition(XMVECTOR position, Direct3D::VIEW_PORT_TYPE type);

	//焦点（見る位置）を設定
	void SetTarget(XMVECTOR target, Direct3D::VIEW_PORT_TYPE type);

	//位置を取得
	XMVECTOR GetPosition(Direct3D::VIEW_PORT_TYPE type);
	XMVECTOR GetPosition();

	//焦点を取得
	XMVECTOR GetTarget(Direct3D::VIEW_PORT_TYPE type);

	//ビュー行列を取得
	XMMATRIX GetViewMatrix();

	//プロジェクション行列を取得
	XMMATRIX GetProjectionMatrix();

	class Cam {

	private:
		FLOAT width_;
		FLOAT height_;
		XMVECTOR position_;
		XMVECTOR target_;
		XMMATRIX view_;
		XMMATRIX proj_;

	public:
		Cam(const D3D11_VIEWPORT& viewport);

		//初期化（プロジェクション行列作成）
		void Initialize();

		//更新（ビュー行列作成）
		void Update();

		//視点（カメラの位置）を設定
		void SetPosition(XMVECTOR position);

		//焦点（見る位置）を設定
		void SetTarget(XMVECTOR target);

		//位置を取得
		XMVECTOR GetPosition() const;

		//焦点を取得
		XMVECTOR GetTarget() const;

		//ビュー行列を取得
		XMMATRIX GetViewMatrix() const;

		//プロジェクション行列を取得
		XMMATRIX GetProjectionMatrix() const;
	};
};