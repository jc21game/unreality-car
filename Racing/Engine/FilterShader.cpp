#include "FilterShader.h"

#include <DirectXCollision.h>

#include <array>

#include "Direct3D.h"

using namespace DirectX;

struct Vertex {
	XMVECTOR position;
	XMVECTOR uv;
};

void FilterShader::CreateVertexBuffer(ID3D11Device* d3dDevice) {

	// 頂点データ宣言
	std::array<Vertex, 4> vertices { {
		{ XMVectorSet(-1.f, 1.f,  0.f, 0.f), XMVectorSet(0.f, 0.f, 0.f, 0.f) },   // 四角形の頂点（左上）
		{ XMVectorSet(1.f,  1.f,  0.f, 0.f), XMVectorSet(1.f, 0.f, 0.f, 0.f) },   // 四角形の頂点（右上）
		{ XMVectorSet(-1.f, -1.f, 0.f, 0.f), XMVectorSet(0.f, 1.f, 0.f, 0.f) },   // 四角形の頂点（左下）
		{ XMVectorSet(1.f,  -1.f, 0.f, 0.f), XMVectorSet(1.f, 1.f, 0.f, 0.f) },   // 四角形の頂点（右下）
	} };


	// 頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;
	bd_vertex.ByteWidth = sizeof(Vertex) * static_cast<UINT>(vertices.size());
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd_vertex.CPUAccessFlags = 0;
	bd_vertex.MiscFlags = 0;
	bd_vertex.StructureByteStride = 0;
	
	D3D11_SUBRESOURCE_DATA data_vertex;
	data_vertex.pSysMem = vertices.data();
	d3dDevice->CreateBuffer(&bd_vertex, &data_vertex, &vertexBuffer_);
}

void FilterShader::CreateIndexBuffer(ID3D11Device* d3dDevice) {

	std::array<int, 6> index {{
		2,1,0, 2,3,1
	} };

	// インデックスバッファを生成する
	D3D11_BUFFER_DESC   bd;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(int) * static_cast<UINT>(index.size());
	bd.BindFlags = D3D10_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = index.data();
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;
	d3dDevice->CreateBuffer(&bd, &InitData, &indexBuffer_);
}

void FilterShader::CreateRenderTargetView(ID3D11Device* d3dDevice) {

	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Width = Direct3D::screenWidth_;
	texDesc.Height = Direct3D::screenHeight_;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;
	HRESULT hr = d3dDevice->CreateTexture2D(&texDesc, nullptr, &renderTarget_);

	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	rtvDesc.Format = texDesc.Format;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	rtvDesc.Texture2D.MipSlice = 0;
	hr = d3dDevice->CreateRenderTargetView(renderTarget_.Get(), &rtvDesc, &renderTargetView_);

	D3D11_SHADER_RESOURCE_VIEW_DESC srv;
	srv.Format = texDesc.Format;
	srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srv.Texture2D.MostDetailedMip = 0;
	srv.Texture2D.MipLevels = 1;
	hr = d3dDevice->CreateShaderResourceView(renderTarget_.Get(), &srv, &shaderResourceView_);


}

void FilterShader::SetBuffers(ID3D11DeviceContext* d3dContext) {

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	d3dContext->IASetVertexBuffers(0, 1, vertexBuffer_.GetAddressOf(), &stride, &offset);

	stride = sizeof(int);
	offset = 0;
	d3dContext->IASetIndexBuffer(indexBuffer_.Get(), DXGI_FORMAT_R32_UINT, 0);

	if (constantBuffer_) {

		d3dContext->PSSetConstantBuffers(0, 1, constantBuffer_.GetAddressOf());
		d3dContext->VSSetConstantBuffers(0, 1, constantBuffer_.GetAddressOf());
	}
}

void FilterShader::SetRenderTargetView(ID3D11DeviceContext* d3dContext, ID3D11DepthStencilView* depthStencil) {

	d3dContext->OMSetRenderTargets(1, renderTargetView_.GetAddressOf(), depthStencil);
}

void FilterShader::ClearRenderTarget(ID3D11DeviceContext* d3dContext, XMVECTORF32 clearColor) {

	d3dContext->ClearRenderTargetView(renderTargetView_.Get(), clearColor);
}
