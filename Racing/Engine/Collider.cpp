#include "Collider.h"

#include "GameObject.h"
#include "ColliderSphere.h"
#include "ColliderBox.h"
#include "ColliderOrientedBox.h"


Collider::Collider()
	: pObject_(nullptr), center_(0, 0, 0), hDebugModel_(-1) {
}

void Collider::SetGameObject(GameObject* pObject) {

	pObject_ = pObject;
	TranslationPosition();
};

XMVECTOR Collider::GetWorldPosition() const {

	return XMVector3TransformCoord(XMLoadFloat3(&center_), pObject_->GetWorldMatrix());
}

bool Collider::IsHitSpheres(ColliderSphere* sphereA, ColliderSphere* sphereB) {

	return sphereA->bSphere_.Intersects(sphereB->bSphere_);
}

bool Collider::IsHitSphereVsBox(ColliderSphere* sphere, ColliderBox* box) {

	return sphere->bSphere_.Intersects(box->bBox_);
}

bool Collider::IsHitSphereVsOrientedBox(ColliderSphere* sphere, ColliderOrientedBox* orientedBox) {

	return sphere->bSphere_.Intersects(orientedBox->bOrientedBox_);
}

bool Collider::IsHitBoxes(ColliderBox* boxA, ColliderBox* boxB) {

	return boxA->bBox_.Intersects(boxB->bBox_);
}

bool Collider::IsHitBoxVsOrientedBox(ColliderBox* box, ColliderOrientedBox* orientedBox) {

	return box->bBox_.Intersects(orientedBox->bOrientedBox_);
}

bool Collider::IsHitOrientedBoxes(ColliderOrientedBox* orientedBoxA, ColliderOrientedBox* orientedBoxB) {

	return orientedBoxA->bOrientedBox_.Intersects(orientedBoxB->bOrientedBox_);
}
