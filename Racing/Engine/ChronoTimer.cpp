#include "ChronoTimer.h"

float ChronoTimer::GetTimeSec()
{
	return static_cast<float>(duration_cast<nanoseconds>(steady_clock::now().time_since_epoch()).count()) / 1000000000;
}

float ChronoTimer::GetElapsed(float start, float end)
{
	return end - start;
}
