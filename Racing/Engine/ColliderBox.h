#pragma once
#include "Collider.h"

class ColliderBox : public Collider {

	friend class Collider;	//親クラスにprivateメンバを公開

private:
	BoundingBox bBox_;	//境界ボックス

	/**
	 * \brief コピーコンストラクタ
	 * \param bb コピーしたい境界ボックス
	 */
	ColliderBox(BoundingBox& bb);

public:
	/**
	 * \brief コンストラクタ
	 * \param center 中心座標
	 * \param extents 広がり(中心座標から広げる長さ)
	 */
	ColliderBox(FXMVECTOR center, FXMVECTOR extents);
	
	/**
	 * \brief 接触判定
	 * \param pTarget 相手の当たり判定
	 * \return 接触しているなら真
	 */
	bool IsHit(Collider* pTarget) override;

	/**
	 * \brief デバッグ用モデル描画
	 */
	void Draw() override;

	/**
	 * \brief 境界範囲の座標を更新
	 */
	void TranslationPosition() override;
	
	/**
	 * \brief インスタンスをコピーする
	 * \return コピーしたインスタンス
	 */
	virtual Collider* Copy() override;
};

