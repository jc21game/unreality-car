#include "StageSelectScene.h"

#include "../Engine/Image.h"
#include "../Engine/Model.h"
#include "../Engine/Input.h"
#include "../Engine/SceneManager.h"
#include "../Stage/StageDataManager.h"
#include "../Stage/StageData.h"
#include "PreviewStage.h"
#include "StageSelecter.h"


StageSelectScene::StageSelectScene(GameObject* parent)
	: GameObject(parent, "StageSelectScene")
	, hPictBackGround_(-1) {
}

void StageSelectScene::Initialize() {

	//�w�i�p�摜�ǂݍ���
	hPictBackGround_ = Image::Load("SampleResource/BGI04.png");
	Image::SetTransformFullSize(hPictBackGround_);
	//Instantiate<PreviewStage>(this);
	Instantiate<StageSelecter>(this);
}

void StageSelectScene::Update() {

	
}

void StageSelectScene::Draw() {

	Image::Draw(hPictBackGround_);
}

void StageSelectScene::Release() {
}
