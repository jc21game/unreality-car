#include "PreviewStage.h"

#include "../Engine/Direct3D.h"
#include "../Engine/StaticModel.h"
#include "../Engine/Camera.h"
#include "../Engine/SceneTimer.h"
#include "../Stage/StageDataManager.h"
#include "../Stage/StageData.h"


PreviewStage::PreviewStage(GameObject* parent)
	: GameObject(parent) {
}

void PreviewStage::Initialize() {

	ResetDrawViewPortList();
	AddDrawViewPort(Direct3D::VIEW_PORT_TOP);

	Camera::SetPosition(XMVectorSet(0, 10, -10, 0), Direct3D::VIEW_PORT_TOP);
	Camera::SetTarget(XMVectorSet(0, 0, 10, 0), Direct3D::VIEW_PORT_TOP);

	transform_.scale_ *= 0.05f;
}

void PreviewStage::Update() {

	/*transform_.rotate_.vecY += 1.f;

	auto move = XMVectorSet(0, 0, -10, 0);
	auto orientedY = XMQuaternionRotationAxis(XMVectorSet(0, 1, 0, 0), XMConvertToRadians(transform_.rotate_.vecY));
	move = XMVector3Rotate(move, orientedY);

	transform_.position_ += move;*/

	/*auto camPos = Camera::GetPosition(Direct3D::VIEW_PORT_TOP);
	auto orientedY = XMQuaternionRotationAxis(XMVectorSet(0, 1, 0, 0), SceneTimer::GetDelta());
	camPos = XMVector3Rotate(camPos, orientedY) + XMVectorSet(0, 0, 10, 0);
	Camera::SetPosition(camPos, Direct3D::VIEW_PORT_TOP);*/

}

void PreviewStage::Draw() {

	auto* pSDM = StageDataManager::GetInstance();
	auto* pStageData = pSDM->GetCurrentStageData();
	auto hModel = pStageData->GetWholeAreaModelHandle();
	StaticModel::SetTransform(hModel, transform_);
	StaticModel::Draw(hModel);
}

void PreviewStage::Release() {
}
