#pragma once
#include "../Engine/GameObject.h"

class PreviewStage : public GameObject {

public:
	explicit PreviewStage(GameObject* parent);
	~PreviewStage() = default;

	virtual void Initialize() override;
	virtual void Update(void) override;
	virtual void Draw() override;
	virtual void Release() override;
};

