#include "StageSelecter.h"

#include "../Engine/Input.h"
#include "../Engine/SceneManager.h"
#include "../Engine/SceneTimer.h"
#include "../Engine/Image.h"
#include "../Engine/StaticModel.h"
#include "../Stage/StageDataManager.h"
#include "../Stage/StageData.h"


StageSelecter::StageSelecter(GameObject* parent)
	: GameObject(parent)
	, hReturn_(-1) {

	hIcons_.fill(-1);
}

void StageSelecter::Initialize() {

	hReturn_ = Image::Load("SampleResource\\Return.png");
	auto trans = transform_;
	trans.scale_ = XMVectorReplicate(0.3f);
	trans.position_ = XMVectorSet(-0.85f, 0.8f, 0, 0);
	Image::SetTransform(hReturn_, trans);
	
	hIcons_[0] = Image::Load("MapData/SS01.png");
	hIcons_[1] = Image::Load("MapData/SS02.png");
	
	transform_.position_.vecX = -0.5f;
}

void StageSelecter::Update() {

	using namespace Input;

	auto* pSDM = StageDataManager::GetInstance();

	if (IsKeyDown(DIK_LEFT)) {

		pSDM->GoBack();
	}
	if (IsKeyDown(DIK_RIGHT)) {

		pSDM->GoForword();
	}
	if (IsKeyDown(DIK_RETURN)) {
		if (auto* pSceneManager = SceneManager::GetInstance()) {

			pSceneManager->ChangeScene(SCENE_ID_PLAY);
		}
	}
	if (IsKeyDown(DIK_ESCAPE)) {
		if (auto* pSceneManager = SceneManager::GetInstance()) {

			pSceneManager->ChangeScene(SCENE_ID_START);
		}
	}
}

void StageSelecter::Draw() {

	//todo ここで選択できるパーツを描画
	auto* pSDM = StageDataManager::GetInstance();

	Image::Draw(hReturn_);
	
	//変形情報をコピー
	Transform trans = transform_;

	//すべてのパーツを描画
	for (auto i = 0; i < pSDM->GetSize(); ++i) {

		//選択されているなら大きく表示
		auto* pStageData = pSDM->GetStageData(i);
		auto hModel = pStageData->GetWholeAreaModelHandle();
		if (pStageData == pSDM->GetCurrentStageData()) {

			Transform bigTrans = trans;
			bigTrans.scale_ *= 1.f + sin(SceneTimer::GetElapsedSecounds() * 5) * 0.07f;
			//StaticModel::SetTransform(hModel, bigTrans);
			Image::SetTransform(hIcons_[i], bigTrans);
		}
		else {

			/*StaticModel::SetTransform(hModel, trans);*/
			Image::SetTransform(hIcons_[i], trans);
		}
		Image::Draw(hIcons_[i]);
		//右へずらす
		trans.position_.vecX += 1.f;
		//trans.scale_ *= 0.5f;
	}
}

void StageSelecter::Release() {
}
