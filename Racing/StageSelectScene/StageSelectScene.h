#pragma once
#include "../Engine/GameObject.h"


class StageSelectScene : public GameObject {

private:
	int hPictBackGround_;

public:
	StageSelectScene(GameObject* parent);
	~StageSelectScene() = default;

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
