#pragma once
#include "../Engine/GameObject.h"

#include <array>


class StageSelecter : public GameObject {

private:
	int hReturn_;
	std::array<int , 2> hIcons_;	
	
public:
	explicit StageSelecter(GameObject* parent);
	~StageSelecter() = default;

	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

