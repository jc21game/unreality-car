#include "CountDown.h"
#include "../Engine/Image.h"
#include "DataMediator.h"

//コンストラクタ
CountDown::CountDown(GameObject * parent)
	: GameObject(parent, "CountDown") {

}

//初期化
void CountDown::Initialize() {

	auto&& dataMediator = DataMediator::GetInstance();
	countDownNumber = dataMediator->GetWaitTime();
	CountDownInfo countDownInfo;
	
	for (int in = countDownNumber; in >= 0 ; --in) {

		std::string str =std::to_string(in);
		std::string filePath = "../Assets/Number/" + str + ".png";
		countDownInfo.hImage = Image::Load(filePath);
		
		hImage_.push_back(countDownInfo);

		assert(hImage_[countDownNumber - in].hImage >= 0);
	}

	countDownInfo.hImage = Image::Load("../Assets/Number/Go.Png");
	hImage_.push_back(countDownInfo);
}

//更新
void CountDown::Update() {


}

//描画
void CountDown::Draw() {

	auto&& dataMediator = DataMediator::GetInstance();
	if(dataMediator->RaceTimeUpdate() < countDownNumber){

		//hImage_[dataMediator->RaceTimeUpdate()].imageTransform.scale_ += XMVectorSet(0.1f,0.1f,0.1f,1.f);
		hImage_[dataMediator->RaceTimeUpdate()].imageTransform.scale_ += XMVectorSet(0.05f,0.05f,0.05f,1.f);
		//hImage_[dataMediator->RaceTimeUpdate()].imageTransform.rotate_.vecZ+=7;
		
		Image::SetTransform(hImage_[dataMediator->RaceTimeUpdate()].hImage, hImage_[dataMediator->RaceTimeUpdate()].imageTransform);
		Image::Draw(hImage_[dataMediator->RaceTimeUpdate()].hImage);
	}
	else{
		
		if (dataMediator->RaceTimeUpdate()< countDownNumber + 1){

			hImage_[dataMediator->RaceTimeUpdate()].imageTransform.scale_ += XMVectorSet(0.05f, 0.05f, 0.05f, 1.f);
			Image::SetTransform(hImage_[countDownNumber + 1].hImage, hImage_[dataMediator->RaceTimeUpdate()].imageTransform);
			Image::Draw(hImage_[countDownNumber + 1].hImage);
		}
	}
}

//開放
void CountDown::Release() {

}
