#pragma once
#include <DirectXMath.h>
#include <string>
#include <vector>
#include <list>
#include "../Engine/GameObject.h"

class CheckPoint;

//地面を管理するクラス
class Stage : public GameObject {

private:

	int hModel_;					//モデル番号
	int hModelDirt_;					//モデル番号
	int hModelWall_;
	int hModelAll_;

	
	int checkPointNumber;			//チェックポイントの数（カウント方法は未定）
	
public:
	Transform wallTransform;
	int outsideModel;
	
	//コンストラクタ
	Stage(GameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	int GetModelHandle () const;
	int GetDirtModelHandle() const;
	int GetWallModelHandle() const;
	
	//次のチェックポイントまでの距離を返す
	//関数の置き場所変えるかも
	float GetDistanceNextCheck(XMVECTOR& position, int checkNumber);

	CheckPoint* InstantiateCheckPoint(GameObject* pParent, std::string name, FXMVECTOR& rotate);
};