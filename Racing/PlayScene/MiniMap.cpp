#include "MiniMap.h"

#include "../Engine/StaticImage.h"
#include "../Stage/StageDataManager.h"

MiniMap::MiniMap(GameObject* parent)
	: GameObject(parent, "MiniMap")
	, hImage_(-1) {
}

void MiniMap::Initialize() {

	auto* stageDataManager = StageDataManager::GetInstance();
	auto* stageData = stageDataManager->GetCurrentStageData();
	hImage_ = stageData->GetMiniMapHandle();
}

void MiniMap::Update() {
}

void MiniMap::Draw() {

}

void MiniMap::PostDraw() {

	//StaticImage::Draw(hImage_);
}

void MiniMap::Release() {
}
