#include "AccelerationPanel.h"
#include "../Engine/ColliderOrientedBox.h"
#include "../Engine/ColliderSphere.h"
#include "../Engine/Direct3D.h"

//コンストラクタ
AccelerationPanel::AccelerationPanel(GameObject * parent)
	: GameObject(parent, "AccelerationPanel") {

}

//初期化
void AccelerationPanel::Initialize() {

	//transform_.position_.vecX = 50.f;
	
	//ColliderSphere* col = new ColliderSphere(XMVectorSet(10, 10, 10, 0), 10.f);
	//当たり判定付与
	ColliderOrientedBox* col = new ColliderOrientedBox(XMVectorSet(0, 2, 0, 0), XMVectorSet(10, 2, 10, 0),XMVectorSet(0, 0, 0, 0));
	AddCollider(col);

	AddDrawViewPort(Direct3D::VIEW_PORT_LEFT);
	AddDrawViewPort(Direct3D::VIEW_PORT_RIGHT);
}

//更新
void AccelerationPanel::Update() {

}

//描画
void AccelerationPanel::Draw() {

}

//開放
void AccelerationPanel::Release() {

}
