#pragma once
#include "../Engine/GameObject.h"

struct PlayerKeyCode;
struct InputDevice;
struct CameraMotion;

namespace Direct3D {

	enum VIEW_PORT_TYPE;
}

namespace PlayerCamera{

	void SetIotaMotion(float moveVelocity, CameraMotion& cameraMotion, PlayerKeyCode& keyCode, int padNum);
	void PlayerCameraSetPosition(GameObject* targetObj,float moveVelocity, CameraMotion& cameraMotion, PlayerKeyCode& keyCode , XMMATRIX& rotateMatrix, Direct3D::VIEW_PORT_TYPE& type);
	void PlayerCameraSetPosition(GameObject* targetObj, CameraMotion& cameraMotion, Direct3D::VIEW_PORT_TYPE& type);
	void PlayerCameraSetTarget(GameObject* targetObj ,  XMVECTOR& targetPosition , Direct3D::VIEW_PORT_TYPE& type);
}