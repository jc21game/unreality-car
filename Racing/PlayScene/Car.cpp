#include "Car.h"
#include "DataMediator.h"
#include "Stage.h"
#include "GamepadKeyCode.h"
#include "PlayerAuxiliary.h"
#include "PlayerCamera.h"

#include "../CustomCar/CustomCar.h"
#include "../Engine/StaticModel.h"
#include "../Engine/Input.h"
#include "../Engine/Camera.h"
#include "../Engine/SceneTimer.h"

#include <iostream>

//コンストラクタ
Car::Car(GameObject* parent, std::string& name)
	: GameObject(parent, name)
	, isDuringFall_(false)
	, initialVelocity_(0.f)
	, gear_(DRIVE)
	, groundState_(NORMAL)
	, acceleration_(0.f)
	, velocity_(0.f)
	, time_(0.f)
	, timeStartedAccelerator_(0.f)
	, timeReleasedAccelerator_(0.f)
{
}

//初期化
void Car::Initialize() {

	//初期位置の調整用
	transform_.rotate_.vecY = -90;

	//Player名によって使えるキー、メインで映るカメラを変更
	SettingEachPlayer(objectName_);

	//ステージのモデル番号を取得
	auto&& dataMediator = DataMediator::GetInstance();
	hStageModel_ = dataMediator->GetCurrentStage()->GetModelHandle();
	hDirtModel_ = dataMediator->GetCurrentStage()->GetDirtModelHandle();
	hWallModel_ = dataMediator->GetCurrentStage()->GetWallModelHandle();;
	
	dataMediator->SetCarPerformance(objectName_, mass_);
}

//更新
void Car::Update() {
	
	//加速パネルの処理
	AccelerationPanelProcess();

	//地面の判定
	GroundDecision();

	if (isDuringFall_) {

		LandingProcess();
	}
	else {

		CarBodyRotation();
		AcceleratorAndBrake();
	}

	//速度を m/s に直して、FPSで割る
	moveVelocity_ = velocity_ * 1000 / 60 / 60 / 10;
	GearChangeApplication();
	
	//回転行列
	rotateMatrix_ = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	moveVec_ = XMVector3TransformCoord(XMVectorSet(0.f, 0.f, moveVelocity_, 0.f), rotateMatrix_);

	HitWithWall();

	auto&& dataMediator = DataMediator::GetInstance();
	if (dataMediator->GetCarInfo(objectName_).isCarRaceDuring)
	{
		ModelMotion();
	}

	CameraMotion();
	UpdateEnd();

	if (dataMediator->GetCarInfo(objectName_).isGoal)
	{
		velocity_ -= 2;
		//速度はゼロ以下になった場合 0 にする
		velocity_ = max(velocity_, 0.f);
	}
}

//描画
void Car::Draw() {

	//モデルのみの回転処理
	//現在のTransformを受け取る
	auto modelTransform = transform_;
	modelTransform.rotate_.vecY += modelRotateY_;
	
	if (objectName_ == "Player1") {
		
		CustomCar::GetFirstCustom()->Draw(modelTransform);
	}
	else {
		
		CustomCar::GetSecondCustom()->Draw(modelTransform);
	}
}

//開放
void Car::Release() {

}

//何かに当たった
void Car::OnCollision(GameObject * pTarget) {

	const auto str = pTarget->GetObjectName();

	//当たったときの処理
	if (str.substr(0, 10) == "CheckPoint") {

		auto&& dataMediator = DataMediator::GetInstance();
		dataMediator->PassingCheckPoint(objectName_, str);
	}

	if (pTarget->GetObjectName() == "AccelerationPanel") {

		isWithAccelerationPanel_ = true;
	}

	if (pTarget->GetObjectName() == "Player1" || pTarget->GetObjectName() == "Player2") {

		LeftRightCarToEachOtherHit(pTarget);
	}
}

void Car::OnCollisionEnter(GameObject * pTarget) {

	if (pTarget->GetObjectName() == "Player1" || pTarget->GetObjectName() == "Player2") {

		FrontRearCarToEachOtherHit(pTarget);
	}
}

void Car::SettingEachPlayer(const std::string & playerName) {

	padNum = std::stoi(objectName_.substr(6)) - 1;

	keyCode_.frontKey.gamepad = PAD_L_SHOULDER;
	keyCode_.backKey.gamepad = PAD_R_SHOULDER;
	keyCode_.curveKey.gamepad = PAD_L_THUMB;
	//keyCode_.backCameraKey.gamepad = PAD_R_THUMB;
	keyCode_.driveKey.gamepad = PAD_A;
	keyCode_.reverseKey.gamepad = PAD_B;

	if (playerName == "Player1") {
		
		//当たり判定の追加
		auto&& custom = CustomCar::GetFirstCustom();
		custom->AddColliderToObject(this);
		
		DrawViewportPreference(Direct3D::VIEW_PORT_LEFT);

		keyCode_.frontKey.keyboard = DIK_W;
		keyCode_.backKey.keyboard = DIK_S;
		keyCode_.leftKey.keyboard = DIK_A;
		keyCode_.rightKey.keyboard = DIK_D;
		keyCode_.curveKey.keyboard = DIK_SPACE;
		keyCode_.backCameraKey.keyboard = DIK_LSHIFT;
		keyCode_.driveKey.keyboard = DIK_F;
		keyCode_.reverseKey.keyboard = DIK_R;

		acceleration_ = custom->GetStatus().acceleration;
		mass_ = custom->GetStatus().weight;
	}

	if (playerName == "Player2") {

		//当たり判定の追加
		auto&& custom = CustomCar::GetSecondCustom();
		custom ->AddColliderToObject(this);
		
		DrawViewportPreference(Direct3D::VIEW_PORT_RIGHT);

		keyCode_.frontKey.keyboard = DIK_UP;
		keyCode_.backKey.keyboard = DIK_DOWN;
		keyCode_.leftKey.keyboard = DIK_LEFT;
		keyCode_.rightKey.keyboard = DIK_RIGHT;
		keyCode_.curveKey.keyboard = DIK_RCONTROL;
		keyCode_.backCameraKey.keyboard = DIK_RSHIFT;

		acceleration_ = custom->GetStatus().acceleration;
		mass_ = custom->GetStatus().weight;
	}

	std::cout << objectName_ << "：" << acceleration_ << std::endl;
	std::cout << objectName_ << "：" << mass_ << std::endl;
	defaultAcceleration_ = acceleration_;
}

void Car::DrawViewportPreference(const Direct3D::VIEW_PORT_TYPE& type) {

	viewPortType_ = type;

	//自分を映すカメラをリセットする
	ResetDrawViewPortList();

	//左右のカメラに表示
	AddDrawViewPort(Direct3D::VIEW_PORT_RIGHT);
	AddDrawViewPort(Direct3D::VIEW_PORT_LEFT);
}

void Car::GroundDecision() {

	//落下判定用
	RayCastData ray;
	ray.start = transform_.position_;
	ray.dir = XMVectorSet(0, -1, 0, 0);
	StaticModel::RayCast(hStageModel_, &ray);
	
	if (!ray.hit) {

		RayCastData dirtRay;
		dirtRay.start = transform_.position_;
		dirtRay.dir = XMVectorSet(0, -1, 0, 0);
		StaticModel::RayCast(hDirtModel_, &dirtRay);

		if (dirtRay.hit){

			isDirt = true;
			velocity_ -= 3.f;
			
			//速度はゼロ以下になった場合 0 にする
			velocity_ = max(velocity_, 0.f);
			
			if(acceleration_ >= (defaultAcceleration_  / 2))
			{
				acceleration_--;
			}
		}
		else {
			
			isDuringFall_ = true;
		}
	}
}

void Car::AccelerationPanelProcess() {

	//if (isWithAccelerationPanel_ && defaultAcceleration_ * 2 > acceleration_)
	//	acceleration_ += 1.5f;
	//else
	//	acceleration_ -= 0.3f;

	//if (acceleration_ < defaultAcceleration_)
	//	acceleration_ = defaultAcceleration_;
}

void Car::LandingProcess() {

	auto&& dataMediator = DataMediator::GetInstance();
	transform_.position_.vecY--;

	if (transform_.position_.vecY < -60) {

		cameraMotion_.cameraStopFlg = true;

		if (transform_.position_.vecY < -100) {

			//最後に通ったチェックポイントの地点にリスポーン
			const auto cp = "CheckPoint" + std::to_string(dataMediator->GetCarInfo(objectName_).currentCheckPointNumber);

			XMVECTOR offTrackPos = dataMediator->GetCheckPointPos(cp);
			if(objectName_ == "Player1")
				offTrackPos.vecZ += 5;
			
			offTrackPos.vecY = 30.f;

			transform_.position_ = offTrackPos;

			auto&& dataMediator = DataMediator::GetInstance();
			std::string nextCheckPoint = "CheckPoint" + std::to_string(dataMediator->GetCarInfo(objectName_).currentCheckPointNumber + 1);
			
			XMVECTOR cpVec = XMVector3Normalize(dataMediator->GetCheckPointPos(cp));
			XMVECTOR nextcp = XMVector3Normalize(dataMediator->GetCheckPointPos(nextCheckPoint));

			float angleAtReturn = XMConvertToDegrees(acos(XMVector3Dot(cpVec, nextcp).vecX));
			
			transform_.rotate_.vecY -= angleAtReturn;

			if(cp == "CheckPoint0")
				transform_.rotate_.vecY = -90;
			
			if(cp == "CheckPoint5")
				transform_.rotate_.vecY = 110;
			
			cameraMotion_.cameraStopFlg = false;
			isDuringFall_ = false;

			fallEndTime_ = SceneTimer::GetElapsedSecounds();
		}
	}
}

void Car::CarBodyRotation() {

	//タイヤ（車体）の回転だけ（仮）
	transform_.rotate_.vecY -= static_cast<float>(0.5f + 1.f * Input::IsKey(keyCode_.curveKey.keyboard)) * Input::IsKey(keyCode_.leftKey.keyboard);
	transform_.rotate_.vecY += static_cast<float>(0.5f + 1.f * Input::IsKey(keyCode_.curveKey.keyboard)) * Input::IsKey(keyCode_.rightKey.keyboard);
	transform_.rotate_.vecY += Input::GetPadStickL(padNum).vecX;
}

void Car::AcceleratorAndBrake() {

	auto&& dataMediator = DataMediator::GetInstance();
	if (!dataMediator->GetCarInfo(objectName_).isGoal)
	{
		//アクセル処理
		if (PlayerAuxiliary::ButtonDownCheck(keyCode_.frontKey.keyboard, keyCode_.frontKey.gamepad , padNum) && !isDuringFall_)
			//キーを押した瞬間の時間
			timeStartedAccelerator_ = SceneTimer::GetElapsedSecounds();

		//アクセルを踏み続けている
		if (PlayerAuxiliary::ButtonCheck(keyCode_.frontKey.keyboard, keyCode_.frontKey.gamepad, padNum) && !isDuringFall_ && !isForwardAndHit_) {

			if (acceleration_ <= defaultAcceleration_ && !isDirt)
			{
				acceleration_+=0.05f;
			}
			
			//キーを押している時間
			time_ = (SceneTimer::GetElapsedSecounds() - timeStartedAccelerator_);
			
			//速度の計算
			velocity_ = initialVelocity_ + acceleration_ * time_;
			velocity_ -= (velocity_ - velocity_ * FRICTION_COEFFICIENT);
			
			//最大速度の判定
			velocity_ = min(velocity_, 120.f);
		}
		else {

			//エンジンブレーキ 
			velocity_ -= velocity_ / mass_ * FRICTION_COEFFICIENT;

			//ブレーキを踏むことで生まれる抵抗力（仮）
			brakeResistance_ = 2.f * PlayerAuxiliary::ButtonCheck(keyCode_.backKey.keyboard, keyCode_.backKey.gamepad, padNum);
			velocity_ -= brakeResistance_;

			//速度はゼロ以下になった場合 0 にする
			velocity_ = max(velocity_, 0.f);

			//踏んでない間に減速した速度を、次アクセル踏んだ時の初速度にする
			initialVelocity_ = velocity_;
		}
	}
}

void Car::HitWithWall() {
	
	XMVECTOR frontVec = XMVector3TransformCoord(XMVectorSet(0.f, 0.f, 1.f, 0.f), rotateMatrix_);
	RayCastData rayToWall;
	rayToWall.start = transform_.position_;
	rayToWall.dir = frontVec;
	StaticModel::RayCast(hWallModel_, &rayToWall);

	auto&& dataMediator = DataMediator::GetInstance();
	int outside = dataMediator->GetCurrentStage()->outsideModel;
	
	RayCastData rayToOutsideWall;
	rayToOutsideWall.start = transform_.position_;
	rayToOutsideWall.dir = frontVec;
	StaticModel::RayCast(outside, &rayToOutsideWall);
	
	if (rayToWall.hit && rayToWall.dist < 20.f)
	{
		XMVECTOR nor =  XMVector3Normalize(rayToWall.normal);
		float dot = XMVector3Dot(-moveVec_, nor).vecX;

		//反射ベクトル
		moveVec_ = moveVec_ + (2 *dot * nor);
	}


	if (rayToOutsideWall.hit && rayToOutsideWall.dist < 20.f)
	{
		XMVECTOR nor = XMVector3Normalize(rayToOutsideWall.normal);
		float dot = XMVector3Dot(-moveVec_, nor).vecX;

		//反射ベクトル
		moveVec_ = moveVec_ + (2 * dot * nor);
	}
}

void Car::GearChangeApplication() {

	if (moveVelocity_ == 0) {

		if (PlayerAuxiliary::ButtonDownCheck(keyCode_.driveKey.keyboard, keyCode_.driveKey.gamepad, padNum))
			ChangeGear(DRIVE);

		if (PlayerAuxiliary::ButtonDownCheck(keyCode_.reverseKey.keyboard, keyCode_.reverseKey.gamepad, padNum))
			ChangeGear(REVERSE);
	}

	if (gear_ == REVERSE)
		moveVelocity_ *= -1.f;

	if (isReverseForwardAndHit_)
		moveVelocity_ *= -1.f;

	if (isForwardAndHit_) {

		moveVelocity_ *= -1.f;

		if (SceneTimer::GetElapsedSecounds() - forwardAndHitTime_ > 0.3f) {

			timeStartedAccelerator_ = SceneTimer::GetElapsedSecounds();
			initialVelocity_ = velocity_;
			isForwardAndHit_ = false;
		}
	}
}

void Car::CameraMotion() {

	if (moveVelocity_ < 0)
		moveVelocity_ *= -1.f;
	
	PlayerCamera::SetIotaMotion(moveVelocity_, cameraMotion_, keyCode_ , padNum);
	PlayerCamera::PlayerCameraSetPosition(this, moveVelocity_, cameraMotion_, keyCode_, rotateMatrix_, viewPortType_);
	PlayerCamera::PlayerCameraSetTarget(this, cameraMotion_.cameraTarget, viewPortType_);

	bool backCameraFlg = Input::IsKey(keyCode_.backCameraKey.keyboard);

	if (Input::GetPadTrrigerL(padNum) != 0)
		backCameraFlg = true;

	if (backCameraFlg && !cameraMotion_.cameraStopFlg || gear_ == REVERSE) {

		PlayerCamera::PlayerCameraSetPosition(this, cameraMotion_, viewPortType_);

		auto backCameraTarget = cameraMotion_.cameraTarget;
		backCameraTarget.vecX = Input::GetPadStickR(padNum).vecY * -2.f;
		backCameraTarget.vecZ = -Input::GetPadStickR(padNum).vecX * -2.f;

		PlayerCamera::PlayerCameraSetTarget(this, backCameraTarget, viewPortType_);
	}
}

void Car::FrontRearCarToEachOtherHit(GameObject* pTarget) {

	//左右の判定
	//正面ベクトル
	XMVECTOR frontVec = XMVector3TransformCoord(XMVectorSet(0.f, 0.f, 1.f, 0.f), rotateMatrix_);
	//自分から相手の車へのベクトル
	XMVECTOR carToEachOtherVector = transform_.position_ - pTarget->GetPosition();

	//ベクトルの正規化
	XMVECTOR frontNormal = XMVector3Normalize(frontVec);
	XMVECTOR carNor = XMVector3Normalize(carToEachOtherVector);
	//正面ベクトルと相手へのベクトルの角度を算出
	float angleWithCar = XMConvertToDegrees(acos(XMVector3Dot(frontNormal, carNor).vecX));

	//前後ろの判定
	//車同士のベクトルを90度回転させる
	XMMATRIX verticalMat = XMMatrixRotationY(XMConvertToRadians(90));
	XMVECTOR frontRearDecisionVector = XMVector3TransformCoord(carToEachOtherVector, verticalMat);

	//外積を求める
	XMVECTOR cross = XMVector3Cross(frontRearDecisionVector, frontVec);

	//前
	if (cross.vecY < 0) {

		auto&& dataMediator = DataMediator::GetInstance();
		auto&& pTargetPerformance = dataMediator->GetCarPerformance(pTarget->GetObjectName());
		auto&& pTargetInfo = dataMediator->GetCarInfo(pTarget->GetObjectName());

		//std::cout << objectName_ << "：" << angleWithCar << std::endl;

		//後ろからの衝突判定で、後方の衝突範囲内ならば
		if (angleWithCar <= 30 && angleWithCar >= 0)
		{
			float massRate = mass_ / (mass_ + pTargetPerformance.mass);

			if (gear_ == REVERSE)
			{
				isReverseForwardAndHit_ = true;
			}

			//当たった車より自分の質量のほうが重かったら
			if (mass_ >= pTargetPerformance.mass)
			{
				velocity_ += (pTargetInfo.currentVelocity - velocity_) * massRate;
			}
			else
			{
				velocity_ += (pTargetInfo.currentVelocity - velocity_) * massRate;
			}
		}

		//std::cout << pTarget->GetObjectName() << "の前にいる" << std::endl;
	}

	//後ろ
	if (cross.vecY > 0) {

		auto&& dataMediator = DataMediator::GetInstance();
		auto&& pTargetPerformance = dataMediator->GetCarPerformance(pTarget->GetObjectName());
		auto&& pTargetInfo = dataMediator->GetCarInfo(pTarget->GetObjectName());

		if (angleWithCar <= 180 && angleWithCar >= 160) {

			float massRate = mass_ / (mass_ + pTargetPerformance.mass);

			if (mass_ >= pTargetPerformance.mass) {

				velocity_ *= massRate;
			}
			else {

				velocity_ *= massRate;
			}

			forwardAndHitTime_ = SceneTimer::GetElapsedSecounds();
			isForwardAndHit_ = true;
			initialVelocity_ = velocity_;
		}
	}
}

void Car::LeftRightCarToEachOtherHit(GameObject * pTarget)
{
	//正面ベクトル
	XMVECTOR frontVec = XMVector3TransformCoord(XMVectorSet(0.f, 0.f, 1.f, 0.f), rotateMatrix_);
	//自分から相手の車へのベクトル
	XMVECTOR carToEachOtherVector = transform_.position_ - pTarget->GetPosition();

	XMVECTOR cross = XMVector3Cross(carToEachOtherVector, frontVec);

	if (cross.vecY < 0) {

		XMMATRIX crashedCarMatrix = XMMatrixRotationY(XMConvertToRadians(pTarget->GetRotate().vecY));
		XMVECTOR normal = XMVector3TransformCoord(XMVectorSet(1.f, 0.f, 0.f, 0.f), crashedCarMatrix);

		float dot = XMVector3Dot(-moveVec_, normal).vecX;

		//敵車に平行なベクトル
		slidingOnWallVec_ = moveVec_ + (dot * normal);

		//敵車に反射したベクトル
		reflectingVec_ = moveVec_ + (2 * dot * normal);

		isSlidingOnWall_ = true;
		isReflecting_ = true;
	}

	if (cross.vecY > 0) {

		XMMATRIX crashedCarMatrix = XMMatrixRotationY(XMConvertToRadians(pTarget->GetRotate().vecY));
		XMVECTOR normal = XMVector3TransformCoord(XMVectorSet(-1.f, 0.f, 0.f, 0.f), crashedCarMatrix);

		float dot = XMVector3Dot(-moveVec_, normal).vecX;

		//敵壁に平行なベクトル
		slidingOnWallVec_ = moveVec_ + (dot * normal);

		//敵車に反射したベクトル
		reflectingVec_ = moveVec_ + (2 * dot * normal);

		isSlidingOnWall_ = true;
		isReflecting_ = true;
	}

	time_ = 0.f;
	initialVelocity_ = 0;
}

void Car::ModelMotion() {

	//落下したら次のチェックポイントのほうに向く
	if (transform_.position_.vecY > 0) {

		velocity_ = 0.f;
		moveVec_ = { 0.f,0.f,0.f,0.f };
		
		if (SceneTimer::GetElapsedSecounds() - fallEndTime_ > 1) {
			
			transform_.position_.vecY--;
			timeStartedAccelerator_ = SceneTimer::GetElapsedSecounds();
			fallEndTime_ = 0;
		}
	}

	//if (move.vecZ > 0)
	//{
	//	if ((Input::IsKey(keyCode.rightKey.keyboard) || Input::GetPadStickL(padNum).vecX > 0))
	//		move.vecX += (0.5f * Input::IsKey(keyCode.curveKey.keyboard)) + (0.5f *Input::IsPadButton(keyCode.curveKey.gamepad));

	//	if ((Input::IsKey(keyCode.leftKey.keyboard) || Input::GetPadStickL(padNum).vecX < 0))
	//		move.vecX -= (0.5f *Input::IsKey(keyCode.curveKey.keyboard)) + (0.5f *Input::IsPadButton(keyCode.curveKey.gamepad));
	//}

	//if (isSlidingOnWall)
	//	transform_.position_ += slidingOnWallVec;
	if (isReflecting_)
		transform_.position_ += reflectingVec_;
	else
		//移動処理
		transform_.position_ += moveVec_;

	//カーブする際に、車体のモデルを傾ける
	if ((Input::IsKey(keyCode_.rightKey.keyboard) || Input::GetPadStickL(padNum).vecX > 0) && modelRotateY_ <= 20) {

		modelRotateY_ += (0.2f * Input::IsKey(keyCode_.curveKey.keyboard)) + (0.2f * Input::IsPadButton(keyCode_.curveKey.gamepad , padNum)) + 0.3f;
	}

	if ((Input::IsKey(keyCode_.leftKey.keyboard) || Input::GetPadStickL(padNum).vecX < 0) && modelRotateY_ >= -20) {

		modelRotateY_ -= (0.2f * Input::IsKey(keyCode_.curveKey.keyboard)) + (0.2f * Input::IsPadButton(keyCode_.curveKey.gamepad, padNum)) + 0.3f;
	}

	if (modelRotateY_ > 0)	modelRotateY_ -= 0.2f;
	if (modelRotateY_ < 0)	modelRotateY_ += 0.2f;
}

void Car::UpdateEnd(){

	auto&& dataMediator = DataMediator::GetInstance();
	isDirt = false;
	isWithAccelerationPanel_ = false;
	isSlidingOnWall_ = false;
	isReflecting_ = false;
	isReverseForwardAndHit_ = false;

	dataMediator->SetCarInfo(objectName_, transform_.position_, moveVec_, velocity_);
}

void Car::ChangeGear(const Gear& change) {

	gear_ = change;
}

void Car::ChangeGroundState(const GroundState & change) {

	groundState_ = change;
}