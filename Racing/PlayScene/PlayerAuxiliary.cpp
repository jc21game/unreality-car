#include "PlayerAuxiliary.h"
#include "../Engine/Input.h"

bool PlayerAuxiliary::ButtonCheck(int keybord, int gamepad){

	return Input::IsKey(keybord) || Input::IsPadButton(gamepad);
}

bool PlayerAuxiliary::ButtonDownCheck(int keybord, int gamepad){

	return Input::IsKeyDown(keybord) || Input::IsPadButtonDown(gamepad);
}

bool PlayerAuxiliary::ButtonCheck(int keybord, int gamepad, int playerNumber){

	return Input::IsKey(keybord) || Input::IsPadButton(gamepad, playerNumber);
}

bool PlayerAuxiliary::ButtonDownCheck(int keybord, int gamepad, int playerNumber){

	return Input::IsKeyDown(keybord) || Input::IsPadButtonDown(gamepad, playerNumber);
}

bool PlayerAuxiliary::IsEnteredCheck(float value){

	if (value != 0)
		return true;
	
	return false;
}

float PlayerAuxiliary::lerp(float start, float goal, float lerpTime){

	return start + lerpTime * (goal - start);
}
