#pragma once
#include "../Engine/GameObject.h"

class MiniMap : public GameObject {

private:
	int hImage_;

public:
	explicit MiniMap(GameObject* parent);
	~MiniMap() = default;

	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void PostDraw() override;
	virtual void Release() override;
};

