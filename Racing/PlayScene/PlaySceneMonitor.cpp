#include "PlaySceneMonitor.h"
#include "PlayerMonitor.h"
#include "../Engine/Image.h"
#include "../Engine/Direct3D.h"
#include "DataMediator.h"

//コンストラクタ
PlaySceneMonitor::PlaySceneMonitor(GameObject * parent)
	: GameObject(parent, "GameMonitor"), hImage_(-1) {

}

//初期化
void PlaySceneMonitor::Initialize() {

	//1P表示
	PlayerMonitor* pMonitor1 = Instantiate<PlayerMonitor>(this);
	pMonitor1->ResetDrawViewPortList();
	pMonitor1->AddDrawViewPort(Direct3D::VIEW_PORT_LEFT);
	auto&& dataMeditor = DataMediator::GetInstance();
	std::string playerName1 = dataMeditor->GetPlayerName(0);
	pMonitor1->SetPlayerName(playerName1);
	
	//2P表示
	PlayerMonitor* pMonitor2 = Instantiate<PlayerMonitor>(this);
	pMonitor2->ResetDrawViewPortList();
	pMonitor2->AddDrawViewPort(Direct3D::VIEW_PORT_RIGHT);
	std::string playerName2 = dataMeditor->GetPlayerName(1);
	pMonitor2->SetPlayerName(playerName2);
}

//更新
void PlaySceneMonitor::Update() {

}

//描画
void PlaySceneMonitor::Draw() {

	//Image::SetTransform(hImage_, transform_);
	//Image::Draw(hImage_);
}

//開放
void PlaySceneMonitor::Release() {

}