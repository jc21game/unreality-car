#pragma once;
#include "../Engine/GameObject.h"
#include <map>
#include <memory>
#include <vector>

//前方宣言
class PlayScene;
class Stage;
class Car;

//車の情報
//車の数だけ用意する
struct CarInfo {

	CarInfo()
		: currentCheckPointNumber(0)
		, roundNumber(0)
		, currentVelocity(0.f)
		, currentPosition(XMVectorSet(0.f, 0.f, 0.f, 0.f))
		, canGoal(false)
		, isGoal(false)
		, isCarRaceDuring(false)
	{
	}

	XMVECTOR currentMoveVector;
	XMVECTOR currentPosition;
	int currentCheckPointNumber;
	int roundNumber;
	float currentVelocity;
	float prevCheck;
	bool canGoal;
	bool isGoal;
	bool isCarRaceDuring;
};

struct CarPerformance {

	float mass;
};

struct RecordInRace {

	std::vector<float> lapTime;
	float maxSpeed;
	float passingCheckPointTime;
};

struct RaceStatus {

	int checkPointNumber;										//ステージ上にあるチェックポイントの数
	int goalRoundNumber;										//ゴールに必要な周回数
	bool isFirst;
	bool isDuringRace;
};

struct RaceResult {

	std::vector<std::string> ranking;						//順位順のプレイヤー名リスト
	std::vector<float> resultTime;							//ゴールした順の時間のリスト
};

class DataMediator {

private:
	//シングルトン
	static std::unique_ptr<DataMediator> instance_;

public:
	/**
	* \brief インスタンスの取得
	* \return インスタンス
	*/
	static const std::unique_ptr<DataMediator>& GetInstance();
	/**
	* \brief インスタンスの破棄
	*/
	static void DestroyInstance();

private:

	std::map <std::string, CarInfo> playerMap_;				//プレイヤー
	std::map <std::string, CarPerformance> carPerformanceMap_;
	std::map < std::string, RecordInRace> raceRecordMap_;
	std::map <std::string, XMVECTOR> checkPointMap_;
	std::vector<std::string> playerNameList_;					//プレイヤー名リスト

	Stage* pStage_;												//ステージ
	PlayScene* pScene_;
	RaceStatus raceStatus_;
	RaceResult raceResult_;

	const int waitTime = 3;
	float raceTime_;
	float raceStartedTime_;


	//コンストラクタ
	DataMediator() = default;

public:
	/**
	* \brief	初期化
	*/
	void Initialize();

	/**
	* \brief	管理するプレイヤーの追加
	* \param playerName		追加するプレイヤーの名前
	*/
	void AddPlayer(const std::string& playerName);

	float RaceTimeUpdate();

	/**
	* \brief	ステージ上にあるチェックポイントを取得する
	* \param	checkpointNumber	ステージ上にあるチェックポイントの数
	*/
	void SetCheckPointNumber(int checkpointNumber);

	void SetGoalRoundNumber(int goalNumber);

	void SetCheckPointMap(const std::string& checkPointName, XMVECTOR checkPointPos);

	void SetCarInfo(const std::string& playerName, XMVECTOR& position, XMVECTOR& move, float vel);

	void SetCarPerformance(const std::string& playerName, float mass);

	/**
	* \param	currentStage		現在のステージ
	*/
	void SetCurrentStage(GameObject* currentStage);
	void SetCurrentScene(GameObject* currentScene);

	//ゴールした時間を記憶
	//ゴールしたら呼んでほしい
	void SetResultTime();

	CarInfo GetCarInfo(const std::string& playerName);

	CarPerformance GetCarPerformance(const std::string& playerName);

	Stage* GetCurrentStage() const;

	/**
	* \brief	指定したプレイヤーの現在の座標と次ぼチェックポイントの距離を取得
	* \param	nextChekPoint		次のチェックポイント
	* \param	playerName			対象のプレイヤーの名前
	*/
	float GetNextCheckPointDistance(int nextCheckPoint, const std::string& playerName);

	XMVECTOR GetCheckPointPos(const std::string& checkPointName);

	float GetRaceTime();

	int GetWaitTime() const;

	bool GetIsDuringRace()const;

	//指定プレイヤーの順位取得
	int GetRanking(const std::string& playerName);

	//num人目のプレイヤー名取得
	std::string GetPlayerName(const int num);


	//指定順位のゴールした時間を取得
	float GetResultTime(const int ranking);

	//指定プレイヤーの瞬間最高速度を取得
	float GetMaxSpeed(const std::string& playerName);

	//指定プレイヤーのラップタイム取得
	float GetLapTime(const std::string& playerName, const int round);

	/**
	* \brief	指定したプレイヤーがどのチェックポイントを通過したか
	* \param	playerName			対象のプレイヤーの名前
	* \param	checkPointName		対象のプレイヤーが当たったチェックポイント
	*/
	void PassingCheckPoint(const std::string& playerName, const std::string& checkPointName);

	//順位更新
	void UpdateRanking();

	int GetPlayerNumber(const std::string& playerName);

	int GetGoalRoundNumber();

};