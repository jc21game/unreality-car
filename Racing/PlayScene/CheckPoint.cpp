#include "CheckPoint.h"
#include "../Engine/Model.h"
#include "../Engine/Direct3D.h"
#include "../Engine/ColliderOrientedBox.h"
#include <iostream>

//コンストラクタ
CheckPoint::CheckPoint(GameObject* parent, std::string& name, FXMVECTOR& rotate)
	: GameObject(parent, name)
{
	std::cout << name << std::endl;
	//当たり判定付与
	ColliderOrientedBox* col = new ColliderOrientedBox(XMVectorSet(0, 10, 0, 0), XMVectorSet(120, 10, 10, 0), rotate);
	col->SetRotate(rotate);
	AddCollider(col);
}

//初期化
void CheckPoint::Initialize() {

	ResetDrawViewPortList();
	AddDrawViewPort(Direct3D::VIEW_PORT_LEFT);
	AddDrawViewPort(Direct3D::VIEW_PORT_RIGHT);
}

//更新
void CheckPoint::Update() {
}

//描画
void CheckPoint::Draw() {
}

//開放
void CheckPoint::Release() {
}