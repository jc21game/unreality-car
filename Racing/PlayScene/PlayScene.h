#include "../Engine/GameObject.h"

class Car;

class PlayScene
	: public GameObject
{
	const int playerCount = 1;

public:
	PlayScene(GameObject* parent);
	~PlayScene();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//車の生成
	void InstantiateCar(const int playerCount);

	static Car* InstantiateCar(GameObject* pParent, std::string playerName);

	void ChangeNextScene();
};