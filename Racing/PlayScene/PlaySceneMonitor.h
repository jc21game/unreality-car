#pragma once
#include "../Engine/GameObject.h"


//プレイシーン全体の画面表示を管理するクラス
class PlaySceneMonitor : public GameObject {

private:
	int hImage_;				//画像データのハンドル
	std::string imageName_;		//画像ファイルの名前

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlaySceneMonitor(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//プレイヤーの情報を出す画面の設定
	//引数：mode　モード 0:１人　1:２人
	void SetPlayerMonitor(bool mode);
};