#include "PlayScene.h"
#include "Stage.h"
#include "DataMediator.h"
#include "Car.h"
#include "CountDown.h"
#include "PlaySceneMonitor.h"
#include "MiniMap.h"
#include "Back.h"
#include "../ResultScene/ResultScene.h"

#include "../Engine/SceneManager.h"
#include "../Engine/SceneTimer.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"
#include <iostream>

PlayScene::PlayScene(GameObject* parent) :
	GameObject(nullptr, "PlayScene")
{
}
PlayScene::~PlayScene()
{
}

void PlayScene::Initialize()
{
	Instantiate<Back>(this);

	//メタAIのリセット処理
	DataMediator::DestroyInstance();
	auto&& dataMediator = DataMediator::GetInstance();
	//dataMediator->DestroyInstance();
	
	//ステージの生成とメタAIへの登録
	dataMediator->SetCurrentStage(Instantiate<Stage>(this));
	dataMediator->SetCurrentScene(this);
	dataMediator->SetGoalRoundNumber(3);
	
	//車の生成
	InstantiateCar(2);

	Instantiate<PlaySceneMonitor>(this);
	dataMediator->Initialize();

	Instantiate<MiniMap>(this);

	//ポストエフェクトの有効化
	Direct3D::ValidatePostEffect();

	Instantiate<CountDown>(this);
}

void PlayScene::Update() {

	auto&& dataMediator = DataMediator::GetInstance();
	dataMediator->RaceTimeUpdate();
	dataMediator->GetRaceTime();

	if (Input::IsKeyDown(DIK_ESCAPE)) {
		if (auto* pSceneManager = dynamic_cast<SceneManager*>(FindObject("SceneManager"))) {
			
			pSceneManager->ChangeScene(SCENE_ID_START);
		}
	}
}

void PlayScene::Draw() {

}

void PlayScene::Release() {

	//メタAIのリセット処理
	//auto&& meta = DataMediator::GetInstance();
	//meta->DestroyInstance();
	//カメラを初期位置にセット
	Camera::Initialize();
}

void PlayScene::InstantiateCar(const int playerCount) {

	//playerの数だけループ
	for (int i = 0; i < playerCount; i++) {

		//プレイヤーにつける名前
		auto playerName = "Player" + std::to_string(i + 1);
		GameObject* pCar = InstantiateCar(this, playerName);
		//プレイヤーのメタAIへの登録
		auto&& dataMediator = DataMediator::GetInstance();
		auto startPos = dataMediator->GetCheckPointPos("CheckPoint1");
		startPos.vecZ = i * 5;
		pCar->SetPosition(startPos);
		dataMediator->AddPlayer(playerName);
	}
}

void PlayScene::ChangeNextScene() {

	SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
	pSceneManager->ChangeScene(SCENE_ID_RESULT);
}

Car * PlayScene::InstantiateCar(GameObject * pParent, std::string playerName)
{
	auto pNewObject = new Car(pParent, playerName);
	if (pParent != nullptr)
	{
		pParent->PushBackChild(pNewObject);
	}
	pNewObject->Initialize();
	return pNewObject;
}
