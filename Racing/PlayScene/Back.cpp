#include "Back.h"
//#include "../Engine/Model.h"
#include "../Engine/Image.h"

//コンストラクタ
Back::Back(GameObject * parent)
	:GameObject(parent, "Back"), hModel_(-1), hPict_(-1) {
}

//デストラクタ
Back::~Back() {
}

//初期化
void Back::Initialize() {
	//モデルデータのロード
	/*hModel_ = Model::Load("Model/Back.fbx");
	assert(hModel_ >= 0);

	transform_.position_.vecZ = 20;*/

	//画像データのロード
	//昼
	//hPict_ = Image::Load("SampleResource/BackSun.png");
	//夜
	//hPict_ = Image::Load("SampleResource/BackStar.png");
	//調整用(夜)
	hPict_ = Image::Load("SampleResource/BackTest.png");
	
	assert(hPict_ >= 0);
	Image::SetTransformFullSize(hPict_);
}

//更新
void Back::Update() {
}

//描画
void Back::Draw() {
	/*Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);*/

	//Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Back::Release() {
}


