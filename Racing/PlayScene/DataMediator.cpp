#include <cmath>
#include "DataMediator.h"
#include "Car.h"
#include "PlayScene.h"
#include "Stage.h"
#include "../Engine/Direct3D.h"
#include "../Engine/SceneTimer.h"

#include <iostream>
using namespace std;

unique_ptr<DataMediator>DataMediator::instance_;

//初期化
void DataMediator::Initialize() {

	assert(pScene_ != nullptr);
	assert(pStage_ != nullptr);
	raceStatus_.isFirst = true;
}

const unique_ptr<DataMediator>& DataMediator::GetInstance() {

	if (!instance_) {

		//適当な構造体にクラスを継承させてインスタンス
		struct A : DataMediator {};
		//make_unique . ヘルパー関数 動的にメモリを確保
		instance_ = make_unique<A>();
	}

	return instance_;
}

void DataMediator::DestroyInstance() {

	instance_.reset();
}

void DataMediator::AddPlayer(const std::string& playerName) {

	CarInfo info;
	playerMap_[playerName] = info;
	playerMap_[playerName].isCarRaceDuring = false;

	raceResult_.ranking.push_back(playerName);
	playerNameList_.push_back(playerName);
}

float DataMediator::RaceTimeUpdate() {

	raceTime_ = SceneTimer::GetElapsedSecounds();

	if (raceTime_ > waitTime && raceStatus_.isFirst) {

		raceStartedTime_ = SceneTimer::GetElapsedSecounds();
		for (int index = 0; index < playerNameList_.size(); ++index)
			playerMap_[GetPlayerName(index)].isCarRaceDuring = true;

		raceStatus_.isDuringRace = true;
		raceStatus_.isFirst = false;
	}

	return raceTime_;
}

void DataMediator::SetCheckPointNumber(int checkpoint) {

	raceStatus_.checkPointNumber = checkpoint;
}

void DataMediator::SetGoalRoundNumber(int goalNumber) {

	raceStatus_.goalRoundNumber = goalNumber;
}

void DataMediator::SetCheckPointMap(const std::string & checkPointName, XMVECTOR checkPointPos) {

	checkPointMap_[checkPointName] = checkPointPos;
	//std::cout << checkPointName << "：" << checkPointMap_[checkPointName].vecX << "：" << checkPointMap_[checkPointName].vecZ << std::endl;
}

void DataMediator::SetCarInfo(const std::string & playerName, XMVECTOR & position, XMVECTOR & move, float vel) {

	playerMap_[playerName].currentPosition = position;
	playerMap_[playerName].currentMoveVector = move;
	playerMap_[playerName].currentVelocity = vel;

	if (raceRecordMap_[playerName].maxSpeed < vel) raceRecordMap_[playerName].maxSpeed = vel;
}

void DataMediator::SetCarPerformance(const std::string & playerName, float mass) {

	carPerformanceMap_[playerName].mass = mass;
}

void DataMediator::SetCurrentStage(GameObject * currentStage) {

	pStage_ = dynamic_cast<Stage*>(currentStage);
}

void DataMediator::SetCurrentScene(GameObject * currentScene) {

	pScene_ = dynamic_cast<PlayScene*>(currentScene);
}

CarInfo DataMediator::GetCarInfo(const std::string & playerName) {

	return playerMap_[playerName];
}

CarPerformance DataMediator::GetCarPerformance(const std::string & playerName)
{
	return carPerformanceMap_[playerName];
}

void DataMediator::SetResultTime() {

	raceResult_.resultTime.push_back(SceneTimer::GetElapsedSecounds());
}

Stage * DataMediator::GetCurrentStage() const {

	return pStage_;
}

float DataMediator::GetNextCheckPointDistance(int nextCheckPoint, const std::string& playerName) {

	float distance = hypot(
		checkPointMap_["CheckPoint" + std::to_string(nextCheckPoint + 1)].vecX - playerMap_[playerName].currentPosition.vecX,
		checkPointMap_["CheckPoint" + std::to_string(nextCheckPoint + 1)].vecZ - playerMap_[playerName].currentPosition.vecZ
	);

	return distance;
}

XMVECTOR DataMediator::GetCheckPointPos(const std::string & checkPointName)
{
	return checkPointMap_[checkPointName];
}

float DataMediator::GetRaceTime() {

	if (raceStatus_.isDuringRace)
	{
		//std::cout << "経過時間：" << SceneTimer::GetElapsedSecounds() - raceStartedTime_ << std::endl;
		return SceneTimer::GetElapsedSecounds() - raceStartedTime_;
	}

	return  0.f;
}

int DataMediator::GetWaitTime() const {

	return  waitTime;
}

int DataMediator::GetRanking(const std::string& playerName)
{
	int ranking = 1;
	auto it = raceResult_.ranking.begin();
	while (it != raceResult_.ranking.end()) {

		if ((*it) == playerName)
			return ranking;

		ranking++;
		it++;
	}

	return -1;
}

std::string DataMediator::GetPlayerName(const int num) {

	return playerNameList_[num];
}

float DataMediator::GetResultTime(const int ranking) {

	return raceResult_.resultTime[ranking - 1];
}

float DataMediator::GetMaxSpeed(const std::string & playerName) {

	return raceRecordMap_[playerName].maxSpeed;
}

float DataMediator::GetLapTime(const std::string & playerName, const int round) {

	if (raceRecordMap_[playerName].lapTime.size() < round) return -1;

	return raceRecordMap_[playerName].lapTime[round];
}

void DataMediator::PassingCheckPoint(const std::string& playerName, const std::string& checkPointName) {

	int passingCheckPoint = stoi(checkPointName.substr(10));
	//std::cout << playerMap_[playerName].currentCheckPointNumber << std::endl;

	//今回通過したチェックポイントが、前回通過したチェックポイントより一つ後のものか判定
	if (passingCheckPoint == playerMap_[playerName].currentCheckPointNumber + 1) {

		playerMap_[playerName].prevCheck = 0;
		playerMap_[playerName].currentCheckPointNumber = passingCheckPoint;
		raceRecordMap_[playerName].passingCheckPointTime = SceneTimer::GetElapsedSecounds();

		std::cout << playerName << "：現在のチェックポイント" << playerMap_[playerName].currentCheckPointNumber << std::endl;
	}

	if (playerMap_[playerName].canGoal && passingCheckPoint == 1 ) {

		playerMap_[playerName].isGoal = true;
	}
	
	if (playerMap_[playerName].currentCheckPointNumber >= raceStatus_.checkPointNumber - 1) {

		playerMap_[playerName].roundNumber++;
		playerMap_[playerName].currentCheckPointNumber = 0;
		raceRecordMap_[playerName].lapTime.push_back(SceneTimer::GetElapsedSecounds());

		std::cout << playerMap_[playerName].roundNumber << "周目" << raceRecordMap_[playerName].lapTime.back() << std::endl;
		
		//指定の周回数に達したらレース中ではなくする
		if (playerMap_[playerName].roundNumber == raceStatus_.goalRoundNumber){
			
			playerMap_[playerName].canGoal = true;
			SetResultTime();
			if (raceResult_.resultTime.size() == playerMap_.size())
				pScene_->ChangeNextScene();
		}
	}

	if (playerMap_[playerName].prevCheck < GetNextCheckPointDistance(playerMap_[playerName].currentCheckPointNumber, playerName) && playerMap_[playerName].prevCheck != 0) {

		//std::cout << playerName << "：逆走！" << std::endl;
	}

	playerMap_[playerName].prevCheck = GetNextCheckPointDistance(playerMap_[playerName].currentCheckPointNumber, playerName);
}

void DataMediator::UpdateRanking() {

	//前後の順位で比較、入れ替え
	//1位と2位、2位と3位みたいな
	//3位が1/60秒で一位になるなんてないやろ（多分）

	//最下位までループ
	auto it = raceResult_.ranking.begin();
	while (++it != raceResult_.ranking.end()) {

		auto first = --it;
		auto second = ++it;
		auto firstPlayer = playerMap_[(*first)];
		auto secondPlayer = playerMap_[(*second)];

		if (firstPlayer.isGoal)	continue;		//ゴールしてたら入れ替えない

		//周回数が後者のほうが多ければ順位入れ替え
		if (firstPlayer.roundNumber < secondPlayer.roundNumber)
			iter_swap(first, second);

		//周回数が同じ
		else if (firstPlayer.roundNumber == secondPlayer.roundNumber) {

			//チェックポイントの通過数が後者のほうが多ければ順位入れ替え
			if (firstPlayer.currentCheckPointNumber < secondPlayer.currentCheckPointNumber)
				iter_swap(first, second);


			//チェックポイントの通過数が同じ
			else if (firstPlayer.currentCheckPointNumber == secondPlayer.currentCheckPointNumber) {

				float firstdist = pStage_->GetDistanceNextCheck(firstPlayer.currentPosition, firstPlayer.currentCheckPointNumber);
				float seconddist = pStage_->GetDistanceNextCheck(secondPlayer.currentPosition, secondPlayer.currentCheckPointNumber);

				//次のチェックポイントまでの距離が後者のほうが短ければ順位入れ替え
				if (firstdist > seconddist)
					iter_swap(first, second);
			}
		}
	}
}

int DataMediator::GetPlayerNumber(const std::string & playerName)
{
	for (int i = 0; i < playerNameList_.size(); i++) {

		if (playerNameList_[i] == playerName)return i;
	}

	return -1;
}

int DataMediator::GetGoalRoundNumber()
{
	return raceStatus_.goalRoundNumber;
}