#include "Stage.h"
#include "../Stage/StageDataManager.h"
#include "../Engine/StaticModel.h"
#include "../Engine/FbxParts.h"
#include "../Engine/Direct3D.h"
#include "../Engine/Model.h"

#include "DataMediator.h"
#include "CheckPoint.h"
#include "AccelerationPanel.h"
#include <iostream>

//コンストラクタ
Stage::Stage(GameObject* parent)
	:GameObject(parent, "Stage"), hModel_(-1), hModelAll_(-1)
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize() {

	//モデルデータのロード
	auto* stageDataManager = StageDataManager::GetInstance();
	auto* currentStage = stageDataManager->GetCurrentStageData();
	
	hModel_ = currentStage->GetRoadModelHandle();
	hModelDirt_ = currentStage->GetDirtModelHandle();
	hModelWall_ = currentStage->GetWallModelHandle();
	hModelAll_ = currentStage->GetWholeAreaModelHandle();


	assert(hModel_ >= 0);
	assert(hModelDirt_ >= 0);
	assert(hModelWall_ >= 0);

	auto&& dataMediator = DataMediator::GetInstance();
	auto&& checkArray = currentStage->GetCheckPointData();
	checkPointNumber = checkArray.size();

	if (checkPointNumber > 11) {

		outsideModel = StaticModel::Load("../Assets/MapData/suzuki_Obstracle2.fbx");
	}

	for (int index = 0; index < checkPointNumber; ++index) {

		XMVECTOR pos = XMVectorSet(checkArray[index].position.x, checkArray[index].position.y, checkArray[index].position.z, 0.f);
		XMVECTOR rotate = XMVectorSet(0.f, checkArray[index].angle, 0.f, 0.f);
		auto name = "CheckPoint" + std::to_string(index);
		
		GameObject* checkPoint = InstantiateCheckPoint(this ,name, rotate);
		checkPoint->SetPosition(pos);

		dataMediator->SetCheckPointMap(name, pos);
	}

	dataMediator->SetCheckPointNumber(checkArray.size());
	//Instantiate<AccelerationPanel>(this);

	wallTransform = transform_;
	wallTransform.position_.vecY =  -10.f;
	
	ResetDrawViewPortList();
	AddDrawViewPort(Direct3D::VIEW_PORT_LEFT);
	AddDrawViewPort(Direct3D::VIEW_PORT_RIGHT);
}

//更新
void Stage::Update() {

}

//描画
void Stage::Draw() {

	auto* stageDataManager = StageDataManager::GetInstance();
	auto* currentStage = stageDataManager->GetCurrentStageData();
	
	if (currentStage == stageDataManager->GetStageData(1)) {
		StaticModel::SetTransform(hModel_, transform_);
		StaticModel::Draw(hModel_);

		StaticModel::SetTransform(hModelDirt_, transform_);
		StaticModel::Draw(hModelDirt_);

		StaticModel::SetTransform(hModelWall_, wallTransform);
		StaticModel::Draw(hModelWall_);

		if (checkPointNumber > 11) {

			StaticModel::SetTransform(outsideModel, wallTransform);
			//StaticModel::Draw(outsideModel);
		}
		StaticModel::SetTransform(hModelAll_, transform_);
		//StaticModel::Draw(hModelAll_);
	}
	else {

		StaticModel::SetTransform(hModelAll_, transform_);
		StaticModel::Draw(hModelAll_);
	}
}

//開放
void Stage::Release() {

}

int Stage::GetModelHandle() const{

	return hModel_;
}

int Stage::GetDirtModelHandle() const{

	return hModelDirt_;
}

int Stage::GetWallModelHandle() const{

	return hModelWall_;
}

float Stage::GetDistanceNextCheck(XMVECTOR& position, int checkNumber) {

	XMVECTOR nextCheckPasition;	//次のチェックポイントの位置

	//チェックポイントをすべて取ってたら
	if (checkNumber < checkPointNumber) {

		//次は一つ目
		std::string nextCheckName = "CheckPoint" + std::to_string(1);
		nextCheckPasition = StaticModel::GetBonePosition(hModel_, nextCheckName);
	}
	//取ってなければ
	else {

		//次の番号
		std::string nextCheckName = "CheckPoint" + std::to_string(checkNumber + 1);
		nextCheckPasition = StaticModel::GetBonePosition(hModel_, nextCheckName);
	}

	//とりあえず直線距離
	XMVECTOR vec = nextCheckPasition - position;

	return XMVector3Length(vec).vecX;
}

CheckPoint* Stage::InstantiateCheckPoint(GameObject* pParent, std::string name, FXMVECTOR& rotate){
	
	CheckPoint* pNewObject = new CheckPoint(pParent,name,rotate);
	if (pParent != nullptr)
	{
		pParent->PushBackChild(pNewObject);
	}
	pNewObject->Initialize();
	return pNewObject;
}