#pragma once
#include "../Engine/GameObject.h"
#include <string>

namespace Direct3D {

	enum VIEW_PORT_TYPE;
}

enum Gear {

	DRIVE = 0,
	REVERSE
};

enum GroundState {

	NORMAL = 0,
	DIRT
};

struct InputDevice {

	int keyboard;
	int gamepad;
};

struct PlayerKeyCode {

	InputDevice frontKey;
	InputDevice backKey;
	InputDevice leftKey;
	InputDevice rightKey;
	InputDevice curveKey;
	InputDevice backCameraKey;
	InputDevice driveKey;
	InputDevice reverseKey;
	InputDevice emptyKey;
};

struct CameraMotion {

	CameraMotion()
		:cameraStopFlg(false)
	{
	}

	XMVECTOR cameraPos;
	XMVECTOR backCameraPos;
	XMVECTOR cameraTarget;
	XMVECTOR camV;
	float lerpTime = 0.f;
	float driftCamera;
	float iotaMotion;
	bool cameraStopFlg;
};

class Car : public GameObject {

	float initialVelocity_;						//初速度
	float acceleration_;						//加速度（仮）
	float defaultAcceleration_;
	float velocity_;								//速度
	float time_;									//アクセルを踏んでいる時間
	float timeStartedAccelerator_;		//アクセルを踏み始めた時間
	float timeReleasedAccelerator_;		//アクセルを離した時間
	float brakeResistance_;					//ブレーキによる抵抗力
	float moveVelocity_;

	int padNum;
	int hStageModel_;
	int hDirtModel_;
	int hWallModel_;
	
	GroundState groundState_;
	Gear gear_;
	PlayerKeyCode keyCode_;
	CameraMotion cameraMotion_;
	Direct3D::VIEW_PORT_TYPE viewPortType_;		//どのカメラに映すか

	XMVECTOR moveVec_;
	XMVECTOR slidingOnWallVec_;
	XMVECTOR reflectingVec_;
	XMMATRIX rotateMatrix_;

	float modelRotateY_;

	float fallEndTime_;
	float stoppedTime_;
	float forwardAndHitTime_;

	bool isDirt = false;
	bool isDuringFall_;
	bool isWithAccelerationPanel_;
	bool isReverseForwardAndHit_ = false;
	bool isForwardAndHit_ = false;
	bool isSlidingOnWall_ = false;
	bool isReflecting_ = false;

	/****************************************************************/

	//車クラスが持つ変数、パーツや車種で変動する
	//暫定で必要な物 -> 加速度・質量・
	float mass_;							//車両全体質量（kg）

	//車クラスは持たない変数、環境の変化によって変わる変数
	const float FRICTION_COEFFICIENT = 0.8f;				//摩擦係数　			乾燥路面を想定
	const float gravitationalAcceleration = 9.8f;			//重力加速度

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Car(GameObject* parent, std::string& str);

	/**
	* \brief	初期化
	*/
	void Initialize() override;

	/**
	* \brief	更新
	*/
	void Update() override;

	/**
	* \brief	描画
	*/
	void Draw() override;

	/**
	* \brief	解放
	*/
	void Release() override;

	/**
	* \brief	何かに当たった時呼ばれる
	*/
	void OnCollision(GameObject *pTarget) override;
	void OnCollisionEnter(GameObject *pTarget) override;

	void SettingEachPlayer(const std::string& playerName);
	void DrawViewportPreference(const Direct3D::VIEW_PORT_TYPE& type);
	void GroundDecision();
	void AccelerationPanelProcess();
	void LandingProcess();
	void CarBodyRotation();
	void AcceleratorAndBrake();
	void HitWithWall();
	void GearChangeApplication();
	void CameraMotion();
	void ModelMotion();
	void UpdateEnd();
	
	void FrontRearCarToEachOtherHit(GameObject* pTarget );
	void LeftRightCarToEachOtherHit(GameObject* pTarget);

	void ChangeGear(const Gear& change);
	void ChangeGroundState(const GroundState& change);
};