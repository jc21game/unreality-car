#pragma once
#include <vector>
#include "../Engine/GameObject.h"

class CountDown : public GameObject {

	struct CountDownInfo
	{
		int hImage;
		Transform imageTransform;
	};
	
	int countDownNumber;
	std::vector<CountDownInfo> hImage_;
	
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	CountDown(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};