#include "Car.h"
#include "PlayerCamera.h"
#include "PlayerAuxiliary.h"
#include "../Engine/Direct3D.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"

namespace PlayerCamera{

	const float start = 0.f;
	const float goal = 1.f;
}

void PlayerCamera::SetIotaMotion(float moveVelocity, CameraMotion& cameraMotion,PlayerKeyCode& keyCode , int padNum){

	if (moveVelocity > 0.f) {

		cameraMotion.driftCamera += static_cast<float>(0.11f * Input::IsKey(keyCode.curveKey.keyboard)) * (Input::IsKey(keyCode.leftKey.keyboard) + Input::IsKey(keyCode.rightKey.keyboard));
		cameraMotion.driftCamera += static_cast<float>(0.11f * Input::IsPadButton(keyCode.curveKey.gamepad , padNum)) * PlayerAuxiliary::IsEnteredCheck(Input::GetPadStickL(padNum).vecX * 0.1f);
	}

	cameraMotion.driftCamera = max(cameraMotion.driftCamera - 0.1f, 0);

	if (abs(cameraMotion.lerpTime) < goal){

		if (Input::IsKey(keyCode.rightKey.keyboard) || Input::GetPadStickL(padNum).vecX > 0) {

			cameraMotion.lerpTime -= 0.025f;
		}

		if (Input::IsKey(keyCode.leftKey.keyboard) || Input::GetPadStickL(padNum).vecX < 0) {

			cameraMotion.lerpTime += 0.025f;
		}
	}

	if (cameraMotion.lerpTime > 0)	cameraMotion.lerpTime -= 0.0125f;
	if (cameraMotion.lerpTime < 0)	cameraMotion.lerpTime += 0.0125f;

	cameraMotion.iotaMotion = PlayerAuxiliary::lerp(start, goal, cameraMotion.lerpTime);
}

void PlayerCamera::PlayerCameraSetPosition(GameObject* targetObj,float moveVelocity, CameraMotion & cameraMotion, PlayerKeyCode & keyCode, XMMATRIX & rotateMatrix, Direct3D::VIEW_PORT_TYPE& type){

	//カメラを車に追従させる
	cameraMotion.cameraPos = { cameraMotion.iotaMotion,5.f,-1 * (12.f + (moveVelocity / 2) + cameraMotion.driftCamera) ,0.f };
	cameraMotion.backCameraPos = { 0.f,5.f, 12 ,0.f };
	cameraMotion.cameraTarget = { 0.f,3.f,0.f,0.f };

	cameraMotion.cameraPos = XMVector3TransformCoord(cameraMotion.cameraPos, rotateMatrix);
	cameraMotion.backCameraPos = XMVector3TransformCoord(cameraMotion.backCameraPos, rotateMatrix);

	if (!cameraMotion.cameraStopFlg)
		cameraMotion.camV = targetObj->GetPosition();

	cameraMotion.cameraPos += cameraMotion.camV;

	Camera::SetPosition(cameraMotion.cameraPos, type);
}

void PlayerCamera::PlayerCameraSetPosition(GameObject * targetObj, CameraMotion & cameraMotion, Direct3D::VIEW_PORT_TYPE & type){

	cameraMotion.backCameraPos += cameraMotion.camV;
	Camera::SetPosition(cameraMotion.backCameraPos, type);
}

void PlayerCamera::PlayerCameraSetTarget(GameObject* targetObj, XMVECTOR& targetPosition, Direct3D::VIEW_PORT_TYPE& type){

	Camera::SetTarget(targetObj->GetPosition() + targetPosition, type);
}
