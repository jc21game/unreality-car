#pragma once

namespace PlayerAuxiliary{

	bool ButtonCheck(int keybord, int gamepad);
	bool ButtonDownCheck(int keybord, int gamepad);
	bool ButtonCheck(int keybord, int gamepad, int playerNumber);
	bool ButtonDownCheck(int keybord, int gamepad, int playerNumber);
	bool IsEnteredCheck(float value);
	
	/**
	* \brief	二点startとgoalの間を、時間 lerpTime で線形補間する
	* \param	start		始点
	* \param	goal		終点
	* \param	lerpTime	経過割合
	*/
	float lerp(float start, float goal, float lerpTime);
};

