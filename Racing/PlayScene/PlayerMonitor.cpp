#include "PlayerMonitor.h"
#include "DataMediator.h"
#include "../Engine/SceneTimer.h"
#include "../Engine/Image.h"
#include "../Engine/Direct3D.h"


#define  VELOCITY 0x01
#define  RANKING 0x02
#define  LAP 0x04
#define  GOAL 0x08

//コンストラクタ
PlayerMonitor::PlayerMonitor(GameObject * parent)
	: GameObject(parent, "PlayerMonitor"), filterFlag(0)
{
	for (int i = 0; i < NUMBER_TYPE_MAX; i++) {

		hImagePeriod[i] = -1;

		for (int j = 0; j < NUMBER_MAX; j++)
			hImageNumbers[i][j] = -1;
	}

	hImageLapSlash = -1;
}

//初期化
void PlayerMonitor::Initialize() {

	const int HALF_SIZE = 2;

	ImageLoad();

	transform_.scale_.vecY /= HALF_SIZE;	//Y方向の縮小
}

//更新
void PlayerMonitor::Update() {

	filterFlag = 0;	//フラグ初期化

	const auto& FilterTipe = Direct3D::GetCurrentFilterType();	// 現在のフィルター

	// フィルターをかけるかを決める
	if (FilterTipe == Direct3D::SHADER_FILTER::CRT) filterFlag |= VELOCITY;
	if (FilterTipe == Direct3D::SHADER_FILTER::CRT) filterFlag |= RANKING;
	if (FilterTipe == Direct3D::SHADER_FILTER::CRT) filterFlag |= LAP;

	if (FilterTipe == Direct3D::SHADER_FILTER::CRT) filterFlag |= GOAL;

	filterFlag |= VELOCITY;
	//filterFlag |= RANKING;
}

//描画（フィルター有り）
void PlayerMonitor::Draw() {

	// flag == 0 で描画
	if (!(filterFlag & VELOCITY)) DrawVelocity();
	if (!(filterFlag & RANKING))  DrawRanking();
	if (!(filterFlag & LAP))	  DrawLap();
	if (!(filterFlag & GOAL))	  DrawGoal();

}

//描画（フィルター無し）
void PlayerMonitor::PostDraw() {

	// flag == 1 で描画
	if (filterFlag & VELOCITY) DrawVelocity();
	if (filterFlag & RANKING)  DrawRanking();
	if (filterFlag & LAP)	   DrawLap();
	if (!(filterFlag & GOAL))	  DrawGoal();
}

//開放
void PlayerMonitor::Release() {


}

void PlayerMonitor::SetPlayerName(std::string& name) {

	playerName = name;
}

void PlayerMonitor::ImageLoad() {

	//Assetsフォルダまでのパス
	std::string assets = "../Assets/";

	//数字のロード
	for (int i = 0; i < NUMBER_MAX; i++) {

		//画像データのロード
		//hImageNumbers[NUMBER_TYPE_TEST][i] = Image::Load(assets + "SampleResource/numbers/" + std::to_string(i) + ".png");
		//assert(hImageNumbers[i] >= 0);

		hImageNumbers[NUMBER_TYPE_NORMAL][i] = Image::Load(assets + "Number/" + std::to_string(i) + ".png");
		assert(hImageNumbers[i] >= 0);

		hImageNumbers[NUMBER_TYPE_RANK][i] = Image::Load(assets + "Play/rank/Rank" + std::to_string(i) + ".png");
		assert(hImageNumbers[i] >= 0);

		hImageNumbers[NUMBER_TYPE_LAP][i] = Image::Load(assets + "Play/wrap/Rap" + std::to_string(i) + ".png");
		assert(hImageNumbers[i] >= 0);
	}

	//ピリオド（小数点）の画像ロード
	//hImagePeriod[NUMBER_TYPE_TEST] = Image::Load(assets + "SampleResource/numbers/period.png");
	//assert(hImagePeriod[NUMBER_TYPE_TEST] >= 0);

	hImagePeriod[NUMBER_TYPE_NORMAL] = Image::Load(assets + "Number/piriod.png");
	assert(hImagePeriod[NUMBER_TYPE_NORMAL] >= 0);

	hImageLapSlash = Image::Load(assets + "Play/wrap/RapLine.png");
	assert(hImageLapSlash >= 0);

	hImageSpeedMeter = Image::Load(assets + "SampleResource/SpeedMeter.png");
	assert(hImageSpeedMeter >= 0);
	hImageSpeedNeedle = Image::Load(assets + "SampleResource/SpeedNeedleUp2.png");
	assert(hImageSpeedNeedle >= 0);

	hImageGoal = Image::Load(assets + "Number/Goal.png");
}

void PlayerMonitor::DrawVelocity() {

	auto&& dataMeditor = DataMediator::GetInstance();							//プレイシーンの情報を保持しているインスタンス
	float velocity = dataMeditor->GetCarInfo(playerName).currentVelocity;		//速度
	XMVECTOR drawPosition = { -0.55f, -0.65f, 0.f, 0.f };						//描画位置

	DrawNumber(drawPosition, velocity, NUMBER_TYPE_NORMAL);
}

void PlayerMonitor::DrawRanking() {

	const float SCALE_MULTIPLIER = 1.5f;


	auto&& dataMeditor = DataMediator::GetInstance();			//プレイシーンの情報を保持しているインスタンス
	dataMeditor->UpdateRanking();								//順位の更新
	int ranking = dataMeditor->GetRanking(playerName);			//順位
	XMVECTOR drawPosition = { -0.7f, 0.8f, 0.f, 0.f };			//描画位置

	transform_.scale_ *= SCALE_MULTIPLIER;

	//描画
	DrawNumber(drawPosition, ranking, NUMBER_TYPE_RANK);

	transform_.scale_ /= SCALE_MULTIPLIER;

}

void PlayerMonitor::DrawLap()
{
	const float SCALE_MULTIPLIER = 1.1f;
	const float SHIFT_WIDE = 0.05f;
	const float SHIFT_HIGHT = 0.01f;

	auto&& dataMeditor = DataMediator::GetInstance();				//プレイシーンの情報を保持しているインスタンス
	int lap = dataMeditor->GetCarInfo(playerName).roundNumber + 1;	//ラップ
	int finalLap = dataMeditor->GetGoalRoundNumber();// +1;	//ラップ
	XMVECTOR drawPosition = { 0.8f, 0.8f, 0.f, 0.f };				//描画位置

	transform_.scale_ *= SCALE_MULTIPLIER;

	if (finalLap < lap)lap = finalLap;

	//現在のラップ数描画
	DrawNumber(drawPosition, lap, NUMBER_TYPE_LAP);

	transform_.scale_ /= SCALE_MULTIPLIER;

	//スラッシュの描画
	drawPosition.vecX -= SHIFT_WIDE;
	drawPosition.vecY -= SHIFT_HIGHT;
	transform_.position_ = drawPosition;

	Image::SetTransform(hImageLapSlash, transform_);
	Image::Draw(hImageLapSlash);

	//ゴールに必要なラップ数の描画
	drawPosition.vecX += SHIFT_WIDE * 2 + NUMBER_INTERVAL;
	drawPosition.vecY -= SHIFT_HIGHT;
	DrawNumber(drawPosition, finalLap, NUMBER_TYPE_LAP);
}

void PlayerMonitor::DrawGoal() {

	auto&& dataMeditor = DataMediator::GetInstance();							//プレイシーンの情報を保持しているインスタンス

	if (dataMeditor->GetCarInfo(playerName).isGoal) {

		Transform imageTransform;

		imageTransform.scale_ *= 2.f + sin(SceneTimer::GetElapsedSecounds() * 5) * 0.1f;
		imageTransform.position_ = { 0.f, 0.f, 0.f, 0.f };						//描画位置

		Image::SetTransform(hImageGoal, imageTransform);
		Image::Draw(hImageGoal);
	}
}

void PlayerMonitor::DrawNumber(XMVECTOR& position, float number, NUMBER_TYPE type) {

	////整数部の桁数を求める
	//int digit = static_cast<int>(std::to_string((int)number).length());			//整数部の桁数

	////整数部の桁数に合わせて位置調整
	//XMVECTOR startPosition = position;							//描画の開始位置
	//startPosition.vecX -= digit * NUMBER_INTERVAL;
	//transform_.position_ = startPosition;


	////描画する小数を四捨五入
	//number = static_cast<float>(std::round(number * pow(10, ROUND_DIGIT)) / pow(10, ROUND_DIGIT));

	//std::string str = std::to_string(number);					//文字列に変換
	//int size = digit + ROUND_DIGIT + 1;							//表示桁数

	////数字の描画
	//for (int i = 0; i < size; i++) {

	//	//小数点の位置はスキップ
	//	if (i == digit) {
	//		transform_.position_.vecX += NUMBER_INTERVAL;
	//		i++;
	//	}

	//	int drawNumber = str[i] - '0';							//描画する数字

	//	Transform trans = transform_;							//描画する状態
	//	if (drawNumber == 0)		trans.scale_.vecX *= 0.85f;	//0の画像の横幅調整

	//	//描画
	//	Image::SetTransform(hImageNumbers[type][drawNumber], trans);
	//	Image::Draw(hImageNumbers[type][drawNumber]);

	//	//一文字分ずらす
	//	transform_.position_.vecX += NUMBER_INTERVAL;
	//}

	//小数点の描画
	transform_.position_ = position;

	Image::SetTransform(hImageSpeedMeter, transform_);
	Image::Draw(hImageSpeedMeter);

	Transform needleTransform = transform_;
	
	auto&& dataMeditor = DataMediator::GetInstance();
	if (dataMeditor->GetCarInfo(playerName).isCarRaceDuring)
	{
		needleTransform.rotate_.vecZ = 120 - number * 2;
	}
	else{

		needleTransform.rotate_.vecZ = 120.f;
	}

	Image::SetTransform(hImageSpeedNeedle, needleTransform);
	Image::Draw(hImageSpeedNeedle);

	//Image::SetTransform(hImagePeriod[type], transform_);
	//Image::Draw(hImagePeriod[type]);
}

void PlayerMonitor::DrawNumber(XMVECTOR&position, int number, NUMBER_TYPE type) {

	//文字列に変換
	std::string str = std::to_string(number);

	//整数部の桁数に合わせて位置調整
	XMVECTOR startPosition = position;							//描画の開始位置
	startPosition.vecX -= str.size() * NUMBER_INTERVAL;
	transform_.position_ = startPosition;

	//数字の描画
	for (auto number : str) {

		int drawNumber = number - '0';							//描画する数字

		Transform trans = transform_;							//描画する状態
		if (drawNumber == 0)		trans.scale_.vecX *= 0.85f;	//0の画像の横幅調整

		//描画
		Image::SetTransform(hImageNumbers[type][drawNumber], trans);
		Image::Draw(hImageNumbers[type][drawNumber]);

		//一文字分ずらす
		transform_.position_.vecX += NUMBER_INTERVAL;
	}
}
