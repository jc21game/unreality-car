#pragma once
#include "../Engine/GameObject.h"
#include <map>

const int NUMBER_MAX = 10;	//数字の数

//プレイヤーに関する画面表示を管理するクラス
class PlayerMonitor : public GameObject {

private:
	const float NUMBER_INTERVAL = 0.125f;	//描画する数字の間隔
	const int ROUND_DIGIT = 2;			//小数値を切り捨てる桁数

	char filterFlag;					//フィルターをかけるかのフラグ

	enum NUMBER_TYPE {

		NUMBER_TYPE_TEST = 0,				//テスト用
		NUMBER_TYPE_NORMAL,					//ノーマル
		NUMBER_TYPE_RANK,					//順位用
		NUMBER_TYPE_LAP,					//ラップ数用
		NUMBER_TYPE_MAX
	};

	std::string playerName;				//リザルト表示をするプレイヤーの名前

	int hImageSpeedMeter;
	int hImageSpeedNeedle;
	int hImageGoal;
	int hImageNumbers[NUMBER_TYPE_MAX][NUMBER_MAX];		//0~9の数字のイメージハンドル
	int hImagePeriod[NUMBER_TYPE_MAX];	//ピリオドのイメージハンドル
	int hImageLapSlash;	//ピリオドのイメージハンドル
	
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayerMonitor(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//フィルターをかけない描画
	void PostDraw() override;

	//開放
	void Release() override;

	//リザルト表示するプレイヤーを指定
	void SetPlayerName(std::string& name);

private:
	//モデルデータのロード
	void ImageLoad();

	//描画
	void DrawVelocity();	//速度
	void DrawRanking();		//順位
	void DrawLap();
	void DrawGoal();



	//値を画像で描画（負数は非対応）
	//引数：小数点の位置、描画する数値
	void DrawNumber(XMVECTOR& position, float number, NUMBER_TYPE type);
	void DrawNumber(XMVECTOR& position, int number, NUMBER_TYPE type);
};