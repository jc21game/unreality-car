#pragma once
#include "../Engine/GameObject.h"

//地面を管理するクラス
class Back : public GameObject {

	int hModel_;    //モデル番号
	int hPict_;    //画像番号

public:
	//コンストラクタ
	Back(GameObject* parent);

	//デストラクタ
	~Back();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


	//モデル番号のゲッター
	int GetModelHandle() { return hModel_; }
};